#!/bin/bash
# This is designed to make it easier to copy images from finder to b.robnugen.com/journal/YYYY/walk/

THISYEAR=$(date +'%Y')

echo remember you can
echo ssh b.rn "'mkdir -p ~/b.robnugen.com/journal/$THISYEAR/walk'"

scp $@ b.rn:b.robnugen.com/journal/$THISYEAR/walk

ssh b.rn "scripts/create_thumbs.pl /home/thundergoblin/b.robnugen.com/journal/$THISYEAR/walk"
