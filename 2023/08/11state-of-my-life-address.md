---
title: "State of My Life Address"
tags: [ "soml", "august", "2023", "wow", "hisashiburi" ]
author: Rob Nugen
date: 2023-08-11T08:03:01+09:00
draft: false
---

(written 08:03 Friday 11 August 2023 JST)

1. Lin and I are still communicating well.
2. I live about 5 minutes away from our house.  There's a small hill
   between us.
3. Jennie is still fine and genki, though she might miss me.
4. Live in this 1BR 1 loft apartment is certainly simpler
5. I'm so grateful to not be fighting and grumbling all the time.
6. since Jan 2023, Mike and I have started ultimate each Sunday after
   SOGO
7. I still try to attend Bootcamp each Sunday in Yoyogi Park
8. I have started going to acro yoga as well most Sundays after
   ultimate
11. Since February, I have run an average of one Cuddle Party per
    month
12. They have had up to 5 attendees and as few as 1
10. Jess has been working with me to get Cuddle Party events promoted
13. Due to low attendance so far, we will go back to drawing board
    after September 2023 event
9. Vince, who said he could publish my book, has been fired for
   non-responsivenes.  I had paid him about 2800 USD, but hardly worth
   a flght to get it back.
14. He did help me write several words, but his non-responsiveness
    recently has made me think more than twice about it.
15. I just got my alien regitration card updated.  It's valid until 2030.
16. Now that Lin and I are separating, I wonder if I'll be here that
    long.
17. I'm working full time at VA.  Still working part time at AB.  Not
    working at JB anymore.  I might have some temp work for SB.
28. I'm now working part time on Support team to help them out. I'm
    asked to become a system expert within a month.
18. I've all but shut down the barefoot walking events.  The one on
    first Saturdays is well-attended, but the others are not so much.
19. Jess points out that I'm treating my heart work like a hobby.
    Hmmm
20. I'm proud that MKP Japan is growing beyond me.  Since January 2023,
    there's a weekly meeting in French and since June 2023 a  meeting
    starting down in Chigasaki.
21. I released TJ Bike last month.  I'm using Señor Disco now, who I
    just named, and might forget the name shortly.
22. I've been using my phone as a hotspot, but it only allows 30 GB
    tethering per month.
23. I can use unlimited not-tethering, but that's useless for my work.
24. I work from home almost every day.
25. I recently attended the Mindfulness Summit in Kamakura.  It was
    great to meet like-minded people!
26. AB's image server has been crashing and needs to be upgraded.
27. Marble Track 3 animation stage is next to my desk here at the
    apartment, but under a plastic cover.
