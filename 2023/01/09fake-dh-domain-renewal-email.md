---
title: "Fake DH domain renewal email"
tags: [ "scam", "spam", "dhstaff" ]
author: Rob Nugen
date: 2023-01-09T19:58:41+09:00
draft: false
---

Happy New Year!

I got an email claiming to be from my web hosting provider.

[![fake domain renewal notice](//b.robnugen.com/journal/2023/thumbs/fake_domain_renewal_notice.png)](//b.robnugen.com/journal/2023/fake_domain_renewal_notice.png)

It looks convincing but for a few details.  It's from dhstaff.com

The link to "update my info" ends with .br the TLD of Brazil.

[![gmail let this through](//b.robnugen.com/journal/2023/thumbs/gmail_let_this_through.png)](//b.robnugen.com/journal/2023/gmail_let_this_through.png)

I'll report this to DH, and I'm creating this post to commemorate the
attempt.