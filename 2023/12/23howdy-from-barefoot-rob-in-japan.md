---
title: "Howdy from barefoot Rob in Japan"
tags: [ "sford", "geeky-boy", "perl" ]
author: Rob Nugen
date: 2023-12-23T00:51:03+09:00
draft: false
---

(written 00:51 Saturday 23 December 2023 JST)

HI Sford!

Now that AI can help me revive old parts of my brain, I recently
decided (for some value of 'decide') to restore my Perl-powered[1]
journal entries, which my brother designed based on my concept.  He
can regex like a pro, but maybe not a Gohd / Ghod (which way do you
spell it?)

I chatted with Laura in the past two years, but not you in the past decade(s) or more. 

Anyway, I found CgiUtils.pm[2] in this repo and thought I'd give you a holler.
My non-Perl-powered journal is here https://www.robnugen.com/journal/

[1] https://github.com/thunderrabbit/Perl_driven_calendar_journal_from_2012/blob/master/cgi-bin/preformatted_journal_index_writer.pl

[2] https://github.com/thunderrabbit/Perl_driven_calendar_journal_from_2012/blob/master/cgi-bin/CgiUtils.pm