---
title: "God Perfects Nature II"
tags: [ "spirit", "god", "aquinas" ]
author: Rob Nugen
date: 2023-12-08T18:48:21+09:00
draft: false
---

(written 18:48 Friday 08 December 2023 JST)

"God Perfects Nature."

This may have been the intended meaning of something (translated) St
Thomas Aquinas may have said back in the day.

In my case, I take it to mean God perfects human nature, of which my
ego is a part, so God perfects my natural way of being in the world.
