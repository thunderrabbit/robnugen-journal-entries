---
title: "ultimate throws in Yoyogi Park"
tags: [ "anniversary", "ultimate", "sogo" ]
author: Rob Nugen
date: 2023-12-24T16:29:55+09:00
draft: false
---

(written 00:29 Monday 25 December 2023 JST)

Today we finished a year of SOGO Ultimate, which we've been doing from
12:15pm, each week after bootcamp.  Today we played basically until
16:30, got pictures and then headed out right at 4:59pm.

I'm so grateful to play and to do fun throws like rainbows that brush
the tree for a tip and then arrive right where Raymond was standing on
the other end of the field.

Great talking to Cheerios Chihiro about photography and meeting new players
Maya and Niko.