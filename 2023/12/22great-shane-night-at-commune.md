---
title: "Great Shane night at COMMUNE"
tags: [ "shane", "commune", "meetup", "group", "internet" ]
author: Rob Nugen
date: 2023-12-22T23:29:36+09:00
draft: false
---

(written 23:29 Friday 22 December 2023 JST)

Thank you Shane and Andrea for hosting another lovely event with
Thinkers Social Club.  This time at B-Flat COMMUNE near Omotesando
station.

https://www.meetup.com/thinkers-social-club/events/297826647/

Topic for tonight https://www.youtube.com/watch?v=fuFlMtZmvY0 by
https://www.youtube.com/@kurzgesagt

I greatly enjoyed reconnecting with Shane, Andrea, Seb, Florence,
Rohan, Felix.  I met Mike, CJ, Matt, and Norma.

Oh, the sandwich shop with vegan sandwiches was yummy!  I also had
strawberry milk (which was sweet).  I also also had chai from the shop
where the guy has a 33.33 rpm vinyl record playing playing so it's
like his living room.  Sweet!

