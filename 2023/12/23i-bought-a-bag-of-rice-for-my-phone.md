---
title: "I bought a bag of rice for my phone"
tags: [ "rice", "phone", "fix", "dry", "hope" ]
author: Rob Nugen
date: 2023-12-23T13:32:17+09:00
draft: false
---

(written 13:32 Saturday 23 December 2023 JST)

A couple of days ago (Thursday?) my phone started vibrating nearly
continuously with its screen nearly continuously dark.

Carefully thinking about the problem, I realize that it's acting as if
its power button is being held down.  Jess guessed the problem is due
to getting wet in the pool.

At the AU (phone) shop there was basically a bouncer trying to
dissuade me from coming in to bother the staff. Frustrating but
understandable.  They presumably only make money by setting up new or
beefing up existing contracts.

He said I should contact the back-office shop to get a replacement
phone.  He suggested I can use a friend's phone to call the number he
gave me on a tiny scrap of paper.

Walking away from the shop, I recalled a dream with QT where I was
laughing about a line from Pulp Fiction: "If Butch go to Indo China
... in a bowl of rice ..."  Laughing about this quote with QT in my
dream got me thinking of an old hack for wet electronics: putting my
phone in a bag of rice to dry out the phone.

So I bought a bag of brown rice and tucked my phone into the middle of
the dry grains.  How long does this hack take?
