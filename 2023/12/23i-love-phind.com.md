---
title: "I love phind.com"
tags: [ "ai", "phind", "yay" ]
author: Rob Nugen
date: 2023-12-23T00:13:55+09:00
draft: false
---

(written 00:13 Saturday 23 December 2023 JST)

I dunno if Phind has done it again, but I realllly like 'talking' to
it and getting suggestions for things, including writing, fixing code,
and now git repo tweaks.

```
git push -u origin master
```

Okay well this didn't work; I'm trying to get `git push` to push to
TWO repos at the same time.

But now I'm thinking of something even better; using AI and stuff to
help restore my old Perl-powered journal.  I look forward to the
calendrical sidebar that Fred designed back in the day.  Gosh yes that
is great!