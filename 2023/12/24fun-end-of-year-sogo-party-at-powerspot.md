---
title: "Fun end of year SOGO party at PowerSpot"
tags: [ "sogo", "powerspot" ]
author: Rob Nugen
date: 2023-12-24T21:36:31+09:00
draft: false
---

(written 00:36 Monday 25 December 2023 JST)

Lots of fun including conversations, UNO, and chess, plus guessing how
some of their equipment is designed to be used!  I enjoyed getting to
know a bit more about Mimi and her job as an elementary school
teacher.  In enjoyed getting to know some of the SOGO leaders a bit
better.