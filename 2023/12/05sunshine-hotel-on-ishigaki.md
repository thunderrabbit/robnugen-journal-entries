---
title: "Sunshine Hotel on Ishigaki"
tags: [ "vacation", "jess", "sunshine", "ishigaki", "december" ]
author: Rob Nugen
date: 2023-12-05T16:32:54+09:00
draft: false
---

(written 16:32 Tuesday 05 December 2023 JST)

Jess and I have been on Ishigaki since Friday, and enjoying almost
every bit.

Great fun in the pool in the rain yesterday or so, plus walking to
Family Mart and finding Sunshine Cafe has a nice owner and great vegan
food (two dishes: taco rice, and vegan wrap.  Jess preferred the first
and I preferred the second).  They also had some vegan desserts
including an amazing raw cheesecake that was heavenly.  I could chow
about ten pieces, I'm sure! (they were small)

The hotel is nice but NOT quiet; sounds can be heard through the
doors, echoing through the halls.  The vegan dinners have been
amazing.  Just gotta schedule them 24 hours in advance.