---
title: "Attending Jess webinar on financial abundance"
tags: [ "jess", "abundace", "money" ]
author: Rob Nugen
date: 2023-12-27T18:02:44+09:00
draft: false
---

(written 18:02 Wednesday 27 December 2023 JST)

Book A Free Clarity Call: https://calendly.com/lifeinconfidence/clarity-call-with-jess-x-goh

Enrol Into Divine Prosperity: https://www.lifeinconfidence.com/divineprosperity

Attend Full Moon Ancestral Trauma Clearing Circle: https://www.lifeinconfidence.com/ancestraltraumaclearingcircle

