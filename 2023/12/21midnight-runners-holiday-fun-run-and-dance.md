---
title: "Midnight Runners holiday fun run and dance"
tags: [ "mr", "holiday", "run", "barefoot" ]
author: Rob Nugen
date: 2023-12-21T23:54:31+09:00
draft: false
---

(written 23:54 Friday 22 December 2023 JST)

Great time running barefoot 5km with Midnight Runners Tokyo!  Special
thanks to the leader team for 元気な leading each time and really
bringing out the best in all of us!  Special thanks to my feet for
surviving the evening and for almost surviving unscathed haha.  The
music was super bumpin; I think I should carry earplugs with me in my
back pack.  So great but I'm lolol old.  I hope to swoop up some
photos and post here when they come out.