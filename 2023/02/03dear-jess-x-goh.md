---
title: "Dear Jess X Goh"
tags: [ "jess x goh", "email" ]
author: Rob Nugen
date: 2023-02-03T19:20:46+09:00
draft: false
---

(written 19:20 Friday 03 February 2023 JST)

Dear Jess X Goh

I'm replying via email after you messaged me on Meetup.

You wrote:

    Hi Barefoot Rob!

Hello Jess X Goh!

Thank you for reaching out! 

    I noticed you share grounding and full moon meditation events and
    I would love to find out if you would be open for me to share my
    online full moon trauma clearing ceremonies, free webinar to
    overcome the impacts of childhood trauma and a face to face
    weekend transforming trauma workshop to your beautiful community?


I would like to get to know you and your work first before
recommending you to any of my community.
 

    If you like what I'm about and the kind of work that I do to
    support humanity to make peace with their past, I'm happy to
    create an affiliate link for you to help contribute towards your
    meet up costs.


Having affiliates is a brilliant way to get the word out.  Good on you
for moving your business to this level.  :-)
 

    You can learn more about me and my work here:
    https://www.lifeinconfidence.com.


I had a look; your website seems quite nice!
 

    Also if you feel these events would be of help and interest to
    your community and would like to see how this goes temporarily
    before having me on as an ongoing event host, you can add me as an
    event host to share in your meet up group or share the event on my
    behalf. You can communicate to me and choose to stop me from being
    an event host at anytime.


I admire your willingness to ask!  I will say no to having a co-host
on Meetup for now until we get to know one another better, at least.
 

    What are your thoughts?

I tend to be easily swayed by attractive confident women, so it's best
for me if I build a friendship with you first instead of just pouring
money your way trying to "make mommy happy" as a way to solve my inner
child issues.  ;-)

Even so, I did fill out the inner child trauma quiz and had a quick
glance at the video.  It looks quite thoughtfully done.

I am happy to have a chat or even a series of conversations with you
as a peer; including perhaps each doing some deeper facilitation in
mutual support of our respective inner children.  :-)

Please allow me to start the conversation: After doing inner work
since 1997 with ManKind Project, plus starting a men's group in Tokyo
and growing it for the past 7 years, I'm beginning a new growth path:

I am just recently finishing up Cuddle Party facilitator training,
with the long term vision of improving communication and consensual
physical touch in Japan.

I'll lead my first Cuddle Party with men only, inviting men from my
men's group in Tokyo.  Because the concept of Cuddle Party could
easily be misconstrued in Japan, I'm starting with men I trust to give
candid feedback on my presentation.

The first semi-public Cuddle Party has been published on my
low-traffic website here:
https://www.robnugen.com/en/events/2023/03/11cuddle-party-event-in-tokyo/
I've just started inviting people to it, focusing first on those I
know well.

I feel lots of fear coming up, as well as excitement, so I trust it's
the right way to go.

Have a brilliant day and lovely weekend.

blessings
- Rob


##### 18:28 3 February 2023 JST

    Wow you have a great memory of Bondi Beach! Lol, yes I absolutely
    love living there! It's summer here and super hot!

    I definitely would love to go to Tokyo in June to run my trauma
    recovery workshops there, need to see the demand for it is there
    if I connect with the right people for it.

    I love what you do, it's so great to encourage people to ground
    themselves esp in a super busy city like Tokyo! It's been more
    than 2 decades since I've been.

    What are your thoughts on this meet up collaboration and my work?



##### 22:00 Thursday 23 February 2023 JST

Jess and I have met via Zoom 3 or 4 times now; I'm really enjoying
chatting and learning more about improving my ability to "sell myself"
and my gifts.

I look forward to meeting her in Tokyo in June!

https://www.lifeinconfidence.com/