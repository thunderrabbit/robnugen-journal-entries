---
title: "Just contacted Yoga.SunLily about doing my workshop there"
tags: [ "sunlily", "cuddle party", "first" ]
author: Rob Nugen
date: 2023-02-23T22:44:34+09:00
draft: false
---

(written 22:44 Thursday 23 February 2023 JST)

I just contacted https://sunlily.work/ about renting their レンタルス
タジオ on March 11th for my first public-facing
[Cuddle Party event](https://www.robnugen.com/en/events/2023/03/11cuddle-party-event-in-tokyo/).
I have reserved the room in Shibuya as a backup, but the SunLily
location looks much better.

Thank you [Jess X Goh](https://www.lifeinconfidence.com/) for finding
it and asking about the location!

I'll check out the location soon to help her decide if it will work
for her
[Trauma Transformation
workshop](https://www.lifeinconfidence.com/ttwtokyo)
in June
https://www.lifeinconfidence.com/ttwtokyo

https://www.lifeinconfidence.com/innerchildhealingcirclebarefootrob