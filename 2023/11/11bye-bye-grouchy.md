---
title: "Bye bye, grouchy"
tags: [ "anger" ]
author: Rob Nugen
date: 2023-11-11T19:15:32+09:00
draft: false
---

(written 19:15 Saturday 11 November 2023 JST)

I remember exactly when and where I last punched my brother.  He went
down flat after I punched him in the lower back.  I was shocked at my
anger.  I was so happy when he leapt up ready to fight back.  I calmed
him down and never hit him again.

Tonight I lost my shit and yelled at my acquaintence who kept talking
when I wanted her to stfu.  I had already requested a few times, but
still while in character. I hope/trust she's okay, especially given
she said she was and she understood and it was good for her to see it.

I had a talk with myself afterward: brother, you need to cut that shit out.
