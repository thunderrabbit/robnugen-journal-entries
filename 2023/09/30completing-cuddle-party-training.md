---
title: "Completing Cuddle Party training"
tags: [ "cuddle", "party", "training", "complete" ]
author: Rob Nugen
date: 2023-09-30T11:47:39+09:00
draft: false
---

(written 11:47 Saturday 30 September 2023 JST)

My name is Rob Nugen.

As an American, I am used to sharing hugs very regularly.  As an
American in Japan, I lead Cuddle Parties to create a safe place for
humans to connect in a consensual manner.
