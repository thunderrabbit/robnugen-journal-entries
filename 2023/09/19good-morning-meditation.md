---
title: "Good morning meditation"
tags: [ "morning", "meditation" ]
author: Rob Nugen
date: 2023-09-19T08:37:36+09:00
draft: false
---

(written 08:37 Tuesday 19 September 2023 JST)

While sitting in meditation this morning on the fallen log outside my
sliding glass back door, I experienced  a few bug bites, some phantom,
some leaving mosquito-bite-sized lumps which should be gone shortly.