---
title: "Declutterization"
tags: [ "journals", "book two", "byebye" ]
author: Rob Nugen
date: 2023-09-18T22:12:09+09:00
draft: false
---

(written 22:12 Monday 18 September 2023 JST)

I decided to write SOMLs based on my old journals and release the
actual books.  Turns out I already transcribed 95% of BOOK TWO, so
that volume is now tossed in the trash as I declutter my space. Just
22.2 books to go haha hmmm.
