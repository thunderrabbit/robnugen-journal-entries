---
title: "Chilling at home on drizzly day"
tags: [ "home", "apartment" ]
author: Rob Nugen
date: 2023-09-09T09:41:07+09:00
draft: false
---

(written 09:41 Saturday 09 September 2023 JST)

I'm chilling at home (apartment in 高石５丁目) on this overcast day.
Sliding glass door is open facing greenery of hill also hosting a
single bamboo growing right up to the sky.

I skipped Mika's New York Yoga today because I woke up a bit late and
decided to work on AB instead.  3 hours of work on AB for a win.  6
hours for amazeballs.

Ready, go!

##### 20:23 Saturday 09 September 2023 JST

Wow 6 hours できました！ Amazeballs!