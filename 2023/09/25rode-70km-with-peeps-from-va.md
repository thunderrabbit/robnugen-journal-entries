---
title: "Rode 70km with peeps from VA"
tags: [ "cycling" ]
author: Rob Nugen
date: 2023-09-25T07:29:33+09:00
draft: false
---

(written 07:29 Monday 25 September 2023 JST)

This was on Saturday; we went up to a lake and rode bikes.  I rented
an e-bike, which was fun.  I swapped with a lighter bike at the end
and was still faster.  Yay!

Also, Zach and I saw three frogs!  Four, if we count the mural.

This morning after SOGO Bootcamp and ultimate yesterday my legs are
quite tired.

Oh and on Thursday 3.5 days ago, I barefootedly joined SOGO night
(evening) 5k (6k) run.  Great energy and great fun!