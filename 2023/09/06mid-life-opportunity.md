---
title: "Mid life opportunity"
tags: [ "life", "wife", "divorce" ]
author: Rob Nugen
date: 2023-09-06T21:47:40+09:00
draft: false
---

(written 21:47 Wednesday 06 September 2023 JST)

They say crisis and opportunity use the same kanji in Chinese.

Lin and I are both much happier being separated.  I cannot say
divorced yet, but we're definitely headed that direction.

I stayed married for a while because I didn't want to get divorced.  I
thought staying together was the right thing to do.

I don't know when things changed, but even a couple of years ago Mr
Magnet was like "why are you two together?"  My only answer then was
related to an ego-centric idea that *I will never get divorced!*

Similar to being headed to the big D, I am thinking of leaving VA and
even JP.  Could be as early as this year.
