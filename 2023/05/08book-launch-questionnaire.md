---
title: "Book Launch Questionnaire"
tags: [ "book", "slow down" ]
author: Rob Nugen
date: 2023-05-08T08:01:31+09:00
draft: false
---

(written 08:01 Monday 08 May 2023 JST)

On Saturday or so, I signed the contract with my publishing company to
get SLOW DOWN done and published.

The earliest date I could find to have our first meeting was Tuesday
May 14th.  Hmm not great but whatcha gonna do?

Yesterday I checked the calendar again and found a date for 6am
today.  Nice!  I grabbed the slot and had a good call with Vince.

He said he had opened up that slot for another client and when I
jumped in on it he had a laugh.  Nice synchronicity!

Now I'm recording some of my answers to their Book Launch Questionnaire:

**What is the theme of your book?**
<br>Nourish your own personal connection with God/spirit/EPP/higher
power/YOUniverse

**What is your title or title options for the book (if you have worked
this out already)**
<br>SLOW DOWN

**Describe your target audience**
<br>age 25-45, who are trying to work out how to better themselves and the world around them

**When Someone has finished reading your book, what do you want them to think?**
<br>"Wow, so my actions *do* make a difference, and I already have
everything I need to move forward!"

**When Someone has finished reading your book, what do you want them to feel?**
<br>Important, Valuable, Inspired, Grounded, Empowered, Compassion, Gratitude, Joy

**When Someone has finished reading your book, what do you want them to do?**
<br>connect with spirit /
reach out to their loved ones /
pet the dog more mindfully /
engage in life more passionately /
get the results an important person gets
