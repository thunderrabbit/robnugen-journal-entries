---
title: "Great time in 芹ヶ谷公園 yesterday"
tags: [ "park", "machida", "kids", "activities" ]
author: Rob Nugen
date: 2023-05-04T10:11:50+09:00
draft: false
---

(written 10:11 Friday 05 May 2023 JST)

Great time yesterday in 芹ヶ谷公園 (Serigaya Park) near Machida
Station, thanks to Yumi and Travis who introduced us to the area!

They have an amazing activity area for kids (and adults) including
things that could potentially be dangerous: hammer, nails, saws,
fire.

There's also a big slide that I want to visit again, next time with
fuzzy blankets or towels so we can whiz down in a flash!