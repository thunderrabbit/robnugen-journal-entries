---
title: "My answers to a funny personality test thing"
tags: [ "orangutan", "stream", "blue" ]
author: Rob Nugen
date: 2023-05-09T12:16:14+09:00
draft: false
---

(transcribed 12:16 Friday 23 February 2024 JST)

My favorite color and why:

Blue: royal, love, bright

My favorite animal and why:

Orangutan: lives in body, joy, climbing

My favorite body of water and why:

Stream: flow, blessing, pure

- - - -

Personality, Perfect mate, Sexuality