---
title: "My favorite radio station, KTRU"
tags: [ "ktru", "radio" ]
author: Rob Nugen
date: 2023-05-19T20:20:58+09:00
draft: false
---

(written 20:20 Friday 19 May 2023 JST)

Yesterday Travis sent me an email with an article and video about
KTRU.

Here's my reply:

Hey Travis!

Wow wow thank you!  For a few months I have been looking for
background music and somehow forgot about good ol' KTRU dot org
streaming through the internettes.

On Thu, May 18, 2023 at 7:30 PM Travis wrote:

> Here are links to a KTRU video and article!

You had me at KTRU!
 
{{< youtube JmOftrE55ms >}}
    
What a great video and great run down memory lane.  I paused it like
ten times to read stickers and look for any I recognized.  None!  I
guess 25 years will do that to a wall of stickers haha

> https://magazine.rice.edu/spring-2023/record-enthusiasm

“Mutant Hardcore Flower Hour,” and “Genetic Memory,” still going!  Nice!

Entertainingly, I am "talking" about KTRU in my backwards written story online.

https://bitbucket.org/thunderrabbit/robnugen-journal-entries/commits/

> The student center is going to be torn down to make room for a new one,
> so the old KTRU station will be replaced with a new one!

That's really great news!  I'm glad KTRU continues to thrive, given
two generations of music lovers to keep the energy rolling.

Rob!
