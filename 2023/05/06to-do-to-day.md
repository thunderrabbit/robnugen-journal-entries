---
title: "To Do To Day"
tags: [ "todo", "today", "list" ]
author: Rob Nugen
date: 2023-05-06T09:14:42+09:00
draft: false
---

#### 09:14 Saturday 06 May 2023 JST

* Create Meetup template for Rinko Park walk
* Post Rinko Park walk on Meetup
* Make namespace for writing templates
* Make it easy to post chapters in Slow Down
* Post 555 story in Slow Down
* Prepare for barefoot walk event
* Go to 新百合ヶ丘駅 (leave at 13:00)
* Lead 新ゆりアートパーク barefoot walk event
* Go to Lilios cafe
* It was closed so we went to Nana's Green Tea Cafe
* Celebrate Life!
