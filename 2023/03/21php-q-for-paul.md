---
title: "PHP Q for Paul"
tags: [ "paul" ]
author: Rob Nugen
date: 2023-03-21T12:12:36+09:00
draft: false
---

(written 12:12 Tuesday 21 March 2023 JST)

    Hello Paul!
    
    Happy Equinox!      I hope you're doing well.
    
    If possible, I'd like to rent your time and PHP brain to help me
    with an upgrade to a PHP codebase.  I'm the only programmer on
    this project that I took over 10 years ago.
    
    It'll probably take 2 to 3 hours of your time (at most), and I'm
    happy to pay half your fee up front (though I don't know your fee
    yet haha oops)
    
    I can show you code on Bitbucket and jump onto a video call to
    share my screen if that helps.
    
    I'm in Japan time, so emails might be delayed until we set
    something up, but I'll adjust to US business hours as needed.
    
    Questions for now
    
    1) Are you interested?
    2) When might you be available?
       (Do you have a Calendly or something for this type of inquiry?)
    3) What's your fee?
    4) Do you have questions for me?
    
        blessings
        - Rob
    
    