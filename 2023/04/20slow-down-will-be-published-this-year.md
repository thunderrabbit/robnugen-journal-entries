---
title: "SLOW DOWN will be published this year"
tags: [ "slow down", "book", "publish", "vince" ]
author: Rob Nugen
date: 2023-04-20T08:05:15+09:00
draft: false
---

(written 08:05 Thursday 20 April 2023 JST)

I just got off a call with [Vince
Warnock](https://chasingtheinsights.com/), introduced to me by
multi-year accountability buddy [Susan
Birch](https://susanbirch.co.nz/about-susan/), with whom I joined
Anna's accountability sessions about 1000 internet years ago.  Vince
sometimes challenges his authors to take an irreversible action,
announce their book will be published e.g. this year.

When I heard that, I was like "heck no!"

This reminds me of the time I decided to get involved on campus during
university.

I just want to hella hide and hold back.

"What has the gene pool gained by having a guy float around and decide
what's not good enough to do?"

I'm stalling.   Fffffffffffffffff

##### 20:14 Tuesday 06 February 2024 JST

Yeah turns out the publisher was a bit more of a 'talker' than a
publisher.

He kept saying "my team" blah blah but it turned out to just be him
and ChatGPT or something.

Very frustrating