---
title: "Enjoying Waking Up course"
tags: [ "waking up", "meditation" ]
author: Rob Nugen
date: 2021-12-15T11:58:23+09:00
draft: false
---

(written 11:58 Wednesday 15 December 2021 JST)

On reddit/r/meditation, /u/Evergreen16
[posted they enjoy Waking Up](https://www.reddit.com/r/Meditation/comments/rdy6an/not_affiliated_at_all_but_if_you_are_looking_for/).
Enough people were like "yep" that I had a listen.  Yep.  Good stuff.

https://app.wakingup.com/