---
title: 'I missed Santa''s appearance'
author: 'Rob Nugen'
date: '2021-12-25T17:55:49+09:00'
---

Today Lin and I went to a kind of grassroots community center and I got to play Santa Claus in pajamas for about 20 little kids who were there.

I felt really nervous at first and went through stages of being okay and uncomfortable.  The while thing lasted about thirty minutes before Santa left and I hopped out the window and came in the front door all like "where's Santa?"

Other performances included singing Christmas carols (one verse each), oh and singing a Japanese version of the Hungry Caterpillar book.

The last entertainment consisted of homemade candy suddenly raining down from the loft into the main area.  Most of the kids loved it, scooping up as much as they could.


