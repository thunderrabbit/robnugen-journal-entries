---
title: "Slow down"
tags: [ "curious", "eri" ]
author: Rob Nugen
date: 2021-12-03T06:14:41+09:00
draft: false
---

##### December 1, 2021 	Eri 	Hello from Eri - MyLanguageExchange.com

Hi Rob,

I may visit Tokyo soon.
Are you interested in meeting in person?

December 1, 2021 	You 	Hello from Rob Nugen - MyLanguageExchange.com

Hi Eri!

Definitely would be nice to meet in person.

When do you think you might be here?

I lead some small casual barefoot walking events and attend some
events in Yoyogi Park.

https://robnugen.com/en/events

- Rob




December 2, 2021 	You 	Hello from Rob Nugen - MyLanguageExchange.com

I'm curious to meet you via Zoom or something first so we can talk a little bit about your trip and what you're hoping to do in Tokyo. I'll be in yoyogi park this coming Sunday from 10:00 a.m. until about 4:00 p.m. or whenever it gets dark. Add me on line using my QR code on my website https://www.robnugen.com/contact
December 2, 2021 	Eri 	Hello from Eri - MyLanguageExchange.com

Hi Rob, Thanks for your message! No problem with you not being vaccinated, it's nice that you can stand up for what you believe in. If you're interested, what days in the next week or 2 would you be available to meet? Regards, Eri
December 1, 2021 	You 	streaking past omnicron

Hi Eri! I haven't and won't be vaccinated. https://members.vaxcontrolgroup.com/verify/KYVUYAS8FQG8STKHPY2TDQ Streaking can be fun but I'll stick with barefooting for now. :-) - Rob
December 1, 2021 	Eri 	Hello from Eri - MyLanguageExchange.com

Hi Rob, Thanks for your message. How about we skip the barefoot walking and just streak?! lol Just kidding. Btw, what are your thoughts on Omicron? And I'm not really concerned one way or the other, but did you get vaccinated? See you, Eri


December 2, 2021 	Eri 	Hello from Eri - MyLanguageExchange.com

Hi Rob, Thanks for your message. >something first so we can talk a little bit about your trip and what you're hoping to do in Tokyo "Let's talk a little bit about your trip and what you're hoping to do in Tokyo" is the kind of question I expect from a border control officer when I go to visit a foreign country. Are you protecting Tokyo from me, Officer Nugen? lol. My plans in Tokyo are really none of your business, are they, Officer Nugen. Officer Nugen, can I speak with your supervisor? lol Btw, I notice from your website you're into meditation and channeling. I'm curious, when you're channeling, how do you know if the entity you're channeling is benign? Regards, Eri
