---
title: "margins 2021"
tags: [ "ascii", "silly", "margins", "text", "news" ]
author: Rob Nugen
date: 2021-12-17T21:53:11+09:00
draft: false
---

(written 21:53 Friday 17 December 2021 JST)

On [ascii-collab.app](https://ascii-collab.app/?x=1024&y=1024), I just wrote the following:

                                                                                               
    I scrolled all the way down here to x=1024&y=1024 but forgot what I had planned to write.  
    Today is December 17th, 2021, and it's been a pretty good day so far, from what I recall.  
    Reading this block of text, you will find the lines feature automatic self-justification,  
    which lends itself to a pleasant reading experience. The only trouble is that starting my  
    text down here, it will take a long time for people to find it. Congratulations on taking  
    the time to scroll all the way here to find this block of text! Perhaps we should try and  
    make a river flowing down the middle of the text. Okay, so right back there, I hope I can  
    create what they call a "river" of spaces, giving the illusion of gaps in the typesetting  
    which cause the eye confusion with an emphasis on vertical instead of the horizontal flow  
    characterized by text in many languages. In China and Japan one can often find vertically  
    aligned kanji. Those languages do not use spaces, which precludes an attempt for creation  
    of rivers in those writing systems. This river is not particularly as clear as I think it  
    can be, but it looks like I have an inadvertantly created river above. See? Rising up and  
    to the left of the capital S used in the question above, leading a line diagonally toward  
    the word "Congratulations," which seems a fitting place for such a river to lead. Anyway,  
    I may continue this later, but for now, will copy the text to my own site for safety just  
    in case something happens to this self-justifying text created exactly two weeks plus one  
    day before 2022. Oh, by the way, I am curious how long until you found this text. Can you  
    sign and date the logbook on the right? It's just perfect timing indeed that you are here  
    in the mysterious future on this mysterious ascii collaboration website, when there could  
    certainly be other things waiting eagerly for you to find them. Given that you have taken  
    the time to read this far down, I think finishing the long diagonal river so it connnects  
    with the corner would be a worthy goal, and quite challenging as the space after the line  
    of whitespace comes closer to its own goal, right down there which you can easily see now  
    that it is finished, but NOW as I write, I wonder how in fact I will find the word length  
    for the last couple of lines. Six lines remain in this quest. Let's talk about some other  
    topic. Right now COVID-19 fills the news and NFTs have somewhat taken over as crypto coin  
    excitement ebbs and flows. Bitcoin price is lower recently than before, but still you may  
    be wishing you had bought into it back when first hearing about it. Anyway, 1 BTC will be  
    higher or lower in the future, but looking up the price now, it's at 41,700 Euros which I  
    certainly cannot afford at the moment, but later, I wonder if we will look back and sigh   
    regret or relief that the price has risen to huge highs and we finally finished the text?  

