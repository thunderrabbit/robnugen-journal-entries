---
title: 'great time and ice cream with Travis and them'
author: 'Rob Nugen'
date: '2021-12-26T16:33:09+09:00'
---

Lin and I are heading home from Travis place.  Great to see them again; first time since BBQ fun in fall and first time since he became a double daddy.

We ate ice cream with chocolate syrup and optional 🍒 cherries after I had leftover chicken (not very leftover: more like waiting for me to arrive after SOGO.)

Speaking of waiting, Travis and his oldest were waiting for me at the station when I arrived.  Nice to have their great support, plus having the great timing precision of 🚃🚃🚃🚃🚃🚃🚃🚃 trains in Tokyo.

We went for a walk around their neighborhood and talked about computer stuff etc; I'm always intrigued by Travis computer stories.

Thank you guys; happy holidays and see you in 2022!
