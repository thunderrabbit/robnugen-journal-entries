---
title: 'last SOGO Bootcamp of 2021'
author: 'Rob Nugen'
date: '2021-12-26T13:26:40+09:00'
---

Bootcamp had about 40 people in attendance.  No Alex nor Valeriya, but Jesse, Kino, Hiro, Naoki were there, plus Adrian, Kaito and Adam, all my first time to meet them.

I did not join the run today but did the core section twice.  My feet are too torn up to want to run through the rocks and sticks under the trees.


