---
title: 'dream elevator doors not closed'
author: 'Rob Nugen'
date: '2021-12-24T09:29:43+09:00'
---

<p class='dream'>I was on an elevator and noticed the doors stayed open when the car started moving so I immediately hit the emergency stop button and some guys came to see what was the problem and I told him the doors hadn't closed before the car started moving and I didn't know why.  It didnt happen again while they were there but I explained in detail that I had seen a movie on YouTube about this and now even the fireman key cannot overwrite that much so they understood where I was coming from.</p>

Even though I had done all that explanation I didn't realize that it was a dream.
