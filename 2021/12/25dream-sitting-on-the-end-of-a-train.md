---
title: 'dream sitting on the end of a train'
author: 'Rob Nugen'
date: '2021-12-25T08:01:48+09:00'
---

<p class='dream'>Sitting on the end of a train car as we were headed into a station and I saw a sign that indicated Train 131 to Austin was late and then my car turned around and the coupler started to come out from between them and then I was like oh shit and I lifted my feet up. </p>
