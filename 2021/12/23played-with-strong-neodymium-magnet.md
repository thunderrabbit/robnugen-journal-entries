---
title: 'played with strong neodymium magnet'
author: 'Rob Nugen'
date: '2021-12-23T16:55:23+09:00'
---

#### 16:30pm Thursday 23 December 2021

After my lessons at SCC, Naoko showed me their relatively large neodymium magnet.  It was big enough to be marked with a sign that said danger, but not so large that we couldn't play with it using our bare hands.

I forgot to take pictures, but I've asked her for some, if possible.

They had some various ferrous and non ferrous items to play with near the magnet, including a 2D card with small magnetic rods inside that let me visualize the magnetic field.

