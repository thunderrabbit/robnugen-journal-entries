---
title: 'met Shuhei'
author: 'Rob Nugen'
date: '2021-12-25T17:56:41+09:00'
---

Most surprising and magical for me was meeting an English speaking Japanese man who is in an industry similar to Mankind Project.  Their brand is more specifically for leadership, and they have trainings, but no long term group programs like we do.

I imagine we can figure out some way to support each other.
