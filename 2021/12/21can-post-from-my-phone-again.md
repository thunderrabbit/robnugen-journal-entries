---
title: 'can post from my phone again'
author: 'Rob Nugen'
date: '2021-12-21T01:14:16+09:00'
---

Quill couldn't find my micropub links because my site refreshes to /en or /ja, and the root path didn't have the correct links in it.

Now it does, so I can post from my phone again.  😁

