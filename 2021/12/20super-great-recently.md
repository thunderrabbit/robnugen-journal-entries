---
title: 'Super great recently'
author: 'Rob Nugen'
date: '2021-12-20T22:35:12+09:00'
---

I'm scheduled to speak at two events (so far) in 2022!

One is for https://www.en-joi.com/wolfpack; I'll speak online on March 14th.

The other is for KSSG (Kanagawa something something Group)(?); I'll speak in Yokohama on July 9th.

Wooooo!!

AND, Sho just put together a video for my December 30th walk!

https://www.youtube.com/watch?v=TkplKUngeFE

{{< youtube TkplKUngeFE >}}
