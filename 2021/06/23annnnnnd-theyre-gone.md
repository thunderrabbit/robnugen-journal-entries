---
title: "annnnnnd theyre gone"
tags: [ "sessions", "helpful", "surprised" ]
author: Rob Nugen
date: 2021-06-23T19:33:43+09:00
draft: false
---


##### 23:47 23 June 2021 JST

Hi Rob,

I love seeing the new landing page for your site. Looks really good.

I discovered that I need total a break from facilitating for you. I am making some super personal progress and it is requiring every last bit of my energy.

It has been an honor to watch your confidence and focus increase.  Maybe doing this without me can further solidify the fact that ‘you’ve got this’.
I am interested in receiving updates, if you find that sending me helps you as well.

Please do contact me in a month or so if you are interested in having another meeting.

I am pretty excited for both of us, really.  I see a lot of hope in each of our progress.

Take care,


##### 0:41 Friday 30 July 2021 JST

Hi Rob,

It has been a month. Do you still want to check in? What have you learned this month?

Cheers,


##### My reply 19:35 Friday 30 July 2021 JST

HI _____

I felt a lot of emotions when our meetings stopped.

I've learned working with you:

* Helped me a lot
* Meant a lot to me
* Was worth money
* Felt empowering
* Felt exciting
* All of the above

I learned how much I crave answers to "what to do now?"

I applaud, respect, and support your choice to take care of yourself.

I learned the importance of systems instead of goals.

I'm developing systems for posting content to my website.

I would love to work with you again, and want to more properly appreciate the support you offer.

It's apparently been a month.

What have you learned?

Do you still want to check in?

    Blessings
    Rob

##### 07:34 Sunday 01 August 2021 JST

Rob,
<br/>Your insights look good.
<br/>I have had a month of clarity and fun developments.
<br/>It saddens me to let our relationship go, but I realize that I am no longer interested in the emotional dynamic, the old-school female feels the emotions while the man feels calm and superior. I am so glad to be free of codependent relationships.
<br/>
<br/>(snip)
<!--
<br/>I have to say the following. It feels wrong not to:
<br/>Rob, you’ve got integrity. Use it. Face your shit. Talk to your wife about the fact that there is no adulthood as long as your toxic mother-in-law is in the house. Tell her either the mother goes or you go. And then go home to Texas. Bring Lin if that works. Ditch the mkp and its manipulations. Take care of your blood family. Make peace with your ghosts. Get a job with benefits. Deal with what is going on here in the States. Be part of the solution, not mired in the toxicity of the problem.
-->
<br/>
<br/>I’d be lying if I said I wasn’t sad to say goodbye. The work we did together got me to the point that I don’t need that type of relationship anymore. But I know that the energy freed by letting go will enliven my life.
<br/>
<br/>Best,
<br/>______



`the man feels calm and superior`

Wait, what?  I never felt superior nor implied that I might.

##### 20:31 Monday 09 August 2021 JST

Hi Rob,

I didn’t want to write you again after all I said in that last note,
but after I saw the recent post on Facebook from your chakras class,
it is only fair:

I was super pissed when you told me you were taking that chakra class
and paying $1000 for it, when I had been giving you multiple sessions
a week and not receiving a dime.

##### 23:49 Monday 09 August 2021 JST

HI _____

When we started our arrangement, you said you would not be comfortable
accepting money because you were not sure of your skills.

After we took a break, you asked what I had learned.  I said your
support was worth money.  Maybe I should have been more clear to say I
wanted to pay you to work with me.

However, after your reply (when I asked what you had learned), you
said you didn't want to continue our interactions.

The other things you said in that email, plus your email below make it
clear to me I don't want to continue our interactions either.

In my way of seeing things, I offered you money twice.  I remember I
was looking for someone who could work on commission.

You chose not to receive money at the time I first offered.

You chose to not work with me the second time I (essentially) offered you money.

Your skills are worth plenty.  You will find your way forward.  I think we both will.

  Blessings
  Rob

##### 6:07 Tuesday 10 August 2021 JST

Rob,

Thank you for this reply.

Would you do me a favor?

I want to know what is going on that I changed my opinions on some
things with you so drastically.  I was triggered by something.  Would
you be willing to meet with me to help me to determine what is going
on?

##### 8:20 Tuesday 10 August 2021 JST

HI _____

Yes; I can do that.  I am curious as well.

I have 6pm to 7pm Central US time available for tomorrow - the rest of the week.   Does that work for you?

If not, please look for a good time on my calendly link https://calendly.com/robnugen/

##### 10:23 Tuesday 10 August 2021 JST

Rob,
<br>I appreciate it.

I am going to sleep on this and get back to you about a time.

#### 8:38 Wednesday 11 August 2021 JST

Rob - I have been on zoom since 8. It is 8:38. I see you are on Facebook but you are not responding.
