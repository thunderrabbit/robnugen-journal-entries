---
title: "Met Petya, Denise, Neil, Missy in Hobgoblin"
tags: [ "hobgoblin", "thanksgivin", "roppongi" ]
author: Rob Nugen
date: 2021-11-25T21:35:53+09:00
draft: false
---

(written 00:35 Friday 26 November 2021 JST)

I arrived barefoot at Hobgoblin and one of the employees asked if I
had shoes.  She didn't ask me to put them on; just asked if I had
them.  Huh.

I didn't see my group, but turns out they were already at the table.
We found each other via texting.

Great to hang out with Denise, Petya, Neil, and Missy.  Lin couldn't
join, but we had dinner buffet and got free drinks at the end because
of some confusion on non-free drink earlier.  yay!

I got a jaegermiester (sp) because I requested a sweet drink so that's
what was recommended.  It was okay.

Also got some ice cream!  It was greeattttt!

Neat to hear stories of odd jobs: mail delivery, host / hostess.  We
talked about Bitcoin, Etherium, Urbit, Minecraft and Meta.

Everyone left before Missy and I so we hung out and talked about her
potential next steps.