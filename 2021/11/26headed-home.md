---
title: "headed home"
tags: [ "nice" ]
author: Rob Nugen
date: 2021-11-26T17:41:35+09:00
draft: false
---

Relatively nice productive time with Dipendra while Tariq worked with
Prajal on audio.  I have been teaching Dipendra how to use git (via
Github app on his machine) and  am now reasonably sure he will be able
to work with Prajal to update the audio files in the app to new ones
that Prajal plans to make.

Also, Dipendra is going to try to figure out how he can position the
buttons in various .tscn files using Godot's built-in anchors,
something I have not been able to figure out.