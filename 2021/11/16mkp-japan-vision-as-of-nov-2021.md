---
title: "MKP Japan vision as of Nov 2021"
tags: [ "mkp", "japan", "vision" ]
author: Rob Nugen
date: 2021-11-16T18:12:47+09:00
draft: false
---

(written 18:12 Tuesday 16 November 2021 JST)

## MKP Japan vision as of 16 Nov 2021

Context: twelve men are just finishing MKP Japan’s first Circle of Men training.

### BIG PICTURE: Use MKP USA model

1. Welcome new men in a good way.  (Online Open Men’s Group (OOMG))
2. Support emotionally mature, accountable, compassionate men
   OOMG -> Men’s Work -> Circle of Men -> PIT -> NWTA -> etc etc
3. Offer leadership opportunities for committed men
4. Transcreate processes into Japanese language/culture


### IMMEDIATE TERM 

1. Reconnect with Nathan and other men in our original 7pm Tuesday meeting
2. Share Circle of Men processes with existing men
3. Only use Circle of Men processes for regular meetings
4. Schedule next N weeks of buddy calls.
5. Set up King Calendar for upcoming meetings
6. Establish membership system to build funds to increase our reach

### SHORT TERM (target March 2022)

1. Have two (or more) groups meeting regularly
2. Transcreate MKP USA Open Group into Japanese (Rob, Richard, Fabien, Eli have attended those)
3. Regular posting to newsletter / blog
4. Strategically post videos of member interviews 
5. Have a mentee relationship with an established MKP community
6. Encourage committed men to attend NWTA
   (so far: Rob, Kevin, Michael, Mark)

### MEDIUM TERM

1. Create a legal entity for MKP Japan
2. Multiple regular weekly meetings online / face to face / Tokyo / Kyoto / Hiroshima
3. Fill Men’s Group Coordinator role to foster community between groups
4. Bilingual native Japanese men attend NWTAs outside Japan
   (OOMG -> Men’s Work -> Circle of Men -> PIT -> NWTA)
5. Featured in local media English and Japanese

### LONG TERM:

1. Host NWTAs in Japan
2. Lead other MKP trainings in Japanese language
3. Replace Rob with bilingual New Warrior
4. Offer mentoring into younger community in Japan
 


