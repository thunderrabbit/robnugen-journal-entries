---
title: "Met Billie at Hobgoblin"
tags: [ "billie", "missy" ]
author: Rob Nugen
date: 2021-11-26T00:33:28+09:00
draft: false
---

(written 00:33 Friday 26 November 2021 JST)

On the train headed home after meeting Billie who came up to Missy to
ask about her tattoos while we were hanging at Hobgoblin.  Neat to see
her creating her own brand of clothes.

https://instagram.com/bellisaxclothing
