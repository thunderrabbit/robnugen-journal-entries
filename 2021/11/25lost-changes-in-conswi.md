---
title: "Lost changes in ConSwi"
tags: [ "dangit", "ugh", "git", "mouse" ]
author: Rob Nugen
date: 2021-11-25T18:16:57+09:00
draft: false
---

(written 18:16 Thursday 25 November 2021 JST)

Ugh.   Because I cannot left click on this laptop without an actual
mouse (or haven't figured out a way to do it), I miss clicked my git
GUI on Atom and unstaged all my changes.   Dammit.  I had done some of
them weeks ago, but wasn't ready to commit because I wasn't ready to
commit.

##### 18:23

oh yay I just discovered Fn two-finger click did a right click.