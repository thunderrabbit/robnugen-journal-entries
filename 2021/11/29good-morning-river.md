---
title: "good morning river"
tags: [ "river sayaka" ]
author: Rob Nugen
date: 2021-11-29T05:59:59+09:00
draft: false
---

(written 05:59 Monday 29 November 2021 JST)

Just arrived at Blue Cafe for river cleanup.

##### 08:02 Monday 29 November 2021 JST

Sayaka arrived at 6:00. 上村さん, ゆうこさん, さやかさん, and I
started river cleanup.  川のまみこ joined us as we were near the weir
(not sure if it's a weir) where we cleaned up the fish stairs a bit.

On the way out of the river, we met Steve,  who had cleaned up a
vacuum cleaner.