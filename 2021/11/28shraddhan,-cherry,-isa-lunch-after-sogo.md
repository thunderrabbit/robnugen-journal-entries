---
title: "Shraddhan, Cherry, Isa lunch after SOGO"
tags: [ "shraddhan", "isa", "cherry", "lunch" ]
author: Rob Nugen
date: 2021-11-28T15:25:46+09:00
draft: false
---

Shraddhan joined me for lunch in the park after SOGO.

Had a great time sitting in the sun with Isa and Cherry who joined SOGO fitness for their first time today.

We talked about work and hobbies and life purpose, joys and living the dream of doing what you love for work.

Cherry loves to edit video and lead Zumba classes.  Isa loves playing Ukelele and teaching / empowering kids.

Shraddhan and I walked around the park, enjoying seeing the sites and sounds.
