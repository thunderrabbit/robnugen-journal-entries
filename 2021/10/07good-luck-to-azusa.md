---
title: "Good luck to Azusa"
tags: [ "scc", "azusa", "quit" ]
author: Rob Nugen
date: 2021-10-07T09:44:01+09:00
draft: false
---

(adapted from BOOK TWENTY 09:44 Saturday 25 December 2021 JST)

##### 2PMish Thursday 7 Oct 2021（木）

Wow Azusa just came in to say she's quitting.  She's been with us a
long time and has tried hard to guide people in the museum.  Best of
luck on future endeavors!

In other news, the museum peeps apparently took my(?) advice and got
their own domain for their website, but are still using their old
subset of T corporate website, including Global header even though the
museum ain't global.