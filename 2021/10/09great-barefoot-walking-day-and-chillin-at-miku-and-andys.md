---
title: "Great barefoot walking day and chillin at Miku and Andys"
tags: [ "barefoot", "walk", "event", "miku", "andy", "frema", "jessica", "lin" ]
author: Rob Nugen
date: 2021-10-09T09:47:58+09:00
draft: false
---

(adapted from BOOK TWENTY 09:47 Saturday 25 December 2021 JST)

##### 10PMish Saturday 9 October 2021（土）

On the train headed back home from Andy and Miku's place after seeing
Jessica and Frema off on the northbound train at 向ヶ丘遊園駅.

Andy, Miku, Jessica, Frema, Robert (from Ogikubo), Lin, and I all hung
out in the baseball field near 和泉多摩川駅 sharing stories and then
walked to the other side of the other field to feel/see nice cool
clover and meet テテ the dog. Robert had to head home and we went back
to the first side of the field and decided to come back to the field
on the full moon night October 20th to enjoy the full moon.

Went to Andy and Miku's for chili and chillin.  Andy and Frema talked
music.  Lin and Miku talked houses.  Jessica and I talked about
spirituality, life, universe, relationships, and Law of Attraction.