---
title: "More easily post walking events on my site"
tags: [ "site", "post", "walk", "perl" ]
author: Rob Nugen
date: 2021-10-04T09:29:22+09:00
draft: false
---

(adapted from BOOK TWENTY 09:29 Saturday 25 December 2021 JST)

##### 18:12pm Monday 4 Oct 2021

Great day today in that I now can easily post events on all
platforms.  So far on my site in English and Japanese, Meetup,
Facebook, Twitter, Instagram.  Each posting location has templates for
each event location: Izumi Tamagawa, Yoyogi Park, YomiUri Art Park.

##### 09:32 Saturday 25 December 2021 JST

Since then, I have created "Izumi Tamagawa full moon" templates, and
"Karakida to TamaCenter 3.7km walk" templates, and one for "Rinshi-no-mori
Park" templates for 林試の森公園 near Meguro station.

https://github.com/thunderrabbit/barefoot_rob/tree/master/event_templates/walk_and_talk

https://github.com/thunderrabbit/barefoot_rob/blob/master/generate_events.pl

