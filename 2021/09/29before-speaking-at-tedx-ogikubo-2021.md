---
title: "before speaking at TEDx Ogikubo 2021"
tags: [ "tedx", "speaking", "before" ]
author: Rob Nugen
date: 2021-09-29T10:29:31+09:00
draft: false
---

Today is TEDx Ogikubo's first event, and I'll be speaking around 5:45pm.

Robert the organizer has repeatedly assured us we are creating video
recordings, which happen to be done while livestreaming.  If
necessary, just pause, take a breath, and continue.  We'll clean it up
in post edits.

##### 09:54 Thursday 30 September 2021 JST

It was great!!  I am super glad everything went so well for me on
stage.  I couldn't see much of the audience at all.  Could just see
Lin and Soness on the front row, cheering me on!

{{< youtube LJzb5n9ldP8 >}}
