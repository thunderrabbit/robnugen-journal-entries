---
title: "Found ixi music, analyzing Nine Inch Nails songs"
tags: [ "ixi.music", "nin", "yay" ]
author: Rob Nugen
date: 2021-09-30T09:22:37+09:00
draft: false
---

(transcribed from BOOK TWENTY 09:22 Saturday 25 December 2021 JST)

#### 22:55pm Thursday 30 September 2021（木）

Noice! Today I found [ixi.music on IG](https://www.instagram.com/ixi.music)
but more importantly [Ixi Music on
YouTube](https://www.youtube.com/channel/UC7SfnX0k6ucXyYKtNwEnTog)
where a woman named Ixi from Austin
analyzes NIN music, something I have
wanted to hear for a long time.
I basically
gave up
because I thought it was
something I could never have.