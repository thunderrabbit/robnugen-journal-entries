---
title: "playing with nocodb"
tags: [ "nocodb", "templatifier" ]
author: Rob Nugen
date: 2021-09-24T16:52:58+09:00
draft: false
---

1. https://www.nocodb.com/
2. One-click deploy on Heroku
3. Ignore Salesforce stuff
4. visit new URL https://gentle-chamber-67556.herokuapp.com
5. Create username
6. Create new project `robone templatifier`
7. Rest APIs

