---
title: "TEDx script play"
tags: [ "tedx", "script", "perl" ]
author: Rob Nugen
date: 2021-09-16T19:35:10+09:00
draft: false
---

    I’m standing on a train platform. Lost. looking at my phone.
    Two station attendants come up to me. Holding socks.
    The guy's looking at my bare feet asking, "Are you okay?"
    
    Yeah, I say. I'm fine. And then I realize,
    "Ohhhhhhh, they think I am going to kill myself."
    
    HI!  I'm known as Barefoot Rob.
    I have walked around barefoot for 21 years.
    In Tokyo, people often come up to me and ask, "Are you okay?"
    Or say "hey, I've seen you on TV."  
    Or "hey, I haven't seen you on TV.  I'm the police. I’d like to search your bag." :)
    
    If the conversation continues, People inevitably ask, 
    "Why are you barefoot?"  I love the feeling of grass between my toes. 
    Free Shiatsu massage with every step?  I'll take it!
    On rainy days, my shoes never get wet!
    
    And what I love most is the sense of .........sense.  
    The awareness. Mindfulness.  Connectedness. 
    To the earth and more importantly, to myself on an emotional level.  
    
    Earlier, while on the train platform, I was feeling lost and confused. 
    The station attendants were feeling worried. 
    
    Taking one’s shoes off is symbolic of suicide in Japan. 
    So there I was, another middle-aged male, 
    standing barefoot on the train platform as trains are passing, 
    in a time where suicides are increasing --even for children--.
    Around the world, there’s a suicide every 40 seconds.   
    
    We have all the self help books in the world, 
    but still something is missing.
    
    As a founder and facilitator of peer-listening groups and 
    having worked in suicide prevention, I’ve found what’s missing...
    is a shared safe space to feel seen and heard; 
    
    Instead, we are isolated.  
    Societally we’ve been taught to not feel our emotions. 
    Or if you do, just ignore them because "nobody wants to hear that."
    
    But if they are ignored, our emotions often drive our behavior.  
    
    I often hear people say
    "I’m overwhelmed."  
    "I’m an emotional roller coaster" 
    "I don’t feel like doing anything" 
    
    With more awareness of our emotions, 
    we can regulate these mood swings, 
    reframe setbacks, and be more happy...or sad!...
    or anything else, if we simply accept whatever we are feeling.    
    
    What’s this got to do with being barefoot?
    
    I could talk about how the Earth makes you feel grounded, 
    the benefits of minus ions and other new-age appeal.
    But I wanna focus on more basic biology and psychology.  
    
    We’ve got 200,000 nerve endings in each of our feet.
    That’s a higher concentration per square centimeter 
    just in the soles of our feet than any other part of the body.
    
    Wearing shoes? We are blocking 30% of our connection to the outside world!
    
    Who here uses the internet?
    
    Who would like to cut their internet bandwidth by 30%? 
    Of course not!  You’d be watching the replay of this talk all staticky 
    like (ehn ehn ehn ehn). 
    People watching at home are like "what just happened? Gotta reset the router..." 
    
    You are reducing your body’s bandwidth by wearing shoes!  
    
    Sure, going barefoot you might get the occasional cut or scratch, .. 
    but you also get access to 52 bones, 66 joints, 
    over 200 ligaments, muscles, and tendons 
    plus the 400 thousand nerve endings.   
    All of these are all designed to help you navigate the world in a good way.
    
    Would you like to feel how they work together?
    
    If so, please stand up.
    Great! Now try to balance on one foot.  
    Now, if you like, take off one shoe, and balance on your bare foot   
    Is it easier to balance with your bare foot or your shoe foot?
    
    For most people it’s easier to balance while barefoot.  
    When your toes can spread out, 
    they have more leverage to help you balance.
    
    Oh, while you are standing.  Let’s play a game!  
    You know rock paper scissors?  Ok rock paper--
    oh hang on! With your feet 
    
    this is rock. This is paper.  And scissors. Ready?  
    
    Rock paper scissors shoot!  
    
    Okay, I’ve got scissors, so if you have Rock you win!  Yay! 
    
    Great job everyone! Thanks for playing! Please have a seat.
    
    How was that?  What did you notice? 
    
    You might have thought, this is fun!  Or felt silly or frustrated, 
    "why are we doing this?!"   
    No matter how you feel, simply noticing, without judgement-- 
    allows the emotions to be acknowledged so they can transform, 
    and you can move on.
    
    So why is it easier to balance while barefoot?
    World renowned physiotherapist Yuji Kitano says 
    
    Wearing shoes that are too tight prevents our feet from adjusting.
    More flexible feet can balance better.
    Every time you take a step, there should be small 
    fine-tuned adjustments between the bones to help you balance.  
    
    If not, you have to balance somewhere else.  
    Ankles, knees, lower back..  They aren’t designed to balance us, 
    and that’s often the source of injuries and body pain.  
    
    If your body is in pain, you tend not to move.
    
    If you can't move? You can’t function. 
    
    Similarly, if your emotions are all bound up, you get emotional pain. 
    Tend not to move. 
    Can’t function. 
    
    So I’m not saying you should walk everywhere barefoot 
    --although I do lead fun barefoot walkshops--
    
    I'm saying, we need to allow freedom and flexibility
    in our physical and emotional lives.  
    
    Taking our shoes off is a physical and symbolic way of opening our senses.  
    Noticing new sensations in our feet helps us notice our emotions in our body.
    
    What are you feeling emotionally right now?   
    
    Notice where in your body you are feeling emotions. 
    
    Let’s take 30 seconds quietly to simply sit with awareness of the body.
    With your eyes open or closed as you prefer.
    
    If your mind wanders, just bring it gently back to your body.
    We’ll sit quietly for 30 seconds, starting now.
    
    Thank you.
    What did you notice? 
    
    Your experience of your physical and emotional reality is uniquely...yours. 
    
    With silence, we can notice and acknowledge our emotions,
    giving them space to transform.
    
    With practice, like in my peer counseling groups, 
    we get more adept; we can adjust with emotions while
    in the middle of conversation
    or argument
    or even while playing rock paper scissors.  
    
    We must notice and acknowledge what’s happening 
    around us and within us 
    so we can make fine-tuned adjustments to feel more stable.
    
    The train station attendants on the platform 
    noticed and acknowledged me.
    
    They went away feeling happy that I was okay.
    I went away feeling happy after finding my train ;) 
    ...And feeling grateful for their awareness.
    
    Our bodies give us information to help navigate the world.  
    
    Notice. Acknowledge.  Feel your soles.


##### 20:08 Thursday 16 September 2021 JST

Above is my TEDx final draft, with hard line breaks to make it fit
easily without soft line breaks.