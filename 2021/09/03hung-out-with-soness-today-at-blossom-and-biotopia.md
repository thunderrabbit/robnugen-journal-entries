---
title: "Hung out with Soness today at Blossom and BioTopia"
tags: [ "soness", "day", "chillin", "fun" ]
author: Rob Nugen
date: 2021-09-03T20:28:54+09:00
draft: false
---

Today I met Soness at Chigasaki and joined her for a little road trip
to Odawara, where we ate lunch at Blossom Cafe, a great little place
with the owner loving to create food customized to the customer.

We described the overview of what we wanted and he chose specifics
based on our preferences.
