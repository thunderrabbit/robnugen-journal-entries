---
title: "Relax through the pandemic wave"
tags: [ "pandemic", "wave", "relax" ]
author: Jordan River
date: 2021-09-06T08:04:04+09:00
draft: false
---

(Thanks to Jordan River of https://spiritmysteries.com)

One of the biggest things on people's minds is the vaccine conversation.

It may be one of the biggest points of contention these days, with so
many people polarizing between one side or another.

I think for so many it comes down to fear, either fear of the virus,
or fear of what the vaccine could do to your body if you get it, or
the medical establishment's influence and control.

And for some, it's not so much fear, but anger, frustration or just
concern for loved ones, family, or friends...

Has this been causing you tension? Anxiety? Frustration?

The polarization can lead a lot of us to take actions sometimes that
we are uncomfortable, or even disconnect from people that we love
because don't want to deal with the energy they're putting off.

It's easy to say "find inner peace with it all", and for some that's
actually fairly simple to do.

"Focus on your own field of consciousness and be here now."

For others, there's so much interconnection (and sometimes
co-dependence) with so many people in your life that it's not so easy.

So... What do we do?

We have to observe that the passing of this pandemic through us is
like a great wave, much like a tidal wave if you were at an ocean
beach.

The solution is actually simple if we think of the pandemic in this way.

When you're facing a tidal wave, the solution is not to fight it,
especially if you're caught in it... but to swim through it.

Let it pass over you.

We can do the same thing with the whole vax and pandemic conversation.

Instead of fighting it, fighting others who disagree with
you... Simply let go, swim through it.

The whole thing will pass but we have to be wise enough to choose our
battles appropriately.

This isn't to say we shouldn't speak our truth, by the way. This isn't
to ask you to remain silent when you see oppression in the world.

Rather, to simply find a place of peace within you by moving THROUGH
the wave yourself in whatever graceful way you feel called to flow.

In this way, I know we'll all get through this and into a better
tomorrow.

I know that this may seem like a rather simple answer to some, maybe
it's "easier said", but when the concept is understood, the easier it
is to "pass-through."
