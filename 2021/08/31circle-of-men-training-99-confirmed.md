---
title: "Circle of Men training 99% confirmed"
tags: [ "mkp", "training", "japan" ]
author: Rob Nugen
date: 2021-08-31T08:01:55+09:00
draft: false
---

Message from Torres:

    Rob and Dan,
    
    Submit the Special CoM and I think I can get it built by Friday.
    
    1. This is a one time deal. MKP USA operations does not have the
    infrastructure to support trainings for unofficial regions.
    2. When Japan does the work to become an official developing region we
    can look at this again.

So it's confirmed!  We'll have the first MKP USA supported training in
Japan which will offer tools and skills to create, build, and sustain
a men’s group over the long haul.

The last 1% is all the pieces settling into place for a great
training.


<!--
{{% img-responsive
"https://b.robnugen.com/mkpjapan/trainings/2021/circle_of_men_info/what_it_is.png"
"what it is" %}}
{{% img-responsive
"https://b.robnugen.com/mkpjapan/trainings/2021/circle_of_men_info/you_will_learn.png"
"you will learn" %}}
{{% img-responsive
"https://b.robnugen.com/mkpjapan/trainings/2021/circle_of_men_info/building_a_better_world.png"
"building a better world" %}}
{{% img-responsive
"https://b.robnugen.com/mkpjapan/trainings/2021/circle_of_men_info/dan_baldwin_bio.png"
"dan baldwin bio" %}}
-->
