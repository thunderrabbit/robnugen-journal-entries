---
title: "Fear vs Love"
tags: [ "" ]
author: Rob Nugen
date: 2019-03-13T23:15:22+09:00
---

<p class="note">(written 15 February 2022) I'm not sure the exact
date, but had this experience during my trip to Seattle and Vancouver.</p>

I had a visceral experience of fear-vs-love after having eaten seven
cookies with marijuana.  (Apparently one or two would have been better
for a first timer!)

I believed my host was Satan and I had been trapped: the entire
universe had been created so that I would have this moment of
experiencing time going forward and backward simultaneously.

I was paralyzed with fear.  This went on for a while (20 minutes of
infinite lifetimes) until the family dog came up and licked my hands.
I had enough awareness to realize "the dog is not in on it," so I
trusted the dog enough that I was willing to move toward him and get
some dog kisses.

It was in that moment motion toward love, I realized the compounding
effect of our attractive universe. When I focused on love, it
magnified infinitely into love.  When I focused on fear, it magnified
infinitely into fear.

I can best describe it as the experience standing between two flat
mirrors.  The tiniest alignment off center is exponentially magnified.

I thanked all that is good for the experience in which I felt the
dramatic impact of the smallest adjustment of my focus toward either
love or fear.
