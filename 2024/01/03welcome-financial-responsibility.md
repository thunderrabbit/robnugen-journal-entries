---
title: "Welcome financial responsibility"
tags: [ "money", "2024", "business" ]
author: Rob Nugen
date: 2024-01-03T09:14:48+09:00
draft: false
---

(written 09:14 Wednesday 03 January 2024 JST)

In 2024 I'm being guided to take more responsibility for my income.

My December salary is now (at best) 5 days late.

According to patio11 of Bits About Money, this is bad.

https://www.bitsaboutmoney.com/archive/payroll-providers-power-respect/

[![bits about money about missing payroll](//b.robnugen.com/journal/2024/thumbs/bits_about_money_about_missing_payroll.png)](//b.robnugen.com/journal/2024/bits_about_money_about_missing_payroll.png)

I ass-u-me-d I would be paid at end of month as it has been since I
started working at Company.

But when I looked today, I see I was wrong.  So I wrote to Mr CEO

[![salary consciousness](//b.robnugen.com/journal/2024/thumbs/salary_consciousness.png)](//b.robnugen.com/journal/2024/salary_consciousness.png)


