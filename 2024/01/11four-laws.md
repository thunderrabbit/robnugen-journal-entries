---
title: "Four laws"
tags: [ "earth", "laws" ]
author: Rob Nugen
date: 2024-01-11T19:56:47+09:00
draft: false
---

(written 19:56 Thursday 11 January 2024 JST)

https://www.youtube.com/watch?v=ZVF2AYJ3zic 

Real laws cannot be broken.

1. You exist.
2. The one is the all.  The all are the one.
3. What you put out is what you get back.
4. Change is the only constant and everything changes except the first
three laws.