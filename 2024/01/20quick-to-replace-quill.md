---
title: "quick to replace quill"
tags: [ "quick", "quill" ]
author: Rob Nugen
date: 2024-01-20T05:09:02+09:00
draft: false
---

(written 05:09 Saturday 20 January 2024 JST)

I've been working on Quick, which I plan to replace Quill because
Quill apparently doesn't work with PHP 8.

Currently, https://quill.plasticaddy.com/ looks like this:

<br>Not logged in!
<br>Next steps:
<br>O done: Create database by grabbing my users record from DB and creating a new database with same structure
<br># Add URL to password manager
<br># Create template to log in
<br># Simulate logged in value
<br># Create template after logged in
<br># Create database tables
<br># Create a new user
<br># Unsimulate logged in value
<br># Log in
<br># See logged in template