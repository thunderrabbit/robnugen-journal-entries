---
title: "Arrived in Bali"
tags: [ "bali", "arrival", "wayan" ]
author: Rob Nugen
date: 2024-01-20T09:00:42+09:00
draft: false
---

(written 09:00 Sunday 21 January 2024 WITA)

After our flight landed; we waited for the aisle to clear out.  Aju
said she felt ドキドキ which I assured her was normal to feel (before
such an adventure). I mentioned that I sometimes specifically go
*toward* that nervous feeling (as it often guides me well!)

While leaving the plane, both Aju and one of the flight attendants
were surprised I was barefoot.

On the jetway, I was elated.  "oh yes so warm!"  Even the tile in the
airport was warm as I guess it had just been sun-heated most of the
day.

Aju and I parted ways with a handshake as I headed to immigration and she
headed to her next flight.

I paid 250,000 IDR for a taxi to my hotel about a mile from the
airport.  My driver said I was his only passenger that day, whereas
pre pandemic he would have 5 per day or up to 10 on weekends.

Dropped off my stuff in the hotel room and headed up to chill by the
pool.

<img
src=
https://b.robnugen.com/journal/2024/2024_jan_20_bali_is_warm.jpg"
alt="2024 jan 20 bali is warm"
class="title" />
