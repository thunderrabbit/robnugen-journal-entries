---
title: "How to think about photos for website"
tags: [ "jess", "gratitude", "photos" ]
author: Rob Nugen
date: 2024-01-12T08:45:30+09:00
draft: false
---

(written 08:45 金曜日 12 1月 2024 JST)

Great gratitude to Jess for helping me understand how to
select/create/direct photos for my website.  What things do I offer my
clients?  Grounding, Joy, etc.  So my photos need to show I've got
these things already.