---
title: "ultimate and acro"
tags: [ "mike", "linda", "chloe", "ultimate", "acro" ]
author: Rob Nugen
date: 2024-01-14T08:56:20+09:00
draft: false
---

(written 08:56 Sunday 21 January 2024 WITA)

Last week weather was chilly and still good turnout for SOGO Ultimate.
My co-worker Zach joined us.  He's good!  Great catches and awareness
of how to play.  He must have been on a competitive team before.

Mike's sister Linda and her friend Chloe also joined us so we had a
pretty good turnout overall.

We played until tree shadows covered our field.  Acro yoga peeps were
still going, so we headed over there and did a few poses.  Good times!


[![2024 jan 14 acro yoga](//b.robnugen.com/journal/2024/thumbs/2024_jan_14_acro_yoga.jpg)](//b.robnugen.com/journal/2024/2024_jan_14_acro_yoga.jpg)
