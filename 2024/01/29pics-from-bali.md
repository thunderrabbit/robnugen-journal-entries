---
title: "Pics from Bali"
tags: [ "bali", "pics" ]
author: Rob Nugen
date: 2024-01-29T00:30:24+09:00
draft: false
---

[![2024 01 21 massage table](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_massage_table.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_massage_table.jpg)
[![2024 01 21 front garden pool](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_front_garden_pool.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_front_garden_pool.jpg)
[![2024 01 21 back garden](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_back_garden.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_back_garden.jpg)
[![2024 01 21 private pool](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_private_pool.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_private_pool.jpg)
[![2024 01 21 yin yang](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_yin_yang.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_yin_yang.jpg)
[![2024 01 21 rob and flower](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_rob_and_flower.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_rob_and_flower.jpg)
[![2024 01 21 massage chairs](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_massage_chairs.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_massage_chairs.jpg)
[![2024 01 21 chilling area](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_chilling_area.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_chilling_area.jpg)
[![2024 01 21 rob standing](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_21_rob_standing.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_21_rob_standing.jpg)
[![2024 01 23 bed with francois on couch](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_23_bed_with_francois_on_couch.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_23_bed_with_francois_on_couch.jpg)

[![2024 01 29 pool day](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_pool_day.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_pool_day.jpg)
[![2024 01 29 two pigeons](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_two_pigeons.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_two_pigeons.jpg)
[![2024 01 29 wet feet](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_wet_feet.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_wet_feet.jpg)
[![2024 01 29 sandy feet](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_sandy_feet.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_sandy_feet.jpg)
[![2024 01 29 bare foot hoof prints](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_bare_foot_hoof_prints.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_bare_foot_hoof_prints.jpg)
[![2024 01 29 pool dusk](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_pool_dusk.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_pool_dusk.jpg)
[![2024 01 29 kit kat](//b.robnugen.com/journal/2024/2024_jan_bali/thumbs/2024_01_29_kit_kat.jpg)](//b.robnugen.com/journal/2024/2024_jan_bali/2024_01_29_kit_kat.jpg)
