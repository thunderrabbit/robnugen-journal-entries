---
title: "Go to Bali"
tags: [ "bali", "jess", "flight" ]
author: Rob Nugen
date: 2024-01-20T03:18:46+09:00
draft: false
---

(written 03:18 Saturday 20 January 2024 JST)

I just woke up after one sleep cycle.  I was up late-ish last night
talking to Jess about our upcoming trip to Bali.

[![2024-jan-20-train to
airport](//b.robnugen.com/journal/2024/thumbs/2024-jan-20-train_to_airport.png)](//b.robnugen.com/journal/2024/2024-jan-20-train_to_airport.png)

[![2024-01-20 map to airport](//b.robnugen.com/journal/2024/thumbs/2024-01-20_map_to_airport.png)](//b.robnugen.com/journal/2024/2024-01-20_map_to_airport.png)

[![2024-01-20 hotel near denpasar](//b.robnugen.com/journal/2024/thumbs/2024-01-20_hotel_near_denpasar.png)](//b.robnugen.com/journal/2024/2024-01-20_hotel_near_denpasar.png)

https://photos.app.goo.gl/jGG4HFmvdVdpcC6qL

There was a group called L7 I heard about via KTRU I think!