---
title: "First talk with Mindy, the book midwife"
tags: [ "first", "mindy", "bookmidwife" ]
author: Rob Nugen
date: 2024-01-08T18:44:48+09:00
draft: false
---

I'm just about to have a call with [Mindy the book
midwife](https://bookmidwife.com/) about the possibility of getting my
book done.

##### 19:38 Monday 08 January 2024 JST

To: Mindy
<br>Subject: Thank you!

    HI Mindy!
    
    Thank you for your time creating your course, holding space for me,
    and gently asking questions on our call today.
    
    I've put a reminder in my calendar to visit your site after my next
    payday (end of January 2024).
    
    I'll 97% surely buy your 62 lesson course and go through it at some
    pace.
    
    2 hours twice a week sounds like a good plan; we shall see how well I
    can implement that.
    
    Congratulations on clarifying your offering (by selling(?) your
    publishing company) so that you can more effectively focus on helping
    people write.  Thank you for clarifying today was not a publishing
    conversation.
    
    May you also be blessed with abundance in 2024 and beyond.
    
       blessings
        - Rob!

##### 19:44 Monday 08 January 2024 JST

From: Mindy
<br>Subject: Amazing

    Hi Rob,
    
    Thanks for your lovely message and your amazing insight about why I
    sold the publishing company.  I love your amazing intuition and hope
    you will continue to follow it and receive even more magic over the
    coming year.
    
    Please stay in touch and let me know how you get on!
    
    Kind regards,
    
    Mindy