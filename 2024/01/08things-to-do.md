---
title: "Things to do"
tags: [ "todo", "things", "list" ]
author: Rob Nugen
date: 2024-01-08T18:24:48+09:00
draft: false
---

My Lemur backlight has stopped lighting. At home (Pallette One), I've
been using an external monitor so it's okay.

Since I plan to go to Bali for a week (without taking my external
monitor), I'll need a laptop with a backlight.

Here are a few things to do before then

✓ clone ABB and ABF
✓ set up `abb` and `abf`
[] Copy ssh public key to ab server
[] Set up `ssh ab` so I can ssh there
[] Ensure I can save locally and have it copied to ab server
[] Fix `C-c j` on this laptop (*)
[] Fix `gitsl` on this laptop (**)

(*) Emacs command to start a new journal entry

(**) CLI command to let me see what word to use in my [backwards story](https://bitbucket.org/thunderrabbit/robnugen-journal-entries/commits/).

