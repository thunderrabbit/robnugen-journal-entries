---
title: "Flight to Bali"
tags: [ "flight", "aju" ]
author: Rob Nugen
date: 2024-01-21T06:22:10+09:00
draft: false
---

(written 06:22 Sunday 21 January 2024 WITA)

Halfway through the flight my rowmate Aju and I began talking.  She
reminds me of me: heading for a year overseas in a new culture.

One funny thing about the flight: the "time until arrival" was pinned
to 6 minutes for the duration of the flight.

[![2024 jan 20 apparently 42 mins to bali](//b.robnugen.com/journal/2024/thumbs/2024_jan_20_apparently_42_mins_to_bali.jpg)](//b.robnugen.com/journal/2024/2024_jan_20_apparently_42_mins_to_bali.jpg)

[![2024 jan 20 still 6 mins to go](//b.robnugen.com/journal/2024/thumbs/2024_jan_20_still_6_mins_to_go.jpg)](//b.robnugen.com/journal/2024/2024_jan_20_still_6_mins_to_go.jpg)

