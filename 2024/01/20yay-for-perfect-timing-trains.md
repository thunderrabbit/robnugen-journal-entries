---
title: "Yay for perfect timing trains"
tags: [ "gratitude", "trains", "tokyo" ]
author: Rob Nugen
date: 2024-01-20T07:13:21+09:00
draft: false
---

(written 07:13 Saturday 20 January 2024 JST)

[![2024 jan 20 to shinjuku](//b.robnugen.com/journal/2024/thumbs/2024_jan_20_to_shinjuku.jpg)](//b.robnugen.com/journal/2024/2024_jan_20_to_shinjuku.jpg)

After enjoying Homeway 90 from 新百合ヶ丘駅 (6:11) to 新宿駅
(nonstop!) and Yamanote Line (6:34) from 新宿駅 to 日暮里駅, I'm
gratefully on Skyliner 9 from 日暮里駅 (7:05) to NRT Terminal 1.

Thank you Tokyo for incredible train system.  Thank you cleaning team
for beautifully clean trains and facilities.  Thank you ground support
for guidance to the right train and train car.  Thank you sign
designers for designing clear signage.  Thank you architects for
designing clear traffic flow for trains, pedestrians, et. al.  Thank
you smooth tile for making barefoot walking a joy to experience.
Thank you train drivers for safe predictable transit.  Thank you
Skyliner for power outlet for my laptop to write this entry.  Thank
you everyone for everything wonderful.
