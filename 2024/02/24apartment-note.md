---
title: "apartment note "
tags: [ "2024", "travel", "guest", "apartment " ]
author: Rob Nugen
date: 2024-02-24T05:46:00+09:00
draft: false
---

I didn't take photos, but

(1) the sheets are hanging inside the apartment to finish drying.

Once the clothes line is clear, unlock it on both ends and it should roll itself up nicely.

(2) I didn't toss out the trash.  The burnable is in a little cardboard box.  It can go out before 8(?) a.m. on any Monday or Thursday.

Plastic is in a plastic bag. I think it can go out before 8a.m. on any Friday.

(3) I cleared and put your name on a few drawers. Two are downstairs just inside the living room door, and the other is in the loft.

(4) the desk on the right hand side has an American layout keyboard and trackball mouse you can use if your laptop has a rectangular USB port

(5) on the desk I put a blue file folder as your outbox for mail.  Please photograph any that look like bills and send me the photos so I can let you know which ones are the utility bills.

Flyers and news magazines can go into burnable trash.

Thank you so much, brother!