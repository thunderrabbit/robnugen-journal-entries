---
title: Creating posts with Quick
tags: [ "2024", "life", "journal", "fun" ]
author: Rob Nugen
date: 2024-02-04T21:22:00+09:00
draft: false
aliases: [
    "/journal/2024/02/04/creating-posts-with-posternator/"
]
---

I'm really glad to have this working. I can now create posts from the web interface.

I just need to create one more class or so to actually save the posts.

Just a simple matter of programming!