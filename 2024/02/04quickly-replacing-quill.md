---
title: Quickly replacing Quill
tags: [ "2024", "yay", "programming", "quick", "php82" ]
author: Rob Nugen
date: 2024-02-04T21:42:00+09:00
draft: false
---

Wow that was pretty fast overall.

With a little help from

* a framework I've used before
* Copilot on VSCode
* PHP 8.2
* Dreamhost shared hosting

I was able to code up a reasonable replacement for Quill, which had tons of 
bells and whistles I never used, and login challenges I didn't need 
(because I don't always have my Github credentials on my phone)

I'm able to post this at https://quick.robnugen.com/poster/ 
and have it relatively quickly show up at https://www.robnugen.com/journal/
