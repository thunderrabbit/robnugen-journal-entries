---
title: "Bondi Beach morning walk"
tags: [ "2024", "beach", "bondi", "jess" ]
author: Rob Nugen
date: 2024-02-26T11:13:00+09:00
draft: false
---

Nice walk this morning on Bondi Beach with my stylin new sun hat and Jess in her swimmers and sunnies.

[![2024 feb 26 rob in sun hat](//b.robnugen.com/journal/2024/thumbs/2024_feb_26_rob_in_sun_hat.jpeg)](//b.robnugen.com/journal/2024/2024_feb_26_rob_in_sun_hat.jpeg)

