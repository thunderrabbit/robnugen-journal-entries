---
title: "Unlocked bicycle and heat in my apartment"
tags: [ "bicycle", "lock", "heat" ]
author: Rob Nugen
date: 2024-02-23T07:26:13+09:00
draft: false
---

(written 07:26 Friday 23 February 2024 JST)

Last night when I arrived at my bicycle, I couldn't unlock its back wheel.
I presumed it might be related to cold air and a night in the warm
apartment might bring the lock back to life.

I brought my bicycle home by essentially walking it with back tire in
that cold air, where I found my heater was not working.  Hmm.  There's
a green light blinking on it which might mean it needs to believe its
filter has been cleaned or something.

This morning, with my bicycle inside, I was able to fiddle with the lock
and get it unlocked.  Now I think it was actually not fully closed, so
the lock stem was binding the locking mechanism.

I no longer think warm air did the trick, but I did find my space
heater which is now doing its trick of warming the air, so I don't
mind not having the bigger heater.