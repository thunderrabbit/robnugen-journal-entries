---
title: brain break
tags: [ "2024", "brain", "va", "break" ]
author: Rob Nugen
date: 2024-02-05T12:45:00+09:00
draft: false
---

My brain almost broke so it's time for a brain break.

Putting brakes on my brain will keep it from breaking.

The interns said it's raining outside instead of snowing;
weather app predicts snow will start at 15:00.
