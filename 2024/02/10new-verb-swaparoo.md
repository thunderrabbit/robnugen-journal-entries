---
title: "new verb swaparoo "
tags: [ "2024", "swaparoo", "verb", "English " ]
author: Rob Nugen
date: 2024-02-10T09:24:00+09:00
draft: false
---

すわぱる is my new verb meaning "use an express vehicle to shorten time between two distant local stops."

[![2024 feb 10 new verb swaparoo](//b.robnugen.com/journal/2024/thumbs/2024_feb_10_new_verb_swaparoo.jpeg)](//b.robnugen.com/journal/2024/2024_feb_10_new_verb_swaparoo.jpeg)