---
title: A few minutes late for work 
tags: [ "2024", "work", "late", "cold", "va" ]
author: Rob Nugen
date: 2024-02-05T09:25:00+09:00
draft: false
---

It's cold today and I couldn't really get out of bed due to headache.
I even debated wearing shoes.

Bare feet prevailed but I have boots in my backpack just in case I need them when it snows today 

Thanks to an idea by Teal Swan, I have been asking myself what I would do if I loved myself.
 Mainly: take more time for myself. 