---
title: "It's snowing outside"
tags: [ "2024", "snow", "work", "va", "cold", "barefoot", "boots" ]
author: Rob Nugen
date: 2024-02-05T15:23:00+09:00
draft: false
---

Apparently it's snowing outside now.  I'm glad I brought my boots.

I do have limits on my willingness to walk barefoot!

Except that I need to walk outside about 10 minutes each direction, I could probably go without shoes today.

We shall see!  I've not put on my boots yet haha!

##### 22:22 Monday 05 February 2024 JST

It was snowing like crazy for Tokyo, with about 10cm on the ground in
my neighborhood.  I definitely enjoyed wearing my boots tonight!