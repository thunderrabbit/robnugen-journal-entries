---
title: "a bit chilly this morning "
tags: [ "2024", "chilly", "chillin", "Haneda", "departure" ]
author: Rob Nugen
date: 2024-02-24T07:42:00+09:00
draft: false
---

#### 04:30 Good morning!

#### 04:45 Depart apartment

It's 3°C now in Tokyo, which makes for a chilly barefoot walk featuring ~140 stairs up and down on the way to my station.

Despite the chill, I'm barefoot with a thin jacket, prepped for the warmer weather in Sydney!

#### 06:10 arrival in Haneda Terminal 3

Log of today's entry and exits from train stations

[![2024 feb 24 train station list](//b.robnugen.com/journal/2024/thumbs/2024_feb_24_train_station_list.png)](//b.robnugen.com/journal/2024/2024_feb_24_train_station_list.png)

Thank you everyone in the train system for amazing smooth connections!

* ✅ Odakyu Line 
* ✅ JR Nambu Line
* ✅ Keikyu main line
* ✅ Keikyu airport line

#### 07:20

Wow the line to go through x-ray check was 16 minutes, plus a couple minutes to go through the x-ray bit itself.  Despite the security-theater itself, I'm glad laptops don't have to be removed from bags when entering the airport system in Haneda.

I arrived at gate 141 at 07:14, giving me 51 minutes of not too chilly chill time before boarding.
