---
title: "seeded structure with P's help"
tags: [ "2024", "va", "seeder", "work" ]
author: Rob Nugen
date: 2024-02-07T12:45:00+09:00
draft: false
---

Super grateful for P's help today in seeding the structure for a new asset.

We're trying to allow the seeder to handle multiple types of organizations.

The originals were created by different teams with different programming languages and different methodologies so it's quite a fun project to merge them  into one.



