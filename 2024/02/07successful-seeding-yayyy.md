---
title: "successful seeding yayyy"
tags: [ "2024", "seed", "va", "work" ]
author: Rob Nugen
date: 2024-02-07T18:31:00+09:00
draft: false
---

Yay very happy I was able to finish the seeding of historical data for a couple of assets.

The second two of three went relatively easily compared to the first, for which
I had to ask P for help.

I've heard Mr A might not be working here much longer.  🤔
