---
title: "Busy train this morning"
tags: [ "2024", "busy", "train" ]
author: Rob Nugen
date: 2024-02-08T09:31:00+09:00
draft: false
---

Trains in Tokyo have largely been the basis of why I've lived here so long, going on 21 years.

From my experience, here are levels of crowdedness on Tokyo trains:

Level 0: can get a seat of your choice (next to a door)

Level 1: can get easily get a seat, with an empty seat on each side

Level 2: you and your friend can sit together

Level 3: you can sit, but your friend would have to stand, (so you both decide to stand)

Level 4: You cannot sit down, but you can stand by the door

Level 5: You have plenty of hanging straps to choose from

Level 6: You have your own hanging strap

Level 7: No hanging straps available, but you can see the floor of the train

Level 8: You can't see the floor, but you can use your phone

Level 9: You cannot use your phone comfortably, but you can still try

Level 10: You cannot use your phone, but you can still breathe (depends on height)

Level 11: You can breathe, but have to contort your body to stay intact.

Level 12: You cannot breathe easily because your chest is compressed

Level 13: You cannot breathe much, but you can lift your legs and be supported by the crowd (depends on weight)

Based on my scale above, this morning around 8:15am inbound on Odakyu Line was crowded at Level 10.