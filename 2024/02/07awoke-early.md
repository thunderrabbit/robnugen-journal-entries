---
title: "awoke early"
tags: [ "2024", "morning", "good", "early " ]
author: Rob Nugen
date: 2024-02-07T06:53:00+09:00
draft: false
---

After falling asleep last night while happily listening to Ixi happily gush after just discovering the truth about Tori,
I woke up early, around 05:35.

I clipped 1.5mm from my nails and listened to some podcast about narcissism.
The psychologist was styling empathy is the number one skill she hoped to nurture in her children.

Bicycling to the station was chilly but basically safe because the roads were cleared yesterday.
Thank you, neighbors!
