---
title: "Prepping for trip to OZ"
tags: [ "2024", "prep", "oz", "2024", "February " ]
author: Rob Nugen
date: 2024-02-14T20:13:00+09:00
draft: false
---

Thank you Deneys for the reminder that I need to apply for tourist visa before I head to OZ in a couple of weeks!

Great to hear about poetry coming together and great job on encouraging students to poetically write as well.

I look forward to hearing your poetry on YouTube.

Today I went to the post office to see if they can temporarily deliver my mail to Lin's house, but that's not an option unless I'm moving.

Strictly speaking, this journal entry is being posted via Quick, which I set up in preparation for posting from mobile.  Yay for travel!