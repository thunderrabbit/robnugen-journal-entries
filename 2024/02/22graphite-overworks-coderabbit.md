---
title: "Graphite overworks coderabbit"
tags: [ "2024", "work", "graphite" ]
author: Rob Nugen
date: 2024-02-22T09:55:00+09:00
draft: false
---

I'm apparently too fast for the rabbit because my Graphite workflow requires a bunch of rebases.

If I understand correctly, there is an option to use Graphite and not rebase things if we are on a the 'Team' plan.

[![2024 feb 22 too fast for rabbit](//b.robnugen.com/journal/2024/thumbs/2024_feb_22_too_fast_for_rabbit.png)](//b.robnugen.com/journal/2024/2024_feb_22_too_fast_for_rabbit.png)

Anyway, I thought it was funny that I just have to wait 38 seconds.