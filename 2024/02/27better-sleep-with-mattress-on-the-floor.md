---
title: "Better sleep with mattress on the floor"
tags: [ "2024", "quiet", "sleep" ]
author: Rob Nugen
date: 2024-02-27T08:45:00+09:00
draft: false
---

The couch bed features a relatively thin light mattress which sags as the bed frame creaks and bed springs squeak.  

Moving the mattress completely off the couch bed frame provides firmer and quieter support allowing continuous deeper sleep.

This arrangement also allows quicker transitions between working mode and sleeping mode:  Instead of having to move the desk chair to open the couch bed I can just keep the couch closed and let the mattress chill against the wall during the day.