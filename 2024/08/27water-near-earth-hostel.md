---
title: "water near Earth Hostel "
tags: [ "2024", "nikko " ]
author: Rob Nugen
date: 2024-08-27T16:16:00+09:00
draft: false
---

Standing in river next to Earth Hostel 

[![2024 aug 30 chilly water 1000](//b.robnugen.com/journal/2024/thumbs/2024_aug_30_chilly_water.jpeg)](//b.robnugen.com/journal/2024/2024_aug_30_chilly_water_1000.jpeg)

I got this stuff out of a whirlpool next to Earth Hostel 

[![2024 aug 30 cleaned from whirlpool 1000](//b.robnugen.com/journal/2024/thumbs/2024_aug_30_cleaned_from_whirlpool.jpeg)](//b.robnugen.com/journal/2024/2024_aug_30_cleaned_from_whirlpool_1000.jpeg)