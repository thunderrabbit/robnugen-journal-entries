---
title: "Book cover finished for IM FINE"
tags: [ "yay", "book" ]
author: Rob Nugen
date: 2024-08-16T20:38:52+09:00
draft: false
---

(written 20:38 Friday 16 August 2024 JST)

First draft

[![first from inam](//b.robnugen.com/blog/2024/thumbs/first_from_inam.png)](//b.robnugen.com/blog/2024/first_from_inam.png)

Second draft

[![im fine red centered](//b.robnugen.com/blog/2024/thumbs/im_fine_red_centered.png)](//b.robnugen.com/blog/2024/im_fine_red_centered.png)

Final draft yay!

<img src="https://b.robnugen.com/blog/2024/red_center_center.png"
alt="red center center"
class="title" />
