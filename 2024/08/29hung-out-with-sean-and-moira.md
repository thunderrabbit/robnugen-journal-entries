---
title: "hung out with Sean and Moira "
tags: [ "2024", "late" ]
author: Rob Nugen
date: 2024-08-29T22:22:00+09:00
draft: false
---

Moira, Seàn, and Rob after someone forgot an umbrella 

[![2024 aug 30 moira sean rob 1000](//b.robnugen.com/journal/2024/thumbs/2024_aug_30_moira_sean_rob.jpeg)](//b.robnugen.com/journal/2024/2024_aug_30_moira_sean_rob_1000.jpeg)

This is not a sleeping car, but it works in a pinch. 

[![2024 aug 30 sleeping car on train 1000](//b.robnugen.com/journal/2024/thumbs/2024_aug_30_sleeping_car_on_train.jpeg)](//b.robnugen.com/journal/2024/2024_aug_30_sleeping_car_on_train_1000.jpeg)