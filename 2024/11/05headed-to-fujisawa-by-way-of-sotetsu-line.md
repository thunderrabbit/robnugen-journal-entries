---
title: "headed to Fujisawa by way of Sotetsu Line"
tags: [ "2024", "oops", "train " ]
author: Rob Nugen
date: 2024-11-05T12:15:00+09:00
draft: false
---

Jess and I are headed to Fujisawa for lunch, beach, dinner, meeting, play, meeting, play, etc.

We were a couple minutes late leaving my station so we missed the recommended train.

I was focused on Jess focused on her internet issue and neglected to notice the next train was headed to Odawara until we departed on the wrong side of Sagami-Ono station.  Oh no, indeed.

Thanks to Dr Google Maps who knew about the Sotetsu Line even as Jess was grumpy with Google Authenticator for its annoying way of keeping things secure.