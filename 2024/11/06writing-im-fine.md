---
title: "Writing I'm Fine"
tags: [ "2024", "book", "structure " ]
author: Rob Nugen
date: 2024-11-06T10:15:00+09:00
draft: false
---

[![2024 im fine 2d representation 1000](//b.robnugen.com/blog/2024/thumbs/2024_im_fine2d_representation.jpeg)](//b.robnugen.com/blog/2024/2024_im_fine2d_representation_1000.jpeg)

Thanks to Karen Langston for the idea of structural edits using two dimensions!