---
title: "not quite working"
tags: [ "git", "journal" ]
author: Rob Nugen
date: 2024-12-27T18:40:43+09:00
draft: false
---

(written 18:40 Friday 27 December 2024 JST)

Though I imagine I can get the concept to work, it's not working yet.

Quick can save files to the remote directory and create a new
temporary branch, but when the master branch is updated, I still need
to switch to master and pull it, then create a new temp branch.

Maybe I can work on it over the holidays, but hopefully I'll be too
busy!

### 2024-12-27T19:16:00+09:00

Yay it works after deleting branch on server

Now I just need to automate that.  Hmmm 🤔
