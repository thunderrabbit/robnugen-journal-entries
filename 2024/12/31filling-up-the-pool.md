---
title: "Filling up the pool"
tags: [ "2024", "pool" ]
author: Rob Nugen
date: 2024-12-31T17:58:00+09:00
draft: false
---

It takes a long time to fill a pool with a typical water hose flowing at about a gallon per minute or so.

The home owner was like "hey, from the camera I can see the pool is low; can you fill it with water to the top and add two scoops of chlorine?"

I immediately went to fill it up, knowing it will take approximately forever, but before starting, Jess wanted to get precise instructions from the home owner, who, from past history *obviously* isn't too worried about these kinda things.

We waited until after Jess got the reply: "10 cm from the top" and then went out to turn on the water.

"Oh, you're right, it's going to take a while."

そうだね。

Also, wow I'm in Australia with Jess!