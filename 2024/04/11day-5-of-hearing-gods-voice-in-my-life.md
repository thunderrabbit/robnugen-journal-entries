---
title: "Day 5 of Hearing God's Voice in my life"
tags: [ "2024", "Candace", "God", "555" ]
author: Rob Nugen
date: 2024-04-11T05:55:00+09:00
draft: false
---

#### 5:55 Thursday 11 April 2024 (木)

Dear God, thank You for myriad ways of speaking to me. I saw 444 twice yesterday or so and woke up to see 555 on my phone while thinking I was late awaking. But I feel great and realizing Your Path Energizes Me. Let's gooooo 555!

Yesterday I prayed for a new journal and today I found the perfect journal in my room.  BOOK TWENTY-SIX is the volume labeled 

YOU DO THE IMPOSSIBLE WHEN YOU'RE HUNGRY AND DRIVEN!!!

which I started 4 February 1995 during my final years of University + year or so dabbling in Amway.

Thank you God for answering my call of having a brand new book with even more pages remaining than the entire TWENTY-FIVE when it was new.

Thank you Candace Doyle and DavidPaul Doyle for recording [5-Steps to Hearing God's Voice 30-Day Course](https://thevoiceforlove.com/5steps/audio/)
