---
title: "emotionally productive "
tags: [ "2024", "crying " ]
author: Rob Nugen
date: 2024-04-13T18:10:00+09:00
draft: false
---

Today I had a good cry, feeling lonely and isolated
on my computer and in Japanese language world 
instead of hanging out in Australia with Jess.

As part of hearing God's voice, 
I practiced extending love to my sadness as it arose.

Oh, another sad thing that happened was the loss of 
my [Iris Go](https://irisgo.ch) cup which 
I wanted to give to my neighbor 愛ちゃん.

I think what happened is that it got stuck by
being closed continuously for about 2 months while I was gone.
But what really happened when I opened it and saw it wouldn't
unlock and I got frustrated then broke it while trying to fix it non-carefully.