---
title: "baseline barefoot footsteps over the hill"
tags: [ "2024", "photos", "count" ]
author: Rob Nugen
date: 2024-04-29T08:50:00+09:00
draft: false
---


156 barefoot steps going up the apartment-facing staircase including landings.

[![2024 apr 29 staircase-facing apartment](//b.robnugen.com/journal/2024/thumbs/2024_apr_29_staircase_facing_apartment.jpeg)](//b.robnugen.com/journal/2024/2024_apr_29_staircase_facing_apartment_1000.jpeg)


194 barefoot steps going down station-facing staircase, including landings.

[![2024 apr 29 staircase-facing station](//b.robnugen.com/journal/2024/thumbs/2024_apr_29_staircase_facing_station.jpeg)](//b.robnugen.com/journal/2024/2024_apr_29_staircase_facing_station_1000.jpeg)

Though the photos were not both taken just now, the second one was and the counts are from ascending and descending the staircases just now.


### 17:55 29 April 2024

While headed home

* 203 footsteps up station-facing staircase, including landings.

* 156 footsteps down the apartment-facing staircase including landings.