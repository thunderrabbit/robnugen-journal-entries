---
title: "great ultimate day "
tags: [ "2024", "weather", "SOGO", "Bootcamp " ]
author: Rob Nugen
date: 2024-04-14T21:52:00+09:00
draft: false
---

Today was a really great day in terms of weather
with a quite a big community of maybe 80 people
in attendance at SOGO Bootcamp.

We had six new attendees (total, not at the same time)
at ultimate and got a few good games of four on four going,
including subs and for a bit we had five on five.

I was happy to hear when Moizse was so impressed
with our endurance, running back and forth for hours.

I must say I'm in top physical condition that I've been
since high school.  Yay 😁!