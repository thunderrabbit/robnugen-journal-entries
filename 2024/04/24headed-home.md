---
title: "headed home"
tags: [ "2024", "blessings" ]
author: Rob Nugen
date: 2024-04-24T22:32:00+09:00
draft: false
---

[![2024 apr 24 thank you god 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_24_thank_you_god.jpeg)](//b.robnugen.com/journal/2024/2024_apr_24_thank_you_god_1000.jpeg)

Thank you God for the opportunities to serve.