---
title: "beautiful purple flowers "
tags: [ "2024", "flowers" ]
author: Rob Nugen
date: 2024-04-28T05:58:00+09:00
draft: false
---

I saw these flowers on my way to get food.  More buds coming!

[![2024 apr 28 new buds coming 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_28_new_buds_coming.jpeg)](//b.robnugen.com/journal/2024/2024_apr_28_new_buds_coming_1000.jpeg)

[![2024 apr 28 barefoot rob feet and flowers 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_28_barefoot_rob_feet_and_flowers.jpeg)](//b.robnugen.com/journal/2024/2024_apr_28_barefoot_rob_feet_and_flowers_1000.jpeg)

[![2024 apr 28 beautiful purple flowers 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_28_beautiful_purple_flowers.jpeg)](//b.robnugen.com/journal/2024/2024_apr_28_beautiful_purple_flowers_1000.jpeg)
