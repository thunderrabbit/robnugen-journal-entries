---
title: "chilled in Botanical Gardens by Opera House "
tags: [ "2024", "botanical", "Sydney " ]
author: Rob Nugen
date: 2024-04-01T18:10:00+09:00
draft: false
---

Now I'm wishing I'd taken more pictures but we got this one photo in the gardens. There was a neat photo op of people up on the Sydney Harbour bridge and the Opera House itself is always iconic.

It's so funny that after being here just a few times within 5 weeks I'm like "eh I've seen it before," but now that I am hella leaving I'm like "oh damn I should have taken more pictures."

