---
title: "tan Rob and purple flowers "
tags: [ "2024", "tan", "purple", "rob", "flowers" ]
author: Rob Nugen
date: 2024-04-29T18:04:00+09:00
draft: false
---

More purple flowers!

[![2024 apr 29 mini purple flowers 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_29_mini_purple_flowers.jpeg)](//b.robnugen.com/journal/2024/2024_apr_29_mini_purple_flowers_1000.jpeg)

[![2024 apr 29 bare feet below flowers 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_29_bare_feet_below_flowers.jpeg)](//b.robnugen.com/journal/2024/2024_apr_29_bare_feet_below_flowers_1000.jpeg)

[![2024 apr 29 closer purple flowers 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_29_closer_purple_flowers.jpeg)](//b.robnugen.com/journal/2024/2024_apr_29_closer_purple_flowers_1000.jpeg)

I'm tan after ultimate

[![2024 apr 29 rob is tan 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_29_rob_is_tan.jpeg)](//b.robnugen.com/journal/2024/2024_apr_29_rob_is_tan_1000.jpeg)
