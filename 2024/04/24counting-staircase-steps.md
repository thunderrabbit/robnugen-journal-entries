---
title: "counting staircase steps"
tags: [ "2024", "stairs", "steps", "count" ]
author: Rob Nugen
date: 2024-04-24T08:21:00+09:00
draft: false
---

There is a hill between my apartment and my station, 百合ヶ丘駅.

On the side of the hill near my apartment there's a single staircase featuring (within an order of magnitude) 100 steps.

The 100 steps are split into about 10 flights, with short landings in between each flight.

Today I climbed this staircase (including the landings) in 65 steps, a new record for me!

[![2023 aug 23the steps go up at night 1000](//b.robnugen.com/journal/2024/thumbs/2023_aug_23the_steps_go_up_at_night.jpeg)](//b.robnugen.com/journal/2024/2023_aug_23the_steps_go_up_at_night_1000.jpeg)

The side of the hill facing the station, the landings are longer, and some are sloped.

(I have been thinking about different ways to compare the relative heights of the staircases on each side of the hill..)

Anyway, on the way down the station-facing staircase, I counted 203 normal (for me) footsteps to descend all the stairsteps and landings.