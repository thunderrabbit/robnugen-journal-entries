---
title: "plans and bills"
tags: [ "2024", "today" ]
author: Rob Nugen
date: 2024-04-24T07:27:00+09:00
draft: false
---

I'll take both my laptops to work today so that I can switch between
tasks as needed, between my business, AB, and VA.

As of this writing, I have three events up on my website, one for my
Barefoot walking events, one for Jess's webinar, and one for our
conscious connection circle coming up on June 21st.

In other news, as of this writing, I have four bills, each for 48,700
yen which I think are related to social insurance.  I also think they
might be duplicates of each other because I was late in paying them.
I should be able to pay one today and one or all the remaining three
after I get paid at the end of this month.

Also, I finally got past the issue I was having with cookies for AB.
I found a line of code that was removing from the database the active
cookie each time a user changed their password.  At the time I wrote
that line, I thought it was in the name of security, but now I think
changing the password should not log out the currently logged in
session.  Wait; maybe it should??  Hmmm I had a workaround to get fix
that issue but now i've untangled it and can go either way as needed.
Right now it's set that they stay logged in.  Yay!

Next step will be to allow login via magic links.