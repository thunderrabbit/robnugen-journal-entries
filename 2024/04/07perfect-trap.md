---
title: "perfect trap"
tags: [ "2024", "Mike", "funny", "haiku" ]
author: Rob Nugen
date: 2024-04-07T14:23:00+09:00
draft: false
---

While on the train to Raymond and Nessa's place,
Mike suggested we all speak only in 5-7-5 Haiku format.

I informed him Haiku must have a reference to the seasons
(I think it's actually a reference to the natural world)

He tried a few and then was like "bah"

When we got off the train in 自由が丘駅,
Mike started the following exchange with me:

Knock knock!

Who's there?

Hike.

Haiku?

    Rob Unsuspecting.
    Mike, waiting with bated breath 
    Springs the perfect trap 
