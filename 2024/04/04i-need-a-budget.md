---
title: "I need a budget "
tags: [ "2024", "ynab", "first", "joy", "money" ]
author: Rob Nugen
date: 2024-04-04T07:38:00+09:00
draft: false
---

Yesterday I finally dove into YNAB (You Need A Budget) enough that I thiiiinnnk I'll be able to stick with it. I already feel more relaxed and excited about my money even though I'm behind on social security payments and not sure about my income this month.

[![2024 apr 04 i need a budget](//b.robnugen.com/journal/2024/thumbs/2024_apr_04_i_need_a_budget.png)](//b.robnugen.com/journal/2024/2024_apr_04_i_need_a_budget_1000.png)

Because I'm in Japan I have to enter all transactions manually, but one benefit (as mentioned by ____) in a recording includes my having more awareness of my spending by entering everything myself.

My categories are still chaotic and I'm not sure if I'm using the credit card function correctly, but this should settle within a month or two.

I set a daily reminder to update YNAB transactions in my budget.

Let's see how it goes! 🙈👀
