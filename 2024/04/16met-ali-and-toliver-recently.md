---
title: "met Ali and Toliver recently "
tags: [ "2024", "first", "Ali", "Toliver " ]
author: Rob Nugen
date: 2024-04-16T07:52:00+09:00
draft: false
---

On Saturday I met Ali and others at Cinta Jawa in 渋谷.

[![2024 apr 16 rob and ali after cinta jawa 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_16_rob_and_ali_after_cinta_jawa.jpeg)](//b.robnugen.com/journal/2024/2024_apr_16_rob_and_ali_after_cinta_jawa_1000.jpeg)

On Sunday I met Toliver who joined us for disc.  He has an electric scooter 🛴 zoom zoom!

[![2024 apr 16 rob and toliver after disc 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_16_rob_and_toliver_after_disc.jpeg)](//b.robnugen.com/journal/2024/2024_apr_16_rob_and_toliver_after_disc_1000.jpeg)