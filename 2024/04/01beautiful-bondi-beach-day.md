---
title: "Beautiful Bondi Beach day"
tags: [ "2024", "beautiful", "beach", "Bondi " ]
author: Rob Nugen
date: 2024-04-01T17:35:00+09:00
aliases: "/journal/2024/04/01/thank-you-bondi/"
draft: false
---

There was a kid at Bondi Beach who dug a hole in the sand deeper than he was tall.  I didn't take a photo because he's a kid, but he inspired me to dig, so I did.

[![01 nothing but bondi beach 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_01_nothing_but_bondi_beach.jpeg)](//b.robnugen.com/journal/2024/2024_apr_01_nothing_but_bondi_beach_1000.jpeg)

In other news, the waves were again amazing today. For the last few days there's been a quite a deep section right where the waves reach the shore and then shallow for about 20 or 30 meters before it gets deep again.

There were a few terrific waves today which ruffled up a lot of sand in the deeper portion of the water, so that was pretty neat.

[![2024 apr 01 thank you bondi 1000](//b.robnugen.com/journal/2024/thumbs/2024_apr_01_thank_you_bondi.jpeg)](//b.robnugen.com/journal/2024/2024_apr_01_thank_you_bondi_1000.jpeg)