---
title: "going to Nagano"
tags: [ "yay", "travel", "plans" ]
author: Rob Nugen
date: 2024-06-10T09:42:15+09:00
draft: false
---

(written 09:42 Monday 10 June 2024 JST)

[![yokohama-to-ueno](//b.robnugen.com/journal/2024/thumbs/yokohama-to-ueno.png)](//b.robnugen.com/journal/2024/yokohama-to-ueno.png)

[![ueno-to-nagano](//b.robnugen.com/journal/2024/thumbs/ueno-to-nagano.png)](//b.robnugen.com/journal/2024/ueno-to-nagano.png)

[![arrival in kurohime](//b.robnugen.com/journal/2024/thumbs/arrival_in_kurohime.png)](//b.robnugen.com/journal/2024/arrival_in_kurohime.png)
