---
title: "Amazing Indian Palm Leaf Reading like OMG"
tags: [ "indian", "palm", "leaf", "reading", "guruji", "gratitude" ]
author: Rob Nugen
date: 2024-06-11T14:49:11+09:00
draft: false
---

(written 14:49 Tuesday 11 June 2024 JST)

After a few minute late start, the meeting started with just me and
moderator, who explained the process and helped me prepare for how to
answer upcoming questions: Yes, No, or Unsure.

Once we were ready the palm leaf reader and translator (from old Tamil
to English)

What an incredible process of finding my palm leaf.  I just about
burst into tears as he started identifying names, professions, desired
professions, dates, star signs, marriage, divorce, siblings, partner
Jessica and then confirmed "we have found your leaf."

Thank you moderator for beautiful gentle helpful guidance.

Absolutely wild experience, and they haven't even done the reading yet.

We are waiting for "Ten Indian Minutes" until we are all ready.  I
hung my laundry and wrote this entry.  Now about to begin, I presume.
(I'm in waiting room on Zoom)

##### 16:15 Tuesday 11 June 2024 JST

Wowwwwww so beautiful.  The leaf articulated my large energies
upcoming in 2 or 3 year increments.  They said I will continue to face
difficulties starting business until 14 April 2025

I will receive an email in about 2 weeks with forecast, poojahs, and
guidance.



##### 15:17 Wednesday 12 June 2024 JST


   HI Kate
   
   Thank you for checking in.  Yesterday after the reading felt
   wonderful and excited.  I thanked my moderator as best I could.
   Her calm patience helped me feel held and grounded during the
   process.  Afterward, I told my partner about the experience and
   some of my other friends, one of whom has asked for more info.
   
   I stayed up way too late last night watching silly YouTube videos.
   I cannot say it was because of the reading, but it's unusual for me
   to do so.
   
   I'm feeling "off" today; hardly able to work, and waiting eagerly
   for the remedy email to arrive in 2 or 3 weeks.  I watched Dr Q on
   NLS and burst into desperate tears when he talked about the Poojas
   which can help fix my issues.  "Please help me, God," gasping
   through tears.
   
   In other news, i changed my email provider to a slightly less
   expensive option and email seems to be working find as I received
   yours at the new provider.  I've been drinking water and orange
   juice, plus ate healthy breakfast and lunch, .  smelling flowers
   and saying hello to people when possible.
   
   So I think I'm okay, just a bit lethargic and eager to get started
   on the Poojas and such.
   
   Maybe I should go outside and get some sun and breeze.
   
   Namaste
   Rob
