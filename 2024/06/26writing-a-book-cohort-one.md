---
title: "Writing a book cohort one"
tags: [ "one", "book", "first" ]
author: Rob Nugen
date: 2024-06-26T08:06:32+09:00
draft: false
---

(written 08:06 Wednesday 26 June 2024 JST)

Given what Hassan is sharing, I guess I should think about writing
different books for the rest of my life. That will prevent me from
thinking I have to write One Book To Rule Them All.

https://writerontheside.com

    Hey Hassan
    
    Thank you for sharing your wisdom today.  I'll get to work on stuff as best I can.
    
    In other news, I was unceremoniously dropped by my internet connection.
    I just wanted to let you know it wasn't intentional that I left without expressing gratitude!
    
       blessings
        - Rob
