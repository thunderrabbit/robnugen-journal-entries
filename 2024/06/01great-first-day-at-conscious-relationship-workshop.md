---
title: "Great first day at Conscious Relationship workshop"
tags: [ "jess", "workshop", "great", "dolphin" ]
author: Rob Nugen
date: 2024-06-01T08:02:14+09:00
draft: false
---

(written 08:02 Sunday 02 June 2024 JST)

Yesterday I had some great breakthroughs and realizations in Jess X
Goh's Conscious Relationship workshop.

I recognize the different perspectives between myself and the CEO of
the company where I work.  CEO is focused on solving customer
problems, and I'm focused on solving customer problems, but for
different customers.  Mine are old extant customers and his are new
shiny higher paying customers.  Anyway, the process allowed me to see
our differences from a neutral perspective so we can find a good solution.

I was able to disconnect from my energetic Mommy.  I unplugged the
umbilical cord.  I connected with dolphin energy as spirit guides.
That brought lots of happy tears.  Suddenly I understood the
significance of being picked to pet the dolphins in Astroworld when I
attended the show as a kid.

I also got clarity on my dad just doing his best when trying to
convince me to not eat too many sweets.  