---
title: "Banner drafts of Overcoming Difficult Relationship Dynamics"
tags: [ "2024", "banner", "canva" ]
author: Rob Nugen
date: 2024-05-03T16:21:00+09:00
draft: false
---

Jess and I will be running an event at The Pink Cow in mid June!

This is my first draft of the banner

[![2024 may 03 overcoming difficult relationship dynamics 1st draft 1000](//b.robnugen.com/journal/2024/thumbs/2024_may_03_overcoming_difficult_relationship_dynamics_1st_draft.png)](//b.robnugen.com/journal/2024/2024_may_03_overcoming_difficult_relationship_dynamics_1st_draft_1000.png)

I signed up for 30 days of Canva pro (so it can auto-remove background for me)..  Plus it's nice to have the brand items; I think I'll likely keep it.  Just about $100 per year.

Jess quickly gave feedback on the banner:

    Hi Rob,
    
    Nice attempt.
    
    Here are my thoughts in making it better below:
    
    1. Remove countries (not relevant)

    2. The event title should be the same font, are you able to find a
       different font that's similar to the Men's Circle banner you created?
    3. You need to add 
    
    Date: Saturday 15th June 2024 
    Time: 5pm-6:30pm 
    Venue: The Pink Cow, Akasaka
    
    4. Remove faded background lines
    
    5. What does it look like if you make the font white?
    

I made all the changes, except I'll keep the time in Japanese format:
17:00 - 18:30

[![2024 may 03 overcoming difficult relationship dynamics fb event cover 2nd draft black 1000](//b.robnugen.com/journal/2024/thumbs/2024_may_03_overcoming_difficult_relationship_dynamics_fb_event_cover_2nd_draft_black.png)](//b.robnugen.com/journal/2024/2024_may_03_overcoming_difficult_relationship_dynamics_fb_event_cover_2nd_draft_black_1000.png)

I increased the opacity of the background so the white text is visible:

[![2024 may 03 overcoming difficult relationship dynamics fb event cover 2nd draft white 1000](//b.robnugen.com/journal/2024/thumbs/2024_may_03_overcoming_difficult_relationship_dynamics_fb_event_cover_2nd_draft_white.png)](//b.robnugen.com/journal/2024/2024_may_03_overcoming_difficult_relationship_dynamics_fb_event_cover_2nd_draft_white_1000.png)
