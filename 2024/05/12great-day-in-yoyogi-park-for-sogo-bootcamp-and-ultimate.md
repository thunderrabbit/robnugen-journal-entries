---
title: "Great day in Yoyogi Park for SOGO Bootcamp and Ultimate"
tags: [ "sogo", "bootcamp", "ultimate", "great", "yoyogi", "photo" ]
author: Rob Nugen
date: 2024-05-12T10:01:33+09:00
draft: false
---

(written 10:01 Monday 13 May 2024 JST)

[![spooky weather](//b.robnugen.com/journal/2024/thumbs/spooky_weather.jpg)](//b.robnugen.com/journal/2024/spooky_weather.jpg)

Yesterday the weather report was spooky, but we had a great day for
SOGO Bootcamp and Ultimate

Photos by Mimi from
https://www.facebook.com/media/set/?set=oa.454090903658736&type=3


[![rob niall group](//b.robnugen.com/journal/2024/thumbs/rob_niall_group.jpg)](//b.robnugen.com/journal/2024/rob_niall_group.jpg)
[![rob and niall](//b.robnugen.com/journal/2024/thumbs/rob_and_niall.jpg)](//b.robnugen.com/journal/2024/rob_and_niall.jpg)
[![rob and jo](//b.robnugen.com/journal/2024/thumbs/rob_and_jo.jpg)](//b.robnugen.com/journal/2024/rob_and_jo.jpg)
[![rob best jn impression](//b.robnugen.com/journal/2024/thumbs/rob_best_jn_impression.jpg)](//b.robnugen.com/journal/2024/rob_best_jn_impression.jpg)
[![knee scraping relay](//b.robnugen.com/journal/2024/thumbs/knee_scraping_relay.jpg)](//b.robnugen.com/journal/2024/knee_scraping_relay.jpg)

After the relay I took this photo:

[![rob knee oopsy](//b.robnugen.com/journal/2024/thumbs/rob_knee_oopsy.jpg)](//b.robnugen.com/journal/2024/rob_knee_oopsy.jpg)

Ai took this photo of Mike being Mike

[![mike be like](//b.robnugen.com/journal/2024/thumbs/mike_be_like.jpg)](//b.robnugen.com/journal/2024/mike_be_like.jpg)

Oh today is the first day I saw food trucks on the edge of the field.

[![food trucks by field](//b.robnugen.com/journal/2024/thumbs/food_trucks_by_field.jpg)](//b.robnugen.com/journal/2024/food_trucks_by_field.jpg)