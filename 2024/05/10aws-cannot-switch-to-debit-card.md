---
title: "AWS cannot switch to debit card"
tags: [ "2024", "ugh" ]
author: Rob Nugen
date: 2024-05-10T09:48:00+09:00
draft: false
---

As part of cleaning up my personal finance, I've been switching all my
online services to use my debit card instead of credit card.

That has been working fine for Google, Dropbox, etc, but not for AWS.

All is fine up to the fifteenth digit.

![2024 may 10 fifteen digits 1000](//b.robnugen.com/journal/2024/2024_may_10_fifteen_digits.png)

But when I enter the sixteenth digit, some fancy JS disables the 3
digit code field: "No security code needed at this time."  HA!

![2024 may 10 sixteen digits 1000](//b.robnugen.com/journal/2024/2024_may_10_sixteen_digits.png)

I called AWS and sent the above screenshots after the guy asked very politely why I wasn't willing to call my bank about it.

![2024 05 10 aws yuno s](//b.robnugen.com/journal/2024/2024_05_10_aws_yuno_s.png)

Fortunately, good service from Sanjan, despite excess apostrophes.

    Hello there,
    
    Greetings from AWS!
    
    It was great speaking to you over the call today. Thank you for
    your time and patience in discussing this matter with us.
    
    As discussed via our 'Call' conversation, I understand the issue
    you're facing while trying to add a new 'VISA' Debit card on your
    AWS account (as the system isn't allowing you to update 'CVV/CVC'
    to verify the card).
    
    We're extremely sorry for any concerns or confusions this issue
    might have caused at your end.
    
    Please do not worry, I've currently reached out to one of our
    specialized service teams by creating an internal ticket, in order
    to further investigate this issue in detail at their end.
    
    They have the necessary tools and information to investigate this
    in more detail so that we can best assist you.
    
    Rest assured, I will hold on to this support case while they
    investigate, and will update you as soon as they respond to my
    internally raised ticket.
    
    I kindly request your patience while I work with the concerned
    service team to review this issue in detail and get this resolved
    for you as soon as possible.
    
    Your understanding and co-operation in this matter is greatly
    appreciated.
    
    Have a great day ahead!
    
    We value your feedback. Please share your experience by rating
    this and other correspondences in the AWS Support Center. You can
    rate a correspondence by selecting the stars in the top right
    corner of the correspondence.
    
    Best regards,
    Sanjan R.
    Amazon Web Services
