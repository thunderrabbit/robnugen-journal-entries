---
title: "plandex is cool"
tags: [ "2024", "plandex" ]
author: Rob Nugen
date: 2024-10-13T00:17:00+09:00
draft: false
---

It's just too convenient to ignore.

The code ain't perfect, but it's prettier than ugly HTML with loads of CSS tags and such.
