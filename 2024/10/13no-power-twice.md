---
title: "no power, twice"
tags: [ "2024", "bali" ]
author: Rob Nugen
date: 2024-10-13T17:52:00+09:00
draft: false
---

The power cut off at the juice bar so I decided to go for a walk with 5 hours to go before I'm allowed to go into the airport.

[![2024 oct 13 capsule hotel 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_13_capsule_hotel.jpeg)](//b.robnugen.com/journal/2024/2024_oct_13_capsule_hotel_1000.jpeg)

I found a capsule hotel in DPS airport where I can rest.  Yayyyy!

but not plug in my computer!!?! boooo!
