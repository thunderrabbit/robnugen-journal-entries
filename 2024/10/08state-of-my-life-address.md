---
title: "State of My Life Address"
tags: [ "soml", "journal", "2024", "october" ]
author: Rob Nugen
date: 2024-10-08T10:23:06+09:00
draft: false
---

(written 10:23 Tuesday 08 October 2024 WITA)

1. The tantra retreat level 2 in Bali was canceled for reasons, so
1. Jess and i are in Ubud at an AirBnB with a private pool. Basically
heaven on earth for me.
1. Today we're going to go to a free half day tantra event led by
Modestas and Dieke
1. Yesterday we did a half day shopping and I got two nice linen shirts,
tripling my linen shirt supply.
1. I've been happily using my wise.com card for cheap currency
cuonversion and payment without
fees. https://wise.com/invite/dic/robertn209
1. I used my copy of Bullet to upload photos to my site
https://b.robnugen.com/journal/2024/2024_jan_bali/
1. I've been using YNAB to track different currencies.  I love it
sooooo much much much https://ynab.com/referral/?ref=dCM6_SUo6VX3-E8h
1. I just got my YNAB IDR, EUR, and JPY balances up to date
1. We went to The Yoga Barn all day on Monday. It's a lot more than a
barn; also there were an amazing array of classes.
1. I'm ostensibly working on I'M FINE, but I haven't touched it in a
couple of weeks. Hmmm.