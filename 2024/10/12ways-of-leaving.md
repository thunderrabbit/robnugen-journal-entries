---
title: "ways of leaving "
tags: [ "2024", "" ]
author: Rob Nugen
date: 2024-10-12T10:38:00+09:00
draft: false
---

### 10:40 12 Oct 2024

We are ready to leave our Ubud accommodation with its private pool and lovely sun.

Our check out time is 11am

It's 10:40 now.

To facilitate our departure, at 10:35 I recommended to Jess we tell our hosts we're done so they can check the place and help us take our bags to the pick up spot in front of Bintang Supermarket.  Our driver Gusti has always been early.

Jess was like "I don't see what difference it will make as Gusti is always early."

Umm, what?

**Because** he is always early, we can check out now (allowing time for them check the property, hand over keys, and bring luggage to our pick up spot) and leave a bit earlier (which always seems to be Jess preference).

Anyway, I was right about the timing but I just let it go because it's not that important.

