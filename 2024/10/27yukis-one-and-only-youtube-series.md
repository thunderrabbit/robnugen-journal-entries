---
title: "Yuki's One and Only YouTube series"
tags: [ "yuki", "youtube", "rob", "barefoot" ]
author: Rob Nugen
date: 2024-10-27T14:20:59+09:00
draft: false
---

(written 14:20 Tuesday 29 October 2024 JST)

On Sunday Yuki and I climbed a tree (ダメ！) in Yoyogi Park to record a
teaser for a longer video we'll recod on Saturday.

{{< youtube q7X9iFjW6Wo >}}
