---
title: "thank you see.d for Piyo at The Yoga Barn "
tags: [ "2024", "yoga barn", "ubud" ]
author: Rob Nugen
date: 2024-10-11T15:22:00+09:00
draft: false
---

We've been having such a great time at the yoga Barn in Ubud.  Today is our third day at The Yoga Barn during our week here in Ubud.

My favorite class is Piyo with see.d.  I love his gentle, encouraging nature, funny jokes, attention to detail, and great workout to boot.

Before the class started, Jess yawned while he was facing away; he immediately said "those yawns won't last for long."

About 45 minutes into our class he said, "I hope you're sweating by now."

His brother lives near 町田, ten minutes from my station.  Wow!