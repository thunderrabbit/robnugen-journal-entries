---
title: "ring scavenger hunt"
tags: [ "jess", "fun" ]
author: Rob Nugen
date: 2024-10-06T09:19:02+09:00
draft: false
---

(written 09:19 Sunday 06 October 2024 WITA)

Yesterday I put a note in a blender thing so I could put it reliably
in the pool where Jess could find it.

I didn't take photos because it was dark, so any photos in this entry
are re-creations.

* Go for a Camel ride

In January, in the Eco Desa Saya pool, I gave Jess piggy back rides in
the pool.  She likened it to a camel ride because my shoulder blades
were like humps.

We did the camel ride in the pool but she couldn't see the next note
because it was dark.

This morning I updated the note in the blender bell.

* Go for a longer daytime camel ride.

She did, and found the note:

* The glass table has a clue

Under the glass table:

* Let's mix/blend things up

In the blender

* Blue right hand side

Under the right hand side blue coffee cup: the first ring she liked
most two days ago!

Also a note expressing my love, loyalty, and desire for life long
partnership.  And another note saying "Check the fridge for more"

The fridge had a note:

* Above the gate

Above the gate was a note:

* Almost there; check the top right hand drawer

In which she found the second ring she liked yesterday.

And another note with a song, something like

    You are my sunshine
    My only sunshine
    You're in the shower
    As I write this
    I'm devoted to you
    My dear Hunbun
    And committed more
    each time we kiss

She said it was a great birthday gift, and worth the wait.

Yayyyy!!