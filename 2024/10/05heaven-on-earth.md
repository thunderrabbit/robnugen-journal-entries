---
title: "heaven on earth"
tags: [ "jess", "bali", "heaven", "ubud" ]
author: Rob Nugen
date: 2024-10-05T16:11:11+09:00
draft: false
---

(written 16:11 Saturday 05 October 2024 WITA)

Jess and I are in an AirBnB in Ubud.  It has a private pool and nice
white tile floors.  Nearby is Yellow Flower Cafe, where we've eaten
twice in two days.  I also bought her two rings I think she'll like.
