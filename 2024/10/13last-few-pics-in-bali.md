---
title: "last few pics in Bali"
tags: [ "2024", "bali" ]
author: Rob Nugen
date: 2024-10-13T21:47:00+09:00
draft: false
---

[![2024 oct 13 bread in bed by pool 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_13_bread_in_bed_by_pool.jpeg)](//b.robnugen.com/journal/2024/2024_oct_13_bread_in_bed_by_pool_1000.jpeg)
[![2024 oct 13 hammock on roof 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_13_hammock_on_roof.jpeg)](//b.robnugen.com/journal/2024/2024_oct_13_hammock_on_roof_1000.jpeg)
[![2024 oct 13 digital hotel 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_13_digital_hotel.jpeg)](//b.robnugen.com/journal/2024/2024_oct_13_digital_hotel_1000.jpeg)

Did I upload these already?

See, I need a Quick editor!