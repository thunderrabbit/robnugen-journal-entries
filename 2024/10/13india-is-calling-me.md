---
title: "India is calling me"
tags: [ "2024", "wow", "india", "change", "synchronicity" ]
author: Rob Nugen
date: 2024-10-13T14:53:00+09:00
draft: false
---

On September 28th, my mala helped start a conversation with Praveen (Prawin), a man from India who was headed out of the park where we had been visiting spider lilys.  We ended up talking for hours, especially after his friend Alok joined us.

I told them I've always been afraid to go to India because of the outright lies I've gotten.  They agreed those sometimes happen, but greatly encouraged me to go sometime, and just be aware while I'm there.

They encouraged me to visit a couple of different cities, temples, ashrams, gurus.

Neem Karoli Baba

Amritsar Golden Temple

Gudawara Bunkyo-ku

Nishi Kasai Diwali

Dalia Superfood

Iskcon Tokyo

They went to an Indian festival near NHK so I went with them as it's near my favorite Yoyogi Park plus my train line.  Great time trying different food and watching the dancers on stage.

I headed out when my internal alarm sounded and saw Mark In The Park on the stairs so I said HI to him.

Oh, I forgot, so then 6 days later while in Bali with Jess, we met a woman who said she loves India and goes every year for months at a time.  Hmmm!

India is calling me!
