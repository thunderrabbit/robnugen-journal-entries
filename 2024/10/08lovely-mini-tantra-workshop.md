---
title: "Lovely mini-tantra workshop"
tags: [ "workshop", "tantra" ]
author: Rob Nugen
date: 2024-10-08T08:37:42+09:00
draft: false
---

(written 08:37 Wednesday 09 October 2024 WITA)

Yesterday Jess and I went to Modestas and Dieke's place in Ubud where
we met other couples who were planning to attend the Soulmate Level 2
retreat.  We got to embody the opposite gender, thereby increasing my
empathy for women and hers for men.
