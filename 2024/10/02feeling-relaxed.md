---
title: "feeling relaxed"
tags: [ "2024", "bali" ]
author: Rob Nugen
date: 2024-10-02T09:14:00+09:00
draft: false
---

Yesterday I decided to go to a hotel near the airport so I would be less stressed today. I'm proud of myself spending money on a hotel and caring for myself.  Timing worked out wonderfully with preferred trains arriving exactly as I arrived at various platforms.

This morning I chose to not buy the hotel breakfast because it looked boring; I got yummy conveni food instead.