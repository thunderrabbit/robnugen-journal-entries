---
title: "photosphere"
tags: [ "photosphere" ]
author: Rob Nugen
date: 2024-10-05T16:17:26+09:00
draft: false
---

(written 16:17 Saturday 05 October 2024 WITA)

I took a photosphere photo from the pool.  Can you
see it?  https://photos.app.goo.gl/E6UieChj9HPa43Sk7   (Blame Google
if not)  (or blame me for relying on closed-source stuff.)

Oh, I might be able to use this
https://github.com/mistic100/Photo-Sphere-Viewer/releases

