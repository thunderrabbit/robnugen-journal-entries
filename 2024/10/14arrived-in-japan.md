---
title: "arrived in Japan"
tags: [ "2024", "travelin" ]
author: Rob Nugen
date: 2024-10-14T11:36:00+09:00
draft: false
---

After being in Bali yesterday, Jess and I have arrived in Japan via jet ✈️, Tokyo via bus 🚌, and now headed to Nagano via 🚅 bullet train to attend Maxence and Emico's Kazejiva retreat.

