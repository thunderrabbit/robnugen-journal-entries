---
title: "Edit entries with a Quick tweak"
tags: [ "2024", "quick", "edit" ]
author: Rob Nugen
date: 2024-10-14T15:31:00+09:00
draft: false
---

At this point, Quick can only write files, not edit existing files (though it can blindly overwrite existing files).

I think it would be easy enough to write an editor for files:
I just need to write a finder and a parser.

Just a simple matter of programming!
