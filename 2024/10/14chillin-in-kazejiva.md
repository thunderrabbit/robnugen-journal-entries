---
title: "chillin in Kazejiva "
tags: [ "2024", "nagano " ]
author: Rob Nugen
date: 2024-10-14T15:51:00+09:00
draft: false
---

[![2024 oct 14 oranges 1000](//b.robnugen.com/journal/2024/nagano/thumbs/2024_oct_14_oranges.jpeg)](//b.robnugen.com/journal/2024/nagano/2024_oct_14_oranges_1000.jpeg)
[![2024 oct 14 feet by flowers 1000](//b.robnugen.com/journal/2024/nagano/thumbs/2024_oct_14_feet_by_flowers.jpeg)](//b.robnugen.com/journal/2024/nagano/2024_oct_14_feet_by_flowers_1000.jpeg)
[![2024 oct 14 feet in mosquito net hammock 1000](//b.robnugen.com/journal/2024/nagano/thumbs/2024_oct_14_feet_in_mosquito_net_hammock.jpeg)](//b.robnugen.com/journal/2024/nagano/2024_oct_14_feet_in_mosquito_net_hammock_1000.jpeg)

Really happy to be hanging out in good energy with Jess, Maxence, Emico, and Mamiko!