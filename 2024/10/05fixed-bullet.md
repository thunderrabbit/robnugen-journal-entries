---
title: "Fixed Bullet"
tags: [ "bullet" ]
author: Rob Nugen
date: 2024-10-05T16:08:06+09:00
draft: false
---

(written 16:08 Saturday 05 October 2024 WITA)

I tried uploaded images with my badmin but it failed for some reason,
saying Multipart post was missing the closing frame.  I also found the
code had an undefined variable (because I moved some code (with var
$debug_level) from main to a function).  I don't know if that fixed it
or if it was a related to the connection I had been on but the images
are now uploaded.  yay!
