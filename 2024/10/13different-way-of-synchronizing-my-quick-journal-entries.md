---
title: "Different way of synchronizing my quick journal entries"
tags: [ "2024", "" ]
author: Rob Nugen
date: 2024-10-13T00:13:00+09:00
draft: false
---

Instead of trying to use ssh or rsync, I've now got my server using git to save journal entries in branch `tempospoon` which I plan to pull locally, then change as I see fit, then merge into main while keeping up with my backwards story *and* keeping `tempospoon` branch intact.

Still gotta figure out the last bit.

I've got a few test entries on `tempospoon` but this is the only one I want to keep.  If you can see it on my site, it's a good sign future me figured it out.
