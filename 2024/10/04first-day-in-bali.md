---
title: "first day in bali"
tags: [ "2024", "bali" ]
author: Rob Nugen
date: 2024-10-04T14:02:00+09:00
draft: false
---

First place in Bali this time around.

[![2024 oct 04 balidinner at mokshu 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_04_bali_dinner_at_mokshu.jpeg)](//b.robnugen.com/journal/2024/2024_oct_04_bali_dinner_at_mokshu_1000.jpeg)
[![2024 oct 04 balioutdoor stone bathtub 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_04_bali_outdoor_stone_bathtub.jpeg)](//b.robnugen.com/journal/2024/2024_oct_04_bali_outdoor_stone_bathtub_1000.jpeg)
[![2024 oct 04 balipool 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_04_bali_pool.jpeg)](//b.robnugen.com/journal/2024/2024_oct_04_bali_pool_1000.jpeg)
[![2024 oct 04 balikitchen 1000](//b.robnugen.com/journal/2024/thumbs/2024_oct_04_bali_kitchen.jpeg)](//b.robnugen.com/journal/2024/2024_oct_04_bali_kitchen_1000.jpeg)
