---
title: "Before walk to Coogee"
tags: [ "walk", "coogee", "morning" ]
author: Rob Nugen
date: 2024-03-03T09:23:16+09:00
draft: false
---

(written 09:23 Sunday 03 March 2024 AEDT)

After finishing 8/21 love appointments, this time including talking,
we will head to [Coogee
Beach](https://maps.app.goo.gl/bbi8XoKW5HVc79hB8) this morning.  It's
about an hour each way, and we'll stay for a couple of hours.