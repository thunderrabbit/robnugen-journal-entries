---
title: "tooth feeling better with CBD oil"
tags: [ "cbd", "relief", "tooth" ]
author: Rob Nugen
date: 2024-03-08T19:49:28+09:00
draft: false
---

(written 09:49 Saturday 09 March 2024 AEDT)

At the Mind BOdy and Spirit festival in Darlinghurst, a woman
recommended her CBD oils for my tooth, and I must say it feels much
better than before!  Yay and thank you God.  (^^)V