---
title: "Paranoid about bus taps"
tags: [ "2024", "fight", "anger", "jess" ]
author: Rob Nugen
date: 2024-03-16T19:53:00+09:00
draft: false
---

#### 20:36 Saturday 16 March 2024

Opal cards are debit cards which can be used to ride trains and buses
in Sydney.

Tap On / Tap Off

Simple, right?

Yes, but there's a delay from the time of the tap to the time the UI
responds.

Tonight, curious if my tap was saying SUCCESS or if it was still
saying SUCCESS from Jess' tap, I tapped again.

Now I thought the interface said I tapped on.  Oops.  So I went to the
bus driver and asked.  He said I can check, so I did.

ALREADY TAPPED ON

He said he couldn't do anything about it, and he had to get going,
driving the bus and all that.

Jess said, "You're too paranoid; I just saw SUCCESS and didn't worry
about it."

Now, I must admit, being described as paranoid isn't something I like;
in fact it kinda pissed me off. 😡

Interesting.  I felt into my feeling and realized my version of the
story is that I tapped again to do the right thing, which can be
boiled down and traced back to my desire to be seen as a good boy.

I asked Jess, "do you have a recommendation as to how to handle it?"

Instead of answering, she began calling the customer service number on
the back of the card.

"If it's just call them, I can handle it," I assured her, but that
didn't dissuade her from calling them.

She was nice on the phone.  The result of the conversation, in
summary: "call back in 24 hours."

When Jess hung up, I said, "Thank you for checking."

She goes, "it's not checked yet, they said I have to call back in 24
hours."

"Um yes, I got that.  My meaning is thank you for checking the
situation.  Thank you for what you've done already."

Jess put a reminder in her calendar for tomorrow night; I activated a timer for 23:55:00 on my phone.

Silence lingered for a while and then I said, "I'm sorry for being
paranoid; I didn't mean to cause you any more distress."

Jess thanked me for apologizing.  Then she goes, "I just don't want to
take on any more responsibility."

"I'm happy to call them tomorrow if that will help you."

Jess checked her calendar event and let me know the bus stop name:
Campbell Parade.

"Thank you.  I'll call them tomorrow."

"It has to be at 8:30 (pm)"

No shit.  (I didn't say that.) I said as articulate as possible, "I
set an alarm for 23 hours and 55 minutes from now so the time will be
perfect.  I'll need to borrow your phone,..."

"That's fine"

".. but you can remove it from your schedule."

"I did.  Just don't talk to me in that tone of voice."

I said "I won't," as I began to walk away.  I already felt furious so
the best I could do at that moment was walk away for literally a few
seconds to breathe.

When I came back, Jess got up and walked away.

According to my countdown timer, that was about an hour ago.

I walked to the beach and have been standing here while the sand
zamboni goes back and forth clearing the beach of any big debris.

It's too cold to stay out all night but I still don't fucking want to
go back to her apartment.

How amazing I have been planning for a week to buy her flowers tomorrow.

#### 21:55

Sitting in silence inside Bondi Pavilion, I heard someone ask if I was okay. 

I said, "technically yes; I just had a fight with my girlfriend so I'm
just sitting here a bit."

She patted my shoulder, "you got this."

So I headed back to Jess' apartment.

I could hear Jess was listening to a podcast, so therefore presumably
in a relatively good state.  I went inside and gave her a kiss on the
forehead.  "How are you feeling?"

"Much better, thank you."

Jess pretty much immediately apologigized and acknowledged it was
stress from unrelated stuff which had really gotten to her.

I held space for her as she cried out the other stress and we were
thereby able to connect with much more heart and mutual love.