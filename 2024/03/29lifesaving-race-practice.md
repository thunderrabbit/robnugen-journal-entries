---
title: "lifesaving race practice"
tags: [ "lifesaving", "bondi" ]
author: Rob Nugen
date: 2024-03-29T09:28:22+09:00
draft: false
---

(written 09:28 Monday 01 April 2024 AEDT)

A few days ago (Friday 29 March) I went early to Bondi Beach and was
fascinated by the life saving racers.

[![2024 mar 29 lifesaving rabbits](//b.robnugen.com/journal/2024/thumbs/2024_mar_29_lifesaving_rabbits.jpeg)](//b.robnugen.com/journal/2024/2024_mar_29_lifesaving_rabbits_1000.jpeg)

They were preparing for state, national, and world competition.

Basically racing inflatable motor boat out into the surf, pick up a
patient, and bring them back as quickly as possible.  Different boat
drivers / savers / patients each time.
