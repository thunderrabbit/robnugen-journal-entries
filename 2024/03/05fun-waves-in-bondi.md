---
title: "Fun waves in Bondi"
tags: [ "2024", "waves", "bondi", "ohshit" ]
author: Rob Nugen
date: 2024-03-05T17:08:00+09:00
draft: false
---

Today in Bondi Beach I enjoyed large waves with lots of other swimmers, some of whom I contacted (*) due to Mother Nature's raw power and unpredictability.

(*) leg on shoulder, etc

A couple of times I talked briefly with other swimmers.  The funniest of these was:

me: "Woah it's wild today!"
<br>she: "Yeah but it's fun!"
<br>wave: (incoming)
<br>she: "oh shit"

After that wave, we were some meters apart.

[![20240304 rob feet in sand](//b.robnugen.com/journal/2024/2024_bondi/thumbs/20240304_rob_feet_in_sand.jpg)](//b.robnugen.com/journal/2024/2024_bondi/20240304_rob_feet_in_sand.jpg)