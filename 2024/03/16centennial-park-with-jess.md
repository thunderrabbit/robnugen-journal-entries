---
title: "Centennial Park with Jess"
tags: [ "centennial", "park", "jess", "geese" ]
author: Rob Nugen
date: 2024-03-16T09:40:08+09:00
draft: false
---

(written 09:40 Monday 01 April 2024 AEDT)

Jess took me to Centennial Park to see its natural beauty.  It's quite
bigger than Yoyogi Park, featuring lots of geese, bats, and even some
swans.  No crows as in Yoyogi.
