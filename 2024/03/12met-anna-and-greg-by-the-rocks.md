---
title: "Met Anna and Greg by The Rocks"
tags: [ "anna", "greg", "first", "rocks" ]
author: Rob Nugen
date: 2024-03-12T23:09:37+11:00
draft: false
---

(written 23:09 Tuesday 12 March 2024 AEDT)

This evening Jess and I went to Circular Quay to meet Anna and Greg,
half of whom I have known since August 2000 or so.  It was lovely to
meet Anna and Greg.

Somehow Anna and Jess seem excellent at asking
questions.  I chimed in where possible but mostly just enjoyed the
experience.

Funny that Anna asked me what benefits I had noticed from being vegan
and I was like 🤔 and didn't have an answer.  Jess later laughed
saying I had just said there were benefits and then she was trying to
continue the conversation.  I didn't remember I had just said that!

After dinner and conversation, we all went to get ice cream but ended
up going to different places because the vegan options didn't look
good at the first location and Anna and Greg had to head home sooner
than later.
