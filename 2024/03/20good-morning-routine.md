---
title: "Good morning routine"
tags: [ "2024", "jess", "morning", "routine" ]
author: Rob Nugen
date: 2024-03-20T10:27:00+09:00
draft: false
---

This morning Jess and I worked on the images for the new workshop we plan to host here on March 29th.  Then some cuddles and kisses (sans sex so far today) before heading out to get some food for today: coconut milk from Scoop, veggies and such from Harris Farm, then two 2-liter Nudie juice bottles from a different grocer near Harris Farm.

As a challenge, I carried the juice bottles with a single finger each on my right hand: index and pinky fingers.  Index finger was fine, but pinky was a bit tired by the time we got back.

Now with perfect timing it has just started raining; oh now it stopped. nvm.  Oh now it started again yay perfect timing.