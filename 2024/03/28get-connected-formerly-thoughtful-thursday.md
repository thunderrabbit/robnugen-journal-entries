---
title: "Get Connected! (formerly Thoughtful Thursdays)"
tags: [ "thoughtful", "thursday", "connected" ]
author: Rob Nugen
date: 2024-03-28T19:40:44+09:00
draft: false
---

from Matt:

Another beautiful night! Loved hearing about actions speaking louder
than words, and letting the undeniable truths speak for
themselves. Loving the new intention setting exercises. Thanks for
being there, and for those that couldn't make it this week, I hope you
still find time to eat, sit, write, and most of all, Get Connected 🧘‍♂️
♥

<img
src="https://b.robnugen.com/journal/2024/2024_mar_28_thoughtful_thursday.jpeg"
alt="2024 mar 28 thoughtful thursday."
class="title" />
