---
title: "In King's Cross again"
tags: [ "2024", "Marley", "kings", "cross", "Govindas " ]
author: Rob Nugen
date: 2024-03-03T09:27:15+09:00
draft: false
---

#### 19:15 Sunday 3 March 2024

Jess helped me print five of my A5 flyers, then I put one into the outdoor basket of a crystal shop.

We ate at Govindas in King's Cross.

Nice buffet featuring deep fried cauliflower and then ice cream yum!

Next Bob Marley movie from 19:30

#### 21:30

<b>Bob Marley: One Love</b> was a great movie; very inspiring and wow
Bob Marley was only 36 when he died.  Amazing.

Thank you Jess for recommending Govindas!

#### 22:00

Wow Jess helped me find the iconic King's Cross fountain I remember
from my family trip to OZ in 1985!

[![2024 mar 03 back in kings cross](//b.robnugen.com/journal/2024/thumbs/2024_mar_03_back_in_kings_cross_.jpeg)](//b.robnugen.com/journal/2024/2024_mar_03_back_in_kings_cross_.jpeg)
[![2024 mar 03 ive seen this fountain
somewhere.](//b.robnugen.com/journal/2024/thumbs/2024_mar_03_ive_seen_this_fountain_somewhere.jpeg)](//b.robnugen.com/journal/2024/2024_mar_03_ive_seen_this_fountain_somewhere.jpeg)

The Hilton hotel is still here too!

