---
title: "zamboni wiping the slate clean"
tags: [ "2024", "sand", "zamboni", "bondi" ]
author: Rob Nugen
date: 2024-03-16T07:55:00+09:00
draft: false
---

[![2024 mar 16 sand zamboni 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_16sand_zamboni.jpeg)](//b.robnugen.com/journal/2024/2024_mar_16sand_zamboni_1000.jpeg)
[![2024 mar 16 sand zambonied 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_16sand_zambonied.jpeg)](//b.robnugen.com/journal/2024/2024_mar_16sand_zambonied_1000.jpeg)

[![2024 mar 16 looking along new lines 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_16looking_along_new_lines.jpeg)](//b.robnugen.com/journal/2024/2024_mar_16looking_along_new_lines_1000.jpeg)

[![2024 mar 16 new zamboni sand 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_16new_zamboni_sand.jpeg)](//b.robnugen.com/journal/2024/2024_mar_16new_zamboni_sand_1000.jpeg)

[![2024 mar 16 feet in new zamboni sand 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_16feet_in_new_zamboni_sand.jpeg)](//b.robnugen.com/journal/2024/2024_mar_16feet_in_new_zamboni_sand_1000.jpeg)
