---
title: "Renee and Renato breathwork workshop"
tags: [ "breath", "renee", "renato", "first" ]
author: Rob Nugen
date: 2024-03-30T14:55:58+09:00
draft: false
---

(written 14:55 Sunday 31 March 2024 AEDT)

Thank you Renee and Renato for an amazing experience.  I've done
breathwork before, but previously had not had so much experiential
impact from breathwork ever before.  Probably the sheer length of the
session (45 minutes?) of in-in-out breathing.  My hands and feet were
tingling as if I had just arrived back in my body from astral
traveling.

After the session, I felt so alive and so much love for the container
and for Jess having introduced me and attending as well.  So great to
sit with Jess after we helped Renee and Renato pack up their elaborate
setup, including altar, lights, drum set, various instruments, plus
yoga mats, blankets, pillows, and eye masks for each participant.
Such a great event!

Thank You God for I am joyfully receiving heart-aligned abundance,
serving soul-aligned clients.

[![2024 mar 30 group breathwork event 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_30_group_breathwork_event.jpeg)](//b.robnugen.com/journal/2024/2024_mar_30_group_breathwork_event_1000.jpeg)

[![2024 mar 30 renato breathwork art 1000](//b.robnugen.com/journal/2024/thumbs/2024_mar_30_renato_breathwork_art.jpeg)](//b.robnugen.com/journal/2024/2024_mar_30_renato_breathwork_art_1000.jpeg)