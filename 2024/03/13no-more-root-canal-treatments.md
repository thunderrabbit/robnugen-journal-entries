---
title: "No more root canal treatments"
tags: [ "2024", "howard", "first", "holistic", "dds" ]
author: Rob Nugen
date: 2024-03-13T10:28:00+09:00
draft: false
---

#### 09:30am Wednesday 13 March 2024

Holistic dentist Dr Howard suggests removing this tooth with an abcess below it.

<img src="//b.robnugen.com/journal/2024/2024_mar_13_abcess_dead_tooth_xray.jpg">

Hmm very interesting to hear the dangers and ineffectiveness of root
canal treatments.  Removing my molar doesn't sound like what I wanted,
but... true enough, there's an abscess under one of my teeth that got a
root canal many years ago.

<img src="//b.robnugen.com/journal/2024/2024_mar_13_abcess_root_canal_xray.jpg">

Dr Howard's main point seems to be ya cannot possibly clean all the
bacteria from the 6-10 kilometers of micro tubal nerve canals, just 8
bacteria wide.

Sealing that shit inside eventually causes the bacteria to "morph"(?)
into anaerobic bacteria which causes damage etc etc blah blah blah.

🤔 Hmmm


