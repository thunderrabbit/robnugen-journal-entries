---
title: "Wonderful day yay"
tags: [ "yay", "bday", "54" ]
author: Rob Nugen
date: 2024-03-25T23:41:22+11:00
draft: false
---

(written 23:41 Monday 25 March 2024 AEDT)

Right up to the very last drop, today has been a wonderful day with
Jess.

It started with me recording hopefully enough video to make a 5 minute
recording for my website.

Then we headed to a cheap first visit to a Chinese medicine doctor who
did acupuncture on me to hopefully fix my ribs.  It didn't work well
so she thinks my rib might be cracked.

Then lunch of fat noodles and eggplant stew plus some shredded
potatoes in a pita type thing.  Then Mosman and Cobblers Beach, a nude
beach where we swam in our birthday suits and ate carrot cake Jess got
for me.

Then we walked back to Mosman and took bus 100 to Martin Place and
then a train to KX and took pics near the fountain before attending
Lindsey's kundalini activate event.  I felt some energies in my body
each time Lindsey came near me.  Quite interesting!

Then we ate pizza at Dominos because Govindas was closed.  On the way
back Jess posted our event and confirmed the link wasn't working on
mobile so I got it fixed today.

Nice clear skies to see the full moon in the park near her place.

DH is still not allowing me to ssh to my shared host but fortunately I
was able to fix the link, just not post this yet.