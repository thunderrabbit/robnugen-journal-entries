---
title: "tumbling in waves leads to magical connections"
tags: [ "waves", "magic", "jess", "bondi" ]
author: Rob Nugen
date: 2024-03-24T20:57:12+09:00
draft: false
---

(written 20:57 Sunday 24 March 2024 AEDT)

A few days ago I enjoyed being tumbled by the waves in Bondi Beach
swim area.  Harmless fun, right?

For the past 2-3 days I've been unable to inhale deeply without pain.
Jess recommended I see Ken at the Japanese 指圧 shiatsu clinic near
her place.  She bought me a 90 minute session today and I received a
great massage from Ai-chan.  Ai-chan said I had scoliosis!  First time
I've heard that news.  Ai-chan should see Ken-san if possible but he
was busy.

After the treatment, I headed back to Jess' place.  She was heading
out through the back gate just as I walked past it to go to the front
door.  I heard her call my name.

Woah nice timing!  Jess and I went to the festival near the beach and
talked to a woman selling hemp salves, CBD oil, and other stuff.  She
kindly offered her time and said my back has scoliosis.  Hmmm!

On the way back to Jess' place, we bought fruits at Harris Farm for
our fruit fast, starting today.  Just afterward, we saw Ken-san
available in his clinic!  He kindly checked me out and recommended 針
(acupuncture).

At 20:07pm Jess contacted her old acupuncture place via text.. and
they replied back immediately.  She got me an appointment for tomorrow
at 11am!  Magically delicious all around.
