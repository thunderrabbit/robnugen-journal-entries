---
title: "Yay finished flyer for mens circle"
tags: [ "flyer", "canva", "finished" ]
author: Rob Nugen
date: 2024-03-02T20:33:05+09:00
draft: false
---

(written 20:33 Saturday 02 March 2024 AEDT)

{{% img-responsive
"https://b.robnugen.com/blog/2024/2024_21_march_mens_circle_link_stripe.png"
"Rob Nugen in plaid red" %}}

Hooray!  I finished a flyer for my men's circle on 21 March.  Thanks
to Jess for feedback to make it nicer.

Thanks to GIMP for removing the background from the original image!

[![2024 jan 13 workshop mens circle](//b.robnugen.com/blog/2024/thumbs/2024_jan_13_workshop_mens_circle.jpg)](//b.robnugen.com/blog/2024/2024_jan_13_workshop_mens_circle.jpg)

