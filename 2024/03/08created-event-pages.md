---
title: "Created event pages"
tags: [ "event", "pages" ]
author: Rob Nugen
date: 2024-03-08T09:23:09+09:00
draft: false
---

(written 09:23 Friday 08 March 2024 AEDT)

I previously created event pages:

https://www.robnugen.com/en/events/2024/03/21mens-circle-sex-love-and-intimacy/

https://www.facebook.com/events/931265768221130

https://www.lovefestaustralia.com/events-setup/2024/3/21/mens-circle-sex-love-and-intimacy

Jess suggested Humantix and Eventbrite are also good platforms for my
event.

https://events.humanitix.com/men-s-circle-sex-love-and-intimacy

https://www.eventbrite.com/e/mens-circle-sex-love-and-intimacy-tickets-859110371247

Now it's time to go for a dip in Bondi Beach yayyy!