---
title: "filming at Blackwood"
tags: [ "filming", "lights", "blackwood", "neat" ]
author: Rob Nugen
date: 2024-03-13T22:15:24+09:00
draft: false
---

(written 22:15 Thursday 14 March 2024 AEDT)

Last night on our way home, Jess and I saw lights at Blackwood cafe
where they were apparently doing a bit of filming.

<img
src="https://b.robnugen.com/journal/2024/2024_mar_14_filming_at_blackwood.jpeg"
alt="2024 mar 14 filming at blackwood."
class="title" />