---
title: "art.robnugen.com extension on my Mediawiki instance wiki.robnugen.com"
tags: [ "2024", "mediawiki", "php", "extension" ]
author: Rob Nugen
date: 2024-03-20T12:41:00+09:00
draft: false
---

For a while, I have been paying an extra $5 per month to keep one of my servers running PHP 7.4.  In the past few days, I have started poking around my art.robnugen.com extension to get it up to Mediawiki and PHP 8 standards.

Follow along here:

https://wiki.robnugen.com/wiki/Art:Rock_Map_-_December_2014 (currently features a &lt;dump&gt; tag as the basis for updating my &lt;permalink&gt; tag)

https://github.com/thunderrabbit/art.robnugen.permalinks (repo that I'm updating)