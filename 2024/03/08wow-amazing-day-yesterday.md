---
title: "Wow amazing day yesterday"
tags: [ "cbd", "authentic social night", "mind body spirit festival", "pineal", "first", "ybrihim" ]
author: Rob Nugen
date: 2024-03-08T23:55:07+09:00
draft: false
---

(adapted from BOOK TWENTY-FIVE page 112)

#### 00:52AM Saturday 9 March 2024 （土）

Wow so happily exhausted arriving home with Jess after an amazing day
at Mind Body Spirit festival and even more amazing evening at the
Authentic Social Night.

The highlight for me at Mind Body Spirit (MBS) festival was meeting Ybrihim
Camero who teaches the Cyclopea Method of opening the energetic pineal
gland.  I loved hearing his story and feeling good vibes from him.  I
actually signed up for his online class on April 27th and 28th.


Ybrihim was much more welcoming than some of the pushy vibes I felt
from some people in other booths.

Other booths included some Japanese women in Happy Science, which is
apparently a cult.  From their literature:

    To save the world from the crisis it is in, the Creator of
    humanity has descended to earth as Master Ryuho Okawa and
    revealed the name of the Creator, El Cantare for the first
    time in human history.

I see I'm not the only one who thinks it's a cult
https://en.wikipedia.org/wiki/Happy_Science#Controversy

MBS featured plenty of psychics and crystal shops and a Silent Disco!

Jess and I enjoyed the vegan crepes, although I had to eat slowly due
to pain in my tooth.  She subsequently got some CBD oil from a shop;
it really seems to help with the pain, although it also seems to leave
a subtly sticky residue on my tooth.

- - -

Authentic Social Night is a monthly (?) event organized by Simon and
Laura.  Great to meet lots of great people last night, including Gavin, Mauricio, 
Simon, Laura, Lindsey, Antwon, Bridget, Katharine (sp), Maria.

At the entrance a table offered delicious vegan food, including
fruits, chocolate, and Maria's bliss balls.

Funny moments included when Laura wanted to make sure I got to meet
Jess because she goes to Japan sometimes.
