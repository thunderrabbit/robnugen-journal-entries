---
title: "Images for our first co-hosted workshop"
tags: [ "2024", "yay", "first", "jess", "rob", "host", "images" ]
author: Rob Nugen
date: 2024-03-20T08:11:00+09:00
draft: false
---

My first draft:

[![2024 mar 29 connection circle for men and women first draft](//b.robnugen.com/blog/2024/thumbs/2024_mar_29_connection_circle_for_men_and_women_first_draft.png)](//b.robnugen.com/blog/2024/2024_mar_29_connection_circle_for_men_and_women_first_draft.png)

With Jess' ideas:

[![2024 mar 29 connection circle for men and women](//b.robnugen.com/blog/2024/thumbs/2024_mar_29_connection_circle_for_men_and_women.png)](//b.robnugen.com/blog/2024/2024_mar_29_connection_circle_for_men_and_women.png)

Meetup size

[![2024 mar 29 meetup connection circle for men and women](//b.robnugen.com/blog/2024/thumbs/2024_mar_29_meetup_connection_circle_for_men_and_women.png)](//b.robnugen.com/blog/2024/2024_mar_29_meetup_connection_circle_for_men_and_women.png)