---
title: "Lovely Lush Valley "
tags: [ "2024", "park", "meditation " ]
author: Rob Nugen
date: 2024-03-19T09:43:00+09:00
draft: false
---

For some reason, Lovely Lush Valley is called Cooper Park in this
reality.

Matt and Amanda were there as I arrived at 7:02am for a meditation.

The earth was still soaked and trees were still drippy from rain so
instead of a sitting meditation, Matt suggested a walking meditation
event.

An amazing fig tree with tree-sized roots criss crossing connections
down a small cliff inspired us to stop and gaze at its splendor.  I
wondered about its history: how do roots reconnect further down the
line?