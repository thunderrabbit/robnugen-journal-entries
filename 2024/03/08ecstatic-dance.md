---
title: "Ecstatic Dance "
tags: [ "2024", "ecstatic", "dance" ]
author: Rob Nugen
date: 2024-03-08T06:28:00+09:00
draft: false
---

Last night ecstatic dance featured *no talking* so we could just vibe on the music and joy of eye contact.

A relatively quick experience, I stayed active basically the whole time, though by the end of the 90 minutes I was definitely slower than at the beginning.

Newish for me dance moves included

* "WHOO" which we learned at the tantra retreat in Bali: arms up in the air and jumping with heels thumping on floor
* one foot dance: trying to balance as long as possible on one foot while all the rest of me is dancing wildly
* eyes closed dance: feet stably planted and eyes closed with legs and arms moving

Afterwards we chatted a bit with organizer Kat Cowling before heading out.  

I was hungry! We got falafel wrap to eat on the beach while watching the zamboni clean up the sand.  Thank you team Bondi for a beautiful beach experience!
