---
title: "Thoughtful Thursdays"
tags: [ "social", "thoughtful", "thursdays" ]
author: Rob Nugen
date: 2024-03-14T22:04:53+11:00
draft: false
---

(written 22:04 Thursday 14 March 2024 AEDT)

Tonight Jess and I greatly enjoyed Matt's Thoughtful Thursday event at
his apartment, just 10 minutes walk from Jess' place.  Great food;
great connection; great format:

While we ate his vegan gluten free cooking, Matt guided us in a check
in about something we're proud of for the past week.  Then Matt spoke
about "intention" including its etymology which includes the meanings
of "stretch" and "lean toward".  Then we had a meditation while Matt
played hangdrum.  Then conversations in pairs for five minutes per
person, basically reciprocal listening style.  Finally we finished
with closing intention for the upcoming week.

<img
src="https://b.robnugen.com/journal/2024/2024_mar_14_thoughtful_thursday.jpeg"
alt="2024 mar 14 thoughtful thursday."
class="title" />