---
title: "Thoughtful Thursday"
tags: [ "thoughtful", "thursday", "second", "matt" ]
author: Rob Nugen
date: 2024-03-21T19:36:27+09:00
draft: false
---

Thank you Matt for a beautiful night exploring the theme of letting
go.  Matt offered a new intention setting exercise which worked quite
synchronistically well as people basically offered insightful guidance
from and to the universe

And thanks to Natasha for introducing the cool idea of being a 'time
millionaire'.  🙏

<img
src="https://b.robnugen.com/journal/2024/2024_mar_21_thoughtful_thursday.jpeg"
alt="2024 mar 21 thoughtful thursday."
class="title" />
