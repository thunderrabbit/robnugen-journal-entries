---
title: "Soulmate Level 2 Canceled"
tags: [ "soulmate", "sarita", "hmmm" ]
author: Rob Nugen
date: 2024-09-17T13:50:21+09:00
draft: false
---

(received 21:51 Tuesday 17 September 2024 JST)

### Important Update: Soulmate Level 2 Retreat Cancellation

    Dear Couple,
    
    It is with a heavy heart that we must inform you that the Soulmate
    Level 2 retreat from October 5th to 12th has been cancelled. Due to
    unforeseen circumstances that arose suddenly last week, Sarita is no
    longer able to teach during this period. We initially explored every
    possibility to move forward with the retreat, but sadly, it has become
    clear that continuing is no longer feasible.
    
    We want to be transparent with you regarding our decision. Of the 16
    couples originally enrolled, the majority have opted to either move
    their spot to the February Level 2 retreat or requested a refund. This
    has left us with too few participating couples to run the training in
    a way that supports and serves both the participants and the teaching
    team and makes it sustainable.
    
    Given these circumstances, we have made the difficult decision to
    cancel this training. We understand this is disappointing and may
    require some significant and unexpected adjustments and changes to
    your plans. We deeply apologize for any inconvenience. We are
    committed to making this transition as smooth as possible for you.
    
    For those that haven’t already, **we invite you to join us for the
    February Level 2 retreat (Feb 11 – 18, 2025)**, and any deposits or
    payments will be moved forward if that is your preference. We will
    also honor the same pricing, including any discounts you were
    promised, despite the fact that starting in January 2025 all
    Tantra-Essence retreats in Bali have received an increase in price.
    
    Additionally, due to this level 2 cancellation, it is possible that
    the Level 3 in May and the Level 4 in October 2025 with Sarita in Bali
    will not be feasible as it may be difficult for some to travel to Bali
    3 times per year. If you were planning to attend Level 3 or 4 next
    year, please reach out to us by the end of the week, as we are
    gathering feedback to assess whether these retreats will still be
    viable next year or whether we will need to cancel them too. Affected
    participants who already signed up will be contacted individually.
    
    The Soulmate training is such a unique and profoundly transformative
    journey. Given that it spans seven levels over several years, it’s
    natural for there to be both ups and downs along the way. We
    understand that, being at the beginning of this process, it may be
    difficult to see the full benefits or where exactly the journey is
    headed. We assure you that the journey will become much more clear and
    becomes only more profound as the levels continue.
    
    Due to this unique nature, from an organizational perspective, this
    journey comes with its own set of challenges due to various factors,
    the primary one being the ‘funnel effect.’ This makes it incredibly
    difficult to organize in a smooth and sustainable way that serves
    everyone involved. However, we are committed to doing our best to
    ensure a meaningful, impactful and continued experience for all
    participants.
    
    *On a personal note from Dieke: being on the Soulmate journey
    personally and having had the privilege of organizing the Soulmate
    trainings in Bali over the past years, we’ve come to see and learn
    that this work has its own rhythm and spirit. Though this shift may be
    difficult to understand right now, we trust that there is a higher
    purpose behind it all.*
    
    We are deeply sorry for the disruption and any inconvenience this may
    cause. Please don’t hesitate to reach out with any questions or
    concerns. We are doing our best to navigate this unexpected turn of
    events, and we appreciate your kindness and understanding as this has
    been a challenging moment for our team as well.
    
    Sending love and hoping to see you in February for the Level 2 retreat. 
    
    With love, 
    
    Dieke, Modestas & Sarita 

<hr>

Jess and I have already booked our flights for Soulmate Level 2,
scheduled for 5-12 October.  Since the retreat is cancelled, we plan
to have a nice vacation in Ubud instead of attending the retreat.

We won't join the suggested February 2025 dates as Jess will be
running a workshop that weekend, so she asked if there will be a Level
2 available in April.  (I'm guessing there won't be.)

We shall see!