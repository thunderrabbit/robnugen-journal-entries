---
title: "Fun Full Moon Meditation"
tags: [ "moon", "meditation", "rain", "fun", "clare", "kaz", "shinagawa" ]
author: Rob Nugen
date: 2024-09-18T21:38:48+09:00
draft: false
---

(written 13:38 Thursday 19 September 2024 JST)

[![rain likely at 6pm 1000](//b.robnugen.com/journal/2024/walk/thumbs/rain_likely_at_6pm.png)](//b.robnugen.com/journal/2024/walk/rain_likely_at_6pm_1000.png)

Nice time yesterday even though it rained on us a bit.  It was nothing
like last month with me, Ever, and Shinagawa-san dancing in the rain
with DJ God on the Lightning and Thunder!

Anyway, yesterday was fun, too.

[![clare rob shinagawa kaz 1000](//b.robnugen.com/journal/2024/walk/thumbs/clare_rob_shinagawa_kaz.jpg)](//b.robnugen.com/journal/2024/walk/clare_rob_shinagawa_kaz_1000.jpg)

[![flying 1000](//b.robnugen.com/journal/2024/walk/thumbs/flying.jpg)](//b.robnugen.com/journal/2024/walk/flying_1000.jpg)

[![shinagawa clare rob see moon
1000](//b.robnugen.com/journal/2024/walk/thumbs/shinagawa_clare_rob_see_moon.jpg)](//b.robnugen.com/journal/2024/walk/shinagawa_clare_rob_see_moon_1000.jpg)

[![kumorebi 1000](//b.robnugen.com/journal/2024/walk/thumbs/kumorebi.jpg)](//b.robnugen.com/journal/2024/walk/kumorebi_1000.jpg)
[![praying 1000](//b.robnugen.com/journal/2024/walk/thumbs/praying.jpg)](//b.robnugen.com/journal/2024/walk/praying_1000.jpg)

