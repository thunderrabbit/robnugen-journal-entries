---
title: "Found François"
tags: [ "francois", "yay" ]
author: Rob Nugen
date: 2024-09-18T00:13:28+09:00
draft: false
---

(written 00:13 Wednesday 18 September 2024 JST)

After finding some [old photos of François in Palestine](https://perl.robnugen.com/journal.pl?type=all&date=2005/10/31#Today), and learning
Soulmate Level 2 has been canceled/postponed until February, I was
like, "hmmm I wonder if I can find François."

Success!

[![2024 sep 18 found francois](//b.robnugen.com/journal/2024/thumbs/2024_sep_18_found_francois.png)](//b.robnugen.com/journal/2024/2024_sep_18_found_francois.png)
