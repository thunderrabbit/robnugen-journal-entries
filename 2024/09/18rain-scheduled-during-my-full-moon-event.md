---
title: "Rain"
tags: [ "rain", "dance", "haha", "full moon" ]
author: Rob Nugen
date: 2024-09-18T13:47:13+09:00
draft: false
---

(written 13:47 Wednesday 18 September 2024 JST)

As of last night rain was expected during my full moon event.

Now it's expected just *before* the full moon event.

Either way, I'm looking forward to it!!
