---
title: "Found my first chapter"
tags: [ "book", "im fine" ]
author: Rob Nugen
date: 2024-09-18T07:10:52+09:00
draft: false
---

(written 07:10 Wednesday 18 September 2024 JST)

I woke up today with a newfound realization that part of the reason
I've been having trouble writing is that I haven't been reading much
for years.  I bounced out of my meditation (27 minutes) and sought Tim
S. Grover's book _RELENTLESS_ which has been chilling on my shelf for
probably 3+ years.

First pages shares a story about being relentless.

Story.   Emotions.   Ah yes that kid crying on the train.  Where did I
write that?  Thank God for Google Docs.  "crying" brought it up in
short notice.  It's perfect.  Thank you, Soness for helping me craft
it.

It's perfect for Chapter 1 of I'M FINE!
