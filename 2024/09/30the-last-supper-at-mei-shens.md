---
title: "The Last Supper at Mei Shen's"
tags: [ "funny" ]
author: Rob Nugen
date: 2024-09-30T22:44:40+09:00
draft: false
---

(written 22:44 Monday 30 September 2024 JST)

<img
src="https://b.robnugen.com/journal/2024/the_last_supper_1000.png"
alt="the last supper 1000"
class="title" />
<img
src="https://b.robnugen.com/journal/2024/2024_sep_29_mei_shen_1000.jpg"
alt="2024 sep 29 mei shen 1000"
class="title" />
