---
title: "scanned journal notes written 5 years ago"
tags: [ "notes", "images", "brazil", "old", "paper" ]
author: Rob Nugen
date: 2017-12-23T18:48:38+09:00
draft: false
---

(written 18:48 Thursday 23 February 2023 JST)

These are from when Lin and I went to see her friends and family in
Brazil.

Perhaps future Rob or an AI or someone can transcribe them?

If I have an assistant who finds these, feel free to transcribe them.

Before doing so, check to see if they have been transcribed into files
in this dated journal directory adjacent to this text file, not the
b.robnugen.com directory.

If they have, then remove this note, I guess as well or archive it or
whatever makes sense.

[![2017 dec 26 brazil journal](//b.robnugen.com/journal/2017/thumbs/2017_dec_26_brazil_journal.png)](//b.robnugen.com/journal/2017/2017_dec_26_brazil_journal.png)
[![2017 dec 21 brazil journal](//b.robnugen.com/journal/2017/thumbs/2017_dec_21_brazil_journal.png)](//b.robnugen.com/journal/2017/2017_dec_21_brazil_journal.png)
[![2017 dec 25 brazil journal](//b.robnugen.com/journal/2017/thumbs/2017_dec_25_brazil_journal.png)](//b.robnugen.com/journal/2017/2017_dec_25_brazil_journal.png)


Here are some notes from ConSwi design

[![2017 dec conswi notes](//b.robnugen.com/journal/2017/thumbs/2017_dec_conswi_notes.png)](//b.robnugen.com/journal/2017/2017_dec_conswi_notes.png)
