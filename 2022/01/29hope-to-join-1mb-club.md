---
title: "Hope to join 1MB club"
tags: [ "1mb club", "site", "size" ]
author: Rob Nugen
date: 2022-01-29T23:04:29+09:00
draft: false
---

I know my images are too large as they are sent.  I tried effing with
code on b.robnugen.com to shrink them according to the size of the
browser, but never really solved the issue. (Possibly because FF tries
not to leak screen size and other identifying info.)

Right now according to this site / page, the top page of my site is
2.77MB

https://gtmetrix.com/reports/www.robnugen.com/Ex2jEHuW/

1.87MB worth of images and 0.72MB worth of JS.  Hmm not great, and I
am sure I can do better, at least on the images, and probably on the
JS.  TBH I don't know what JS is being used on my site. If I added it,
I don't recall.

