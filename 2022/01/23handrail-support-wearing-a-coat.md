---
title: 'handrail support wearing a coat'
author: 'Rob Nugen'
date: '2022-01-23T09:48:34+09:00'
---

I found this coat on the ground two nights ago and hung it on the rail.  It's apparently been there since then!

[![2022 jan 23 handrail support wearing a coat](//b.robnugen.com/journal/2022/thumbs/2022_jan_23_handrail_support_wearing_a_coat.jpeg)](//b.robnugen.com/journal/2022/2022_jan_23_handrail_support_wearing_a_coat.jpeg)          

##### 11:43

Well that was quick

[![2022 jan 23 apparently cold post](//b.robnugen.com/journal/2022/thumbs/2022_jan_23_apparently_cold_post.jpeg)](//b.robnugen.com/journal/2022/2022_jan_23_apparently_cold_post.jpeg)          
