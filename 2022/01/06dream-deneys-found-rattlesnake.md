---
title: 'dream Deneys found rattlesnake'
author: 'Rob Nugen'
date: '2022-01-06T06:59:32+09:00'
---

<p class='dream'>Was camping with Deneys and he found a rattlesnake by the door and knew how to handle it and helped me hold it by its neck and I got a very slight stab on my finger when I was holding it but thought everything was okay and then people were looking at the snake while I was holding it and I began to fall asleep so we decided to throw it outside and I threw the snake outside the door and it climbed up onto a toy fire truck and started to drive the truck and spray water around the yard and it was pretty entertaining to watch a snake drive a fire truck and then it came up toward the steps again and I tried to kick it down and kicked myself awake.</p>
