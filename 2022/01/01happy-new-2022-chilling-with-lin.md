---
title: 'Happy New 2022 chilling with Lin'
author: 'Rob Nugen'
date: '2022-01-01T20:59:26+09:00'
---

Lin and I are watching LoTR 2 now, after watching the first LoTR from about 5pm.

They're currently in the bog with flying ring wraiths while the other 3 are looking for Merry and Pippin in the forest.

Today I worked on AB, cleaning some old code that complicated things that weren't needed: translation of error messages on the backend.  (In ~9+ years, the messages have never been translated.)

In other news, I had a bit of a fever on Dec 30th and sadly canceled the Walk and Talk event in 林試の森公園 that day.  In addition to Anthony joining, apparently Kyoko invited some of her friends.  Wow super happy to have such invitation support!

Yesterday we also canceled going to Lin's sister's for HNY festivities.
