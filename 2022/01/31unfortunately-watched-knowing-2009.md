---
title: 'unfortunately watched Knowing (2009)'
author: 'Rob Nugen'
date: '2022-01-31T00:21:36+09:00'
---

I'm mad at myself for watching this movie.  I just couldn't seem to take my eyes off it, hoping it would somehow get better but it just kept getting worse.

Knowing was approximately the worst directed movie I've ever watched, especially given how much effort they obviously put into it.

I've got gripes about ridiculous simulations and ridiculous physics and many ridiculous things

My gripes are in addition to all the gripes people generally had on IMDB.

They lock a time capsule under a manhole cover, hoping kids won't lift it and take it out for fifty years.

Fifty years later, they hand the fragile paper out like candy to kids without taking any consideration for who got what or what's even in the thing.

John is a professor at MIT, but he has less empathy than my cat. When he approaches a plane crash victim, who's literally on fire running from a burning plane, John yells "hey!  HEY!!" at him like "I wanna talk to you!"

John (still a professor at MIT) doesn't know how to begin a conversation with a stranger except "hey my kid randomly got a note from your dead mom so you need to come with me."

Of course she would run away!  Why waste those screen minutes on a pointless conversation that doesn't move the plot?

John knows the importance of the numerical sequence but freaks out when his son displays the same abilities.  Was he freaked out because his son was wasting time given that the world was going to end the next day?  Why was his son given this ability? What was he predicting?  It's another wasted throwaway scene that makes no sense within the movie.

And then when John (believes he) solves their survival puzzle, he gets so focused he won't talk to the only adults who are willing to be near him.

They've got a graphic of a solar flare jumping out from the surface of the Sun that blows out past the first four planets, all of which were perfectly lined up.  It blows out past Earth in approximately a half a second.  Let's say it took one full second.  So they're predicting a solar flare will travel (let's see.. 8*60+40) ≈ 520 times light speed.  Interesting!  Even Quora knows better!

https://astroengine.com/2009/08/23/knowing-how-solar-flares-dont-work/

They claim its destruction will pass a mile down into the crust of the Earth.

Even if that were possible in this movie's universe, the actual effect just showed it torching all the buildings off the surface.

If you're going create a sciency movie, and hire someone like Nick Cage, hire some actual scientists so things actually makes some kind of sense.

Everyone's phones all die with no signal but John's still works when his kid calls him on the payphone.

Oh, and the gas station guy knows exactly who John is talking about from *30 minutes ago* as everyone in the place is going berserk and looting things.

Okay and so the magic spaceship had the diameter of approximately, oh I don't know, Manhattan, but they couldn't find the room to bring on one more guy that their chosen one obviously was very interested in having come along?  Why would the kids each get to bring rabbits but the boy didn't get to bring his own dad?

The claim that John didn't hear the call is preposterous because he was literally the only person who was able to decipher the message and get to their getaway ship without escorts.

And at the very end, they show 10 spaceships leaving the new planet but only represent that there's two kids on this new planet to be Adam and Eve?   Why did they need 10 spaceships to carry two kids to the planet?

Were the other ships like decoy limos in a presidential motorcade?  
