---
title: 'fixed posts from my phone'
author: 'Rob Nugen'
date: '2022-01-02T19:16:39+09:00'
---

I fixed the language suffix on my domain and now need to deal with the fact that the repository for my journal is separate and the repository for my main website.

Hopefully I fixed it now and can post with my phone and build the site the same time.
