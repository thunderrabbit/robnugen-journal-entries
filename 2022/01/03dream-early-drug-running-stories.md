---
title: 'dream: early drug running stories'
author: 'Rob Nugen'
date: '2022-01-03T09:56:01+09:00'
---

<p class='dream'>I was sitting in the passenger side of an 18-wheeler in an underground parking garage  and then there was a flash outside the driver's side door.</p>

<p class='dream'>A man with no arms got into the truck with me and I didn't know if he had just lost his arms in the flash, but it was suddenly clear I had to drive.</p>

<p class='dream'>He told me verbally which way to go and to run some red lights on the right hand side of a highway and going the wrong direction through traffic.</p>

<p class='dream'>With his instructions I was willing to break the rules and yet I made sure I didn't crash or hurt anyone.</p>

<p class='dream'>Going quickly along the highway there was suddenly everyone stopping on the hill ahead of us so I veered into the left hand shoulder lane and up past the stopped traffic and safely got ahead of many cars.</p>

<p class='dream'>The traffic started flowing again and a man on a bicycle got mad at me for being right behind him.  I knew he wanted to make me have an accident but I was certain to monitor him carefully so he wouldn't cause any problem.</p>

<p class='dream'>We arrived safely after this wild traffic and then could hear narration of wild drug stories, for the story was the beginning of a long career in drug running history.</p>

<p class='dream'>"It was three great days in the mountains actually 5 days" and watching the scene as camera floated over naked woman's body and through walls in a hotel.</p>

<p class='dream'>The narrator lamented "No one spanked my butt when I did something wrong nor praised me when I did something good."</p>

<p class='dream'></p>
