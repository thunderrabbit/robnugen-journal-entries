---
title: "jumped into my sleeping body"
tags: [ "dream", "lucid", "mirror" ]
author: Rob Nugen
date: 2022-01-21T19:39:30+09:00
---

##### 19:39 Friday 21 January 2022 JST

<p class="dream">
Standing in front of a mirror when I took off my jacket, but as I
lifted it over my head the mirror image didn't change until the jacket
was covering my eyesight .. at which point it looked like I was still
wearing the jacket.
</p>

<p class="dream">
I played with this curiosity a few different times and noticing that
the mirror didn't reflect what was happening and then I realized I was
dreaming and then I realized I was dreaming and looking into a mirror
(which is the first step the guy (forgot his name) was saying to do
when we first astral project)
</p>

<p class="dream">
So I looked carefully at myself in the mirror. It looked exactly like
my face would normally look in a mirror, though maybe a little bit
younger. The mirror image basically wasn't moving to match my motion
though I wasn't moving very much in the dream.
</p>

<p class="dream">
So I thought "okay let's jump out of body" and I dove out of my body
forward into the mirror and briefly expected to bump my head on the
mirror but that didn't happen (because I was still bed).
</p>

<p class="dream">
After I jumped out of my body I saw darkness and realized I needed to
have protection in the astral, so invited protection and I could feel
the sensation not like I had jumped out of my body but like I had been
outside of my body while looking at the mirror, and my jumping into
the mirror was actually jumping back into my body.
</p>
