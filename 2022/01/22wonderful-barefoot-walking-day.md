---
title: 'wonderful barefoot walking day'
author: 'Rob Nugen'
date: '2022-01-22T19:25:51+09:00'
---

1/22/22  (土)

#### 10:31am

This morning I helped the volunteer group by pulling weeds at Shin Yuri Art Park.

I left early because I plan to do another barefoot walking event in Meguro at 林試の森公園.

I will arrive in 武蔵小山駅 by about 11:15, where has the official meeting time at that station is 11:40 a.m., so I'm actually quite early.  This gives me some time to chill on a train with seats and write this entry.

#### 11:25

Now arrived at Musashi Koyama station, later than I expected but still earlier than scheduled.

#### 16:00

We had a great time filming and doing an interview plus walking around the meeting space.  The meeting space is aptly named because we met a couple there.  Carlos and Kayla made join our next barefoot walking event in 林試の森 park, scheduled for February 26th.



