---
title: "one month after starting OOMG group in MKP Japan"
tags: [ "thoughts", "mkp", "japan", "oomg", "com" ]
author: Rob Nugen
date: 2022-01-29T17:34:52+09:00
draft: false
---

I'm feeling fear, sadness, shame, joy.

{{% img-responsive
"https://b.robnugen.com/journal/2022/2022_jan_29_mkp_japan_thoughts.jpg"
"2022 jan 29 mkp japan thoughts" %}}

Above,

* MKP = ManKind Project https://www.mkp.org/  https://www.mkpjapan.org/
* OOMG = Open Online Men's Group https://mkpusa.org/mens-group/
  https://www.mkpjapan.org/open-mens-group/
* CoM = Circle of Men https://mkpusa.org/acircleofmen/
* NWTA = New Warrior Training Adventure
  https://mkpusa.org/new-warrior-training-adventure/
