---
title: "seeing signs of dementia"
tags: [ "dementia", "mama" ]
author: Rob Nugen
date: 2022-01-31T11:58:27+09:00
draft: false
---

Lin's mom has been referring to our cat Jennie as Bonnie,
who was their (same-size) dog who died 11+ years ago.

Hmm what could be causing this?

Let's see ([ ] means bad):

* [ ] Don't smoke.
* [ ] Stay at a healthy weight.
* [ ] Get plenty of exercise.
* [?] Eat healthy food.
* [?] Manage health problems including diabetes, high blood pressure, and high cholesterol.
* [ ] Stay mentally alert by learning new hobbies, reading, or solving crossword puzzles.
* [ ] Stay involved socially. Attend community activities, church, or support groups.
* [?] If your doctor recommends it, take aspirin.

https://stanfordhealthcare.org/medical-conditions/brain-and-nerves/dementia/prevention.html

Very frustrating to see, and feeling helpless to do anything about it.
The only exercise she gets is to go outside and smoke.
Otherwise it's just sit inside and watch movies.
