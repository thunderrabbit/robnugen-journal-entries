---
title: 'straight lines cut in platform'
author: 'Rob Nugen'
date: '2022-01-22T11:10:20+09:00'
---

I like watching construction unfold over time and trying to predict what they are doing.

It's common for stations to add gates along platform edges to help prevent suicides.. this has been going on for many years as I've lived in Tokyo area.

In Noborito station they added gates on the platform along Odakyu Line, but they haven't yet put gates on the Nambu Line in Noborito station.

I wouldn't be surprised to see telltale cuts along the edge of the platform, so I *am* surprised to see square shapes cut in the middle of Nambu platform in Noborito.

They seem to have cut a set of rectangles, but the rectangles themselves are not lined up with each other or anything that I can really recognize.

Best I can tell, they look like they are in the common walking path from the stairs onto the platform.

[![2022 jan 22 lines cut by noborito stairs](//b.robnugen.com/journal/2022/construction/thumbs/2022_jan_22_lines_cut_by_noborito_stairs.jpeg)](//b.robnugen.com/journal/2022/construction/2022_jan_22_lines_cut_by_noborito_stairs.jpeg)


[![2022 jan 22 bare feet on noborito platform near cuts](//b.robnugen.com/journal/2022/construction/thumbs/2022_jan_22_bare_feet_on_noborito_platform_near_cuts.jpeg)](//b.robnugen.com/journal/2022/construction/2022_jan_22_bare_feet_on_noborito_platform_near_cuts.jpeg)
