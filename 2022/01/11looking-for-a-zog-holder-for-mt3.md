---
title: 'looking for a Zog holder for MT3'
author: 'Rob Nugen'
date: '2022-01-11T13:00:42+09:00'
---

I'm headed to Akihabara to hopefully find an articulated skeleton that I can use to move Zog in an arc as Candy Mama throws it.

I've taken a few frames, actually maybe just one.. I've tried hanging Zog from threads, and holding it on the end of a wire. So far, both techniques have been too hard to control Zog precisely enough.

