---
title: 'Barefoot walk from Karakida to Tama Center Station'
author: 'Rob Nugen'
date: '2021-12-04T17:28:00+09:00'
---

<div class="note">(from memory 15 Jan 2022, also written on page 105 of my journal volume BOOK TWENTY)</div>

On Saturday December 4th 2021 Andy and I met on the train to go to Karakida station.

We met Kyoko outside the station and walked nearly 3 hours through larks including Maronie, and a long tree lined section split by a highway below sidewalks which went over the highway.

We saw a water well covered with a rebar dragon statue and got kicked out of the flight school which used to be a hotel.

We were serenaded by a guy playing guitar on the grassy field before walking up to the top of the stairs which overlook the wife sidewalk leading down toward Tama Center Station.

At the top of the stairs are some chrome cubes on which Kyoko stood facing the sun in meditation as I lay on the ground and warmed my feet as Andy went to go look at the red gates and then came walking toward us through the water in the shallow reflection pool.

We ate at Saizeria afterward and planned to do the whole thing again sometime soon.
