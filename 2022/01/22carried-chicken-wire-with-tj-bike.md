---
title: 'carried chicken wire with TJ Bike'
author: 'Rob Nugen'
date: '2022-01-22T20:09:19+09:00'
---

[![2022 jan 22 tj bike carries chicken wire](//b.robnugen.com/journal/2022/thumbs/2022_jan_22_tj_bike_carries_chicken_wire.jpeg)](//b.robnugen.com/journal/2022/2022_jan_22_tj_bike_carries_chicken_wire.jpeg)

I taped the rolls of chicken wire to both sides of TJ Bike as I rode home tonight.  It worked quite well and I'm really happy.
