---
title: 'SOGO then lunch'
author: 'Rob Nugen'
date: '2022-01-16T14:54:39+09:00'
---

Today was the second workout this year in yoyogi park for sun's out fitness and we had a relatively low turnout though the weather was pretty much fantastic although the ground was a bit wet in some areas.

The best part though: the big field is slowly being opened up and we were able to run a circuit around the field where the center was still closed off.  We did two laps around that center island and on the second lap I got to catch a frisbee being thrown by a few guys after I tried to catch the frisbee that they were throwing while I was running.
