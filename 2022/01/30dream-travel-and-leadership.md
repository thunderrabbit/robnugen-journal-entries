---
title: "Dream travel and leadership"
tags: [ "dream", "dream", "yruu", "mkp", "leadership" ]
author: Rob Nugen
date: 2022-01-30T09:07:59+09:00
---

##### 09:07 Sunday 30 January 2022 JST

<p class="dream">
So happy the youth group I had started long ago was growing. As I
arrived back they were doing a car wash with about 15 or 20 youth
there and I was saying I was so proud of them and was this happy
crying laughing and so glad it wasn't a dream.
</p>

---

<p class="dream">
There was a concrete seat outside and I had opened it before, so we
were trying to open one side which had some simple round things that
would turn with a screwdriver but it was all rusted out and the
screwdriver wouldn't work so I went to the other side of the bench and
it was actually unlocked and there was a file cabinet inside that was
also unlocked.
</p>

<p class="dream">
I was worried that rain would get inside so I took that thing that I
wanted to check, which was some old documents that I wanted to scan
and I put those into my shopping cart and closed the thing by lifting
the front panel back in place and then realizing the key was inside,
so I needed to reopen it to get the key and opened it, got the key,
closed it, locked it and then put the key where the next person could
find it easily enough. I saw the key was on a plastic keychain that
was loose on the ends so I needed to tie it together somehow and made
a little half hitch knot 🪢 and then with the loose ends wanted to tie
it together in a way so it wouldn't come undone. I realized that each
link was slightly soft and they could be merged together so I pushed
the two links together and rotated one side so that it wouldn't easily
come apart.
</p>

<p class="dream">
I got the box locked up and safe from the elements.
</p>

---

<p class="dream">
Arrived in a train station that I didn't know and I had a list of
train stations and I asked a guy if this station was near the list I
had.
</p>

<p class="dream">
I couldn't understand his answer but I just gave up and stood near the
station desk which was kind of like an information booth with open
sides and looked very casual
</p>

<p class="dream">
There was a placemat covered in chocolate chip cookies and they
offered them and I jokingly took the mat and he immediately pulled it
back and I said I was just kidding and I took one cookie which I
immediately bit and said oishii. One bit of cookie fell and I was
worried it fell on the ground, but it fell on my jacket I found later.
</p>

<p class="dream">
I ate the cookie and then that information desk opened up into a
construction site that I wasn't really supposed to be in, but I talked
to the guys and kind of edged my way inside, sitting on a ledge over
the construction site just while I ate the cookie.
</p>

<p class="dream">
They were talking about some kind of a game or construction project.
</p>

<p class="dream">
Son and I had been traveling together and exploring this area earlier
and we left our stuff somewhere that I could still see.
</p>

<p class="dream">
I ate the rest of the brownie and some of the crumbs fell on the
ground in the construction site too far away to pick up but no one
really worried about it and so I didn't worry about it.
</p>

<p class="dream">
It was time to go up the public corridor alongside the construction
site, so I wanted to put my things back where I had left the rest,
tucked in a corner of the construction site next to a wall separating
from the public corridor.
</p>

<p class="dream">
As I went back there I could see long tree cutters that had already
been there and right at the clippers, I noticed there was a small hole
in the ground. I looked down into the hole and realized there was a
light on the bottom I realized it was on and it was shining into a
tunnel from a different place, which actually looked more open. I
wanted to explore, but the hole in this side with the light was too
small to go in. I also wondered if I would be afraid to get stuck in
the tunnel, which looked like I would have to crawl to go through. I
can see the tunnel was designed so we wouldn't have to walk on the
land and we would be able to walk in a dry place.
</p>

<p class="dream">
This was curious because there was a much higher roof over the entire
facility that protected us from any elements.
</p>
