---
title: 'twelve attendees at Shin Yuri Art Park'
author: 'Rob Nugen'
date: '2022-01-20T16:22:20+09:00'
---

{{% img-responsive "https://b.robnugen.com/blog/2022/walk_and_talk/2022_jan_20_art_park_barefoot_group.jpg" "2022 jan 20 art park barefoot group" %}}

Today was the third Thursday, so we had our monthly barefoot walking event in ShinYuri Art Park.

Twelve people attended!  I think that's the highest number, which is neat given how cold it was.  Super happy to see everyone!

