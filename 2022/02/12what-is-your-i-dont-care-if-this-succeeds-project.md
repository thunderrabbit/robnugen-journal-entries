---
title: "What is your \"I don't care if this succeeds\" project?"
tags: [ "hn", "mt3" ]
author: Rob Nugen
date: 2022-02-12T19:44:47+09:00
draft: false
---

Thank you JNRowe https://news.ycombinator.com/item?id=30283264

My reply: https://news.ycombinator.com/item?id=30311884

I'm working on my first animation piece: a full length stop motion
movie of characters building Marble Track 3, a working rolling ball
track made of wood and glue. (popsicle sticks, chopsticks, toothpicks,
etc.)

I've been working about 2 hours per week since 2017. 10,000+ frames
taken so far. At 12 frames per second, the video runs about 14
minutes.

It all started after I uploaded a video of Marble Track 2
https://www.youtube.com/watch?v=mlUqu6QE7bw (nsfw language) and some
people asked me *how* I made the marble track.

This URL will show (a playlist of the snippets showing) in great
detail exactly how to create a marble track.

https://mt3s.marbletrack3.com/

The snippets are still silent, given that filming isn't even close to
completion.

On the same channel, there are behind the scenes videos of me creating
the track. https://www.youtube.com/channel/UCHiQhB8J_KI2LYQ7dsexfLw

I have done some livestreams and recently some "summary" videos, done
in a timelapse style.

Because work on the track takes a lot of time, almost every piece has
its own name, and the site https://www.marbletrack3.com/ aspires to
document all the pieces, and show which characters made which parts of
the track.

While I am hoping the project organically gains visibility and
financial support, I am not pushing for that so I retain creative
freedom. In any case, I will keep making the movie of characters
making Marble Track 3.