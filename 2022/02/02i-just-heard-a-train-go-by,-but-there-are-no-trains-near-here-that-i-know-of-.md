---
title: "I just heard a train go by, but there are no trains near here that I know of "
tags: [ "ghost train" ]
author: Rob Nugen
date: 2022-02-02T23:00:21+09:00
draft: false
---

(written 23:00 Wednesday 02 February 2022 JST)

At 22:59 I heard the clickity clack of a train going by.

This is the second time I have heard it around this time of night.

Possible sources:

* music.  I was listening to
  https://www.youtube.com/watch?v=mbNi5L3QOCA and just now paused it
  at 1:47:43
* Train going by Shin Yurigaoka station (without stopping) (but why
  haven't I heard it during the day?)
* subway construction with train being tested (it's still 9 years away
  I thought, but hmmm)
* something else

I hope it's the third one, which would mean the new train line will go
right under us which is kinda cool, but then again, clickity clacks
will happen regularly during the day when the line is opened.

To be honest, they're quite quiet. For example, I don't think Lin
would notice unless I point it out to her.