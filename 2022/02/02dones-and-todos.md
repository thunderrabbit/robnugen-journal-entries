---
title: 'dones and todos'
author: 'Rob Nugen'
date: '2022-02-02T23:38:58+09:00'
---


I'm pretty happy that I got almost everything done today that I wanted to.

[![2022 feb 02 done did today](//b.robnugen.com/journal/2022/thumbs/2022_feb_02_done_did_today.jpeg)](//b.robnugen.com/journal/2022/2022_feb_02_done_did_today.jpeg)

I even wrote down a list of 20 different ways that I can expand my barefoot walking events

[![2022 feb 02 possible ways to expand barefoot events](//b.robnugen.com/journal/2022/thumbs/2022_feb_02_possible_ways_to_expand_barefoot_events.jpeg)](//b.robnugen.com/journal/2022/2022_feb_02_possible_ways_to_expand_barefoot_events.jpeg)

Here's my to do list for tomorrow, starting with backing up MT3 frames I took on Tuesday.

[![2022 feb 02 planned todos for february 3rd](//b.robnugen.com/journal/2022/thumbs/2022_feb_02_planned_todos_for_february_3rd.jpeg)](//b.robnugen.com/journal/2022/2022_feb_02_planned_todos_for_february_3rd.jpeg)          
