---
title: "story of I"
tags: [ "servicespace" ]
author: Rob Nugen
date: 2022-02-16T09:12:28+09:00
draft: true
---

Today I am going to work 3 hours (and be in transit nearly 3 hours).
I like the people and the topic (science!) and therefore keep doing
the work even though it's not the most money I could be making.

