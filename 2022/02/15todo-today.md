---
title: "todo today"
tags: [ "todo", "today" ]
author: Rob Nugen
date: 2022-02-15T09:53:38+09:00
draft: false
---

1. ✓ Get mirror from bathroom as they take the place apart for renovation
2. Stop playing Antimatter Dimensions for at least 3 hours
3. ✓ Work on AB at least 3 hours
4. Marble Track 3 (maybe)
5. ✓ Online Open Men's Group from 18:30

[![2022 feb 15 antimatter dimensions](//b.robnugen.com/journal/2022/thumbs/2022_feb_15_antimatter_dimensions.png)](//b.robnugen.com/journal/2022/2022_feb_15_antimatter_dimensions.png)


