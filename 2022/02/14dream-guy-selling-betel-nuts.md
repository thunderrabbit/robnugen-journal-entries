---
title: 'dream: guy selling betel nuts'
author: 'Rob Nugen'
date: '2022-02-14T07:18:06+09:00'
---

There was a guy trying to sell drugs and telling stories about how great they were and recommending that people try what he had on offer for free and then join his community and all that stuff. Everybody in my group were like oh yes and they were happy to sign up and then I was like all right what's the big deal and I went back to try and was like oh hang on you're selling betel nuts as drugs?

He's like yeah I'm like, you know these are just from Papa New Guinea and they're free there. I didn't try when I was there but at least I'll get a ripe one now.. because at least half the ones he had we're not ripe.

So I chose one but before I could even try it there was some kind of a message saying that we needed to go to my friends' party because it was his last one and it was all sad because only nine people came to the previous one and I was like what's the big deal and so I went and when I got there everyone was there like a hundred people and I realized it certainly does help to promote things online.
