---
title: 'dream: River offering self receive key and gift'
author: 'Rob Nugen'
date: '2022-02-07T22:49:09+09:00'
---

<p class='dream'>2022 February 7 Monday 4:45</p>

<p class='dream'>Depart river underground, walking with others out of river, going in specific grooves carved by time.</p>

<p class='dream'>Hard to get lost or go off track.</p>

<p class='dream'>I went off track.</p>

<p class='dream'> up a tunnel ascending from river level</p>

<p class='dream'>One by one</p>

<p class='dream'>
Climbing up, easy then steeper, sandy, slippery.</p>

<p class='dream'>I could still climb easily</p>

<p class='dream'>Helping girl in front of me by holding her bottom of foot to give her traction and lift her up.</p>

<p class='dream'>Caught up to person in front of her and lifted him as well</p>

<p class='dream'>We got to top and emerged to light joyful cheers.  it was like we had arrived out of a well in the cavernous room with tourists and shops.</p>

<p class='dream'>Others exhausted, me fine</p>

<p class='dream'>I jumped over the wall, needed to arrive differently than others who stepped down the ladder</p>

<p class='dream'>Excuse me; where are we?</p>

<p class='dream'>Japan.  Saitama</p>

<p class='dream'>Me?  Tokyo, well Kanagawa</p>

<p class='dream'>Oh! konnichiwa</p>

<p class='dream'>
Sayonara</p>

<p class='dream'>Went to restroom</p>

<p class='dream'>Finally found men's room</p>

<p class='dream'>Very tight entrance 180 degree turn to left with belly against wall.</p>

<p class='dream'>Oh, I can push the curve out to make more room </p>

<p class='dream'>
Girl was on the phone being turned down and berated </p>

<p class='dream'>Broken heart he didn't show up</p>

<p class='dream'>Me: "I'll go with you!  I shouldn't because I'm married but I love amusement parks"</p>

<p class='dream'>Me: "Actually it might be a good thing; I'm much older and can give you insights on your life.</p>

<p class='dream'>" I can answer your questions or reflect them.</p>

<p class='dream'>"Or you can just cry when needed</p>

<p class='dream'>She liked this.  Held my hand</p>

<p class='dream'>Waited for the traffic light</p>

<p class='dream'>Crossing the street. </p>

<p class='dream'>"And he broke up so we lost money on missing ___ event."  </p>

<p class='dream'>Me: "___?"</p>

<p class='dream'>Yes </p>

<p class='dream'>Me: "Who was the organizer?"</p>

<p class='dream'>She:  "Rohini."</p>

<p class='dream'>"Hmm maybe I can talk to her.  I know it's not recorded; they put a lot of effort into it."</p>

<p class='dream'>Still holding her hand. </p>

<p class='dream'>She said her hands were sweaty.  </p>

<p class='dream'>Her boyfriend used to tease her about it.</p>

<p class='dream'>Turned left into nondescript gray dusty passage.</p>

<p class='dream'>Where are we going??  </p>

<p class='dream'>Go with it. Adventure.  Trust</p>

<p class='dream'>Dark door with frosted glass window.  Looked like an office</p>

<p class='dream'>She unlocked the door.</p>

<p class='dream'>She walked right in </p>

<p class='dream'>Deep red mahogany ornate walls with books and statues</p>

<p class='dream'>Her home!</p>

<p class='dream'>Father behind big desk.  He knew my name.  Greeted me in English</p>

<p class='dream'>Mother on the side</p>

<p class='dream'>India? </p>

<p class='dream'>Deep red mahogany walls
 King and queen motifs in different species carved into the walls</p>

<p class='dream'>Thailand!</p>

<p class='dream'>Me: "Sawatdi Krap"</p>

<p class='dream'>He couldn't hear me, mother said.</p>

<p class='dream'>I said it louder.  He was very happy to hear my efforts and instructed me to be more appropriate, I should now say his name:</p>

<p class='dream'>Ruwati Preeti munhere</p>

<p class='dream'>Me: "Sawatdi krap. Ruwati Preeti munher"</p>

<p class='dream'>He: "munher*e*"</p>

<p class='dream'>Feeling embarrassed now
I tried again</p>

<p class='dream'>"Sawatdi krap. Ruwati Preeti munherぇ"</p>

<p class='dream'>"Yes! welcome"</p>

<p class='dream'>fin</p>

<p class='dream'>
</p>
