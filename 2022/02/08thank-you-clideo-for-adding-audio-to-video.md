---
title: "Thank you Clideo for adding audio to video"
tags: [ "clideo", "mt3", "audio", "video", "online" ]
author: Rob Nugen
date: 2022-02-08T10:03:20+09:00
draft: false
---

(written 10:03 Tuesday 08 February 2022 JST)

<p class="note">transcribed from BOOK TWENTY-ONE page 6-7</div>

9:17am Tuesday 8 February 2022

Each time I work on Marble Track 3, I try to document my efforts.  For
a while, I livestreamed to YouTube while creating the track.  It kept
dropping the connection and being annoying so I witched to Reddit's
RPAN, which also dropped connection.

Recently I've been

* recording a timelapse video
* upload to Google Photos
* Download to my computer
* narrate to Otter the stuff I did
* download otter audio
* trim it in Audacity
* ffmpeg add it to video
* upload to YouTube

The process takes a bit so I want to automate parts of it, including
even just knowing where to find the files.

#### 9:43am Tuesday 8 February 2022

I just found "add audio to video" has several results on DDG.  I've
just used clideo.com to trim audio after uploading both video and
audio.  THey're exporting now.  Hopefully all good.

I guess there will be a watermark or something. I would prefer to have
a progress bar, but they just saved me hella long time coding it up
myself.  $9/month or $72/year.

#### 9:46am

done.

The watermark is fine.  Downloading now.  The download is slow:
200KB/sec.  My connection can do 4MB/sec.  It's up to 800KB/sec now.
Ooh 1.1MB/sec for a bit but now crashing down ugain to 200KB/sec over
the time I wrote that.  Now back up to 1.5MB/sec.  I imagine if I use
them in evenings Japan time, their servers will be chillin' and free.

#### 9:53am

Download complete.