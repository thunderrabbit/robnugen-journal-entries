---
title: 'chilly challenge in Yoyogi Park'
author: 'Rob Nugen'
date: '2022-02-12T16:01:24+09:00'
---

There should be pictures here

https://www.robnugen.com/en/blog/2022/02/12barefoot-chilly-challenge-2022-in-yoyogi-park

Alex and James joined me in Yoyogi Park for a nice walk and Alex said
he felt more alive after walking barefoot.

[![2022 feb 12 alex on snow](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_feb_12_alex_on_snow.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_feb_12_alex_on_snow.jpeg)

We got to walk on a thin layer of snow, see an interesting apple in a
tree, see an interesting line in the soil and what looked like new
soil for Flower Land.

[![2022 feb 12 james and rob on snow](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_feb_12_james_and_rob_on_snow.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_feb_12_james_and_rob_on_snow.jpeg)
[![2022 feb 12 apple in tree](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_feb_12_apple_in_tree.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_feb_12_apple_in_tree.jpeg)
[![2022 feb 12 fresh soil line](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_feb_12_fresh_soil_line.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_feb_12_fresh_soil_line.jpeg)          
[![2022 feb 12 old grass and new grass](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_feb_12_old_grass_and_new_grass.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_feb_12_old_grass_and_new_grass.jpeg)


After watching dogs in the three dogs runs, we played with leaves
above the vents under which Chiyoda Line runs.

[![2022 feb 12 leaves blown by vent](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_feb_12_leaves_blown_by_vent.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_feb_12_leaves_blown_by_vent.jpeg)
