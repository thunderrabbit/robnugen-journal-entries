---
title: "Lin and I worked in her garden this morning"
tags: [ "garden", "early", "lin" ]
author: Rob Nugen
date: 2022-05-05T08:21:24+09:00
draft: false
---

We woke up before 5 on 5/5 to go go to the garden.  I tied together
some green poles onto which we'll put a net for viney plants to grow
(tomatoes, cucumbers, peppers)
