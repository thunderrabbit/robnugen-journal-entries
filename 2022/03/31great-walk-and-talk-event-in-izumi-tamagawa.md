---
title: "Great walk and talk event in Izumi Tamagawa"
tags: [ "blue", "izumi", "hugging mari", "andy", "jessica", "windy", "spring", "grass", "barefoot", "walking" ]
author: Rob Nugen
date: 2022-03-30T23:45:15+09:00
draft: false
---

<p class="note">(transcribed 08:05 Thursday 31 March 2022 JST from
BOOK TWENTY-ONE p. 58)</p>

Wonderful day today closing in on AB sortable reports and 5 attendees
at the Mari-inspired walk and talk event near BLUE: Jessica (who sat
at Cafe Naruto with me from 2pm - 3:30pm to chat before (the event)),
Andy who I hadn't seen since Feb (and plans to attend my 19,000th Day
party on April 1st), Hugging Mari, who invited me and Yoshiko, who
invite Asato who seems quite cool.

Yoshiko said she may invite me to do a walk and talk (and speak) with
her group of students.
