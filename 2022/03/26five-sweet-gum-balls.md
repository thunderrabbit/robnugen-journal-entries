---
title: 'five sweet gum balls'
author: 'Rob Nugen'
location: 'geo:35.62655,139.70342;u=500'
date: '2022-03-26T15:02:58+09:00'
---

Great to hang out with Yoshio, Kerry, Jason, and their kids in the park!

Also met Nori, Miki, Ryan, Yuka, and Gerald(?)

[![2022 mar 26 five sweet gum balls](//b.robnugen.com/journal/2022/thumbs/2022_mar_26_five_sweet_gum_balls.jpg)](//b.robnugen.com/journal/2022/2022_mar_26_five_sweet_gum_balls.jpg)
[![2022 mar 26 one sweet gum ball](//b.robnugen.com/journal/2022/thumbs/2022_mar_26_one_sweet_gum_ball.jpg)](//b.robnugen.com/journal/2022/2022_mar_26_one_sweet_gum_ball.jpg)


