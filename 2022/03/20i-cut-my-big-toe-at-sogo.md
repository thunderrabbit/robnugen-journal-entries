---
title: 'I cut my big toe at SOGO'
author: 'Rob Nugen'
date: '2022-03-20T17:58:23+09:00'
---

Slight slice on my left big toe

[![2022 mar 20 toe slice](//b.robnugen.com/journal/2022/thumbs/2022_mar_20_toe_slice.jpeg)](//b.robnugen.com/journal/2022/2022_mar_20_toe_slice.jpeg)
[![2022 mar 20 toe slice close](//b.robnugen.com/journal/2022/thumbs/2022_mar_20_toe_slice_close.jpeg)](//b.robnugen.com/journal/2022/2022_mar_20_toe_slice_close.jpeg)          
