---
title: 'headed to SOGO'
author: 'Rob Nugen'
location: 'geo:35.61735,139.56479;u=45.310001373291016'
date: '2022-03-06T09:52:02+09:00'
---

Today is a beautiful day for SOGO Bootcamp in Yoyogi Park.  It's also the first Sunday of the month, so I'll remain in the park for the ManKind Project Tokyo family and friends gathering.

I plan to head home by 3pm for some domestic duties, possibly including organizing 2021 expenses.
