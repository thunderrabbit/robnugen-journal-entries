---
title: 'lines in the grass today'
author: 'Rob Nugen'
date: '2022-03-19T12:06:18+09:00'
---

[![2022 mar 19 barefoot rob feet and wavy lines](//b.robnugen.com/journal/2022/thumbs/2022_mar_19_barefoot_rob_feet_and_wavy_lines.jpeg)](//b.robnugen.com/journal/2022/2022_mar_19_barefoot_rob_feet_and_wavy_lines.jpeg)
[![2022 mar 19 wavy grassy lines](//b.robnugen.com/journal/2022/thumbs/2022_mar_19_wavy_grassy_lines.jpeg)](//b.robnugen.com/journal/2022/2022_mar_19_wavy_grassy_lines.jpeg)
[![2022 mar 19 wavy grassy line ](//b.robnugen.com/journal/2022/thumbs/2022_mar_19_wavy_grassy_line_.jpeg)](//b.robnugen.com/journal/2022/2022_mar_19_wavy_grassy_line_.jpeg)

These were in ShinYuri Art Park this morning after we had significant rain last night.
