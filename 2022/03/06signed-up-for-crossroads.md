---
title: 'signed up for Crossroads'
author: 'Rob Nugen'
date: '2022-03-06T09:59:50+09:00'
---

Crossroads is a new ManKind Project training I'll attend soon.

https://mkpusa.org/crossroads/

The next session starts in about eight days. I'm looking forward to the experience!



