---
title: 'great time at Rinko Park today'
author: 'Rob Nugen'
date: '2022-03-19T20:27:59+09:00'
---

[![2022 mar 19 rinko parkleaf incoming ](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkleaf_incoming_.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkleaf_incoming_.jpeg)
[![2022 mar 19 rinko parkteru found a rock](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkteru_found_a_rock.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkteru_found_a_rock.jpeg)
[![2022 mar 19 rinko parkrock pass](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkrock_pass.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkrock_pass.jpeg)          

Oh my gosh I forgot to take pictures of the group!


#### 2022-03-19 22:02

yay Emma took group pics!

[![2022 mar 19 rinko parkadam rob teru max clare ](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkadam_rob_teru_max_clare_.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkadam_rob_teru_max_clare_.jpeg)
[![2022 mar 19 rinko parkfoot exercise passing rocks](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkfoot_exercise_passing_rocks.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkfoot_exercise_passing_rocks.jpeg)
[![2022 mar 19 rinko parkflowers](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkflowers.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkflowers.jpeg)
[![2022 mar 19 rinko parkflowery closeup ](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkflowery_closeup_.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkflowery_closeup_.jpeg)
[![2022 mar 19 rinko parkintercontinental ](//b.robnugen.com/blog/2022/walk_and_talk/thumbs/2022_mar_19_rinko_parkintercontinental_.jpeg)](//b.robnugen.com/blog/2022/walk_and_talk/2022_mar_19_rinko_parkintercontinental_.jpeg)          
