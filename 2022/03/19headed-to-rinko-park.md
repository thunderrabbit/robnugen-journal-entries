---
title: 'headed to Rinko Park'
author: 'Rob Nugen'
location: 'geo:35.5549,139.55492;u=24.066999435424805'
date: '2022-03-19T12:12:05+09:00'
---

We left our house at 11:15am, planning to arrive in Rinko Park by 1pm.

There have been some slow segments but seems we'll arrive on time.

We're taking the same (reversed) route we walked home last year on April 16th.

##### 21:08

We made it *just* in time for 1pm.  I'll take the train next time.

Super special thanks to Lin for driving!