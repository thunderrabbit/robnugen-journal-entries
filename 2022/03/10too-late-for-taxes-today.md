---
title: 'too late for taxes today'
author: 'Rob Nugen'
date: '2022-03-10T12:41:27+09:00'
---

I got to the tax office too late to wait in line today. I've learned enough to know that it's easier to not fight it and just ask when is the next opportunity.

8:15am tomorrow it is, then.
