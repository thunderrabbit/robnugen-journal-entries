---
title: "taxing days"
tags: [ "tax", "pun" ]
author: Rob Nugen
date: 2022-03-07T21:11:27+09:00
draft: false
---

The past couple days have been taxing in that Lin and I have had some
challenging conversations yesterday but that's all sorted out and then
today I spent a lot of time entering the expenses from last year which
include the donations from people supporting my walk and the expenses
from my walk.

I have officially now calculated the walk went financially into the
red USD $5,000.