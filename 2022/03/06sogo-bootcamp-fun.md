---
title: 'SOGO Bootcamp fun'
author: 'Rob Nugen'
location: 'geo:35.66907,139.69987;u=42.5'
date: '2022-03-06T13:03:48+09:00'
---

Emi, who usually leads core, led the lower body exercises today in a circuit that kept us on the normal field.  I found it quite challenging!

I was hardly able to do the upper body exercises today in which Doodie had us doing kind of slow motion handstands and lower our feet quietly to the ground.

For the core portion, Daizo had us doing some moves that might be considered break dance.  I met Mimi during that section.

I just wrote the above while standing by the clock waiting from 12:55 p.m. to 1:00 p.m. where no one showed up for the friends and family meet up in the park.

I'll probably stop trying to meet people there and just say meet us in the park because Shraddhan, Adam, and George are already out there and they found it just fine.
