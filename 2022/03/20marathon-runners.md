---
title: 'marathon runners'
author: 'Rob Nugen'
location: 'geo:35.66861,139.67912;u=21.107999801635742'
date: '2022-03-20T10:14:56+09:00'
---


There are a bunch of people running on the street next to Yoyogi Park.

[![2022 mar 20 marathon runners ](//b.robnugen.com/journal/2022/thumbs/2022_mar_20_marathon_runners_.jpeg)](//b.robnugen.com/journal/2022/2022_mar_20_marathon_runners_.jpeg)          
