---
title: 'nearly fixed sortable reports for AB'
author: 'Rob Nugen'
date: '2022-03-28T17:56:32+09:00'
---

Yay great news, I've solved the issue with reports for AB.

Not 100% done but I'm 99.99 percent sure I've got a prototype that will solve all the things.

There are reports that let them view widgets and widget makers.  If they click to view a widget, they can then click prev/next to see the prev/next widget in the list.

If they click a widget maker, though, the prev/next functionality doesn't work...  Until now!  I'm super glad I 99.99% solved it.
