---
title: "Japanese lesson"
tags: [ "vocab", "japanese" ]
author: Rob Nugen
date: 2022-03-08T11:02:20+09:00
draft: false
---

Just had a Japanese lesson with NipponHippo.  Group lessons, so a bit
chaotic, but I still learn(?) a bit.

* 衝撃的　しょうげきてき shocking
* おでこ　forehead (I knew before and forgot)
* 口溶け　くちどけ melts in mouth

Any typos above are my own.
