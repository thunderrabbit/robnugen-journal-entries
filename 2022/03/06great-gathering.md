---
title: 'great gathering'
author: 'Rob Nugen'
location: 'geo:35.6615,139.66546;u=1100'
date: '2022-03-06T15:29:54+09:00'
---

Great to hang out with the guys and amazing to have Steve join us from Hiroshima!!

Shraddhan, Steve, Deneys, Mohamed, Adam, Shiori, Naomi (and her two children) all attended today.

Headed home now, the trains are weirdly behind schedule.  I'm on a local train that plans to terminate in ShinYurigaoka, which is great for me!

The next train is scheduled 15 minutes later, and will terminate in Mukogaokayuen.  I think they announced there was an accident near Machida Station.
