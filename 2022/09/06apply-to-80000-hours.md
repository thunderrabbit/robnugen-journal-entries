---
title: "Apply to 80000 Hours"
tags: [ "80000", "apply" ]
author: Rob Nugen
date: 2022-09-06T13:03:22+09:00
draft: false
---

(written 13:03 Tuesday 06 September 2022 JST)

I have spent a great portion of the past 4-5 years trying to start a
business while making the world a better place.  I'm fluent in
emotions as I've been volunteer-leading a peer counseling circle for 7
years for westerners in Japan. https://www.mkpjapan.org/

During all of that, I have been maintaining a PHP website (in-house
framework) I built with my employer starting in 2012.  I have overseen
upgrades of the website and server from Ubuntu 14 to Ubuntu 22, and
PHP 5.6 to PHP 8.1.  I have been using Paul M Jones' Modernizing
Legacy Applications In PHP to get our PHP code into a modern shape,
all while keeping the site running.

This experience sounds quite similar to your need for upgrading your
site.

I'm not quite as strong on APIs and Node.js, but I have written simple
scripts in Python and complex scripts in Javascript, even finishing
FreeCodeCamp's 300 hour Javascript course last year:
https://www.freecodecamp.org/certification/thunderrabbit/javascript-algorithms-and-data-structures
and their 300 hour frontend certification back in 2016:
https://freecodecamp.org/certification/thunderrabbit/legacy-front-end

I tend to be careful and methodical, making notes along the way so my
path can be followed by others.  I'm happy to join Kushal's team to
make your website transition work well.  I'm relatively familiar with
Wordpress and understand why you'd ultimately want to use something
else.

I communicate well even when situations might become emotionally challenging.  I have been active in ManKind Project peer-counseling
groups since 1997.

For full transparency, I had never heard of Reasoning Transparency
until I saw your job description, but I will look into taking this
course https://effectivethesis.org/reasoning-transparency/ as it seems
interesting.

I often think about how my choices and actions impact the world around
me, which ultimately ripple out into the community and the world.  I
often talk about "doing the right thing" and "doing things in the
right way," which for me means thoughtfully, deliberately, tapping
into my emotional body for insights from higher power.

Yes, higher power, a.k.a. God.  When synchronicities appear, such as
me finding 80,000 Hours, featuring a mission like "make your life have
meaning" and you need a website update almost exactly like what I've
done,  I think "hmm!"

Maybe I can be of service.  

Your mission is worth pursuing because we ALL have unique purpose in
our lives.  Having the greatest impact means tapping into our purpose,
which can sometimes be tricky for people to make a living.  We are
taught by TV to be cogs in the machine.  Hopefully 80,000 Hours
mission can help reach the tipping people so enough of us are living
our purpose that is just becomes natural to do so.   This would
effectively be an evolution of our society.

Do I want to be a part of that?  Definitely!
