---
title: "broke the shit out of Vagrant"
tags: [ "dang", "vagrant", "wow", "wtf" ]
author: Rob Nugen
date: 2022-09-12T15:12:59+09:00
draft: false
---

(written 15:12 Monday 12 September 2022 JST)

So weird..  After applying for Visual Alpha, I decided to start
playing with node.js and couldn't spin up a basic VM with
Vagrant. Hmmm.

I logged out of a Vagrant box that is right now running on my host,
but Vagrant app says it's not running.

Hmmm.

I wiped ALL the Vagrant volumes from my computer but still couldn't
spin up anything.

I uninstalled + reinstalled Vagrant but no luck.

I'm gonna reboot my machine now.

##### 16:42

Oh good.  Rebooting my machine, going for a walk, and watching Rick
and Morty S6:E2 fixed it.