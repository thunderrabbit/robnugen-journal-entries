---
title: "gratitude for other men leading group tonight"
tags: [ "mkp", "japan", "ribs" ]
author: Rob Nugen
date: 2022-09-13T17:06:36+09:00
draft: false
---

I just wrote this in the support channel of MKP Japan Slack space:

    Men
    
    I may not be able to attend all of the meeting tonight, so I'm
    grateful for Steve and other leaders stepping up to run the show.
    
    While in Nagano with her family, Lin cracked two ribs (2 nights
    ago).  Family were able to take her to hospital so all of the
    triage stuff is handled.
    
    They are planning to come home tonight and Lin has asked if I can
    go with her to help carry ママ's heavy stuff up to her apartment
    (15 minutes away).
    
    At the same time, ママ is embarrassed to ask for help, so may
    overrule the request for my support.
    
    In any case, I am proud of Lin for asking for help, and will make
    myself available if in fact they stop by here tonight before Lin
    takes her mom home.
    
    I have the support I need here now.  The greatest support is for
    you men to run the meeting tonight if I'm absent.
    
    love always,
    Rob
