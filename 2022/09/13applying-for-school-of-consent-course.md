---
title: "Applying for School of Consent course"
tags: [ "soc", "school of consent", "course", "betty martin" ]
author: Rob Nugen
date: 2022-09-13T07:13:22+09:00
draft: false
---

(written 07:13 Tuesday 13 September 2022 JST)

While I'm applying for jobs, also applying for a course.

https://www.schoolofconsent.org

Other trainings you have taken:

    Mankind Project (MKP) New Warrior Training Adventure 1997
    MKP Leadership Training  2005 ?
    MKP I-group Facilitator Training   2009 ?
    Suicide hotline training 2017
    ShadowWork Basic Facilitator Training 2019
    MKP Circle of Men  2021

What you hope to gain from this training

    I hope to understand my own desire for physical connection whether
    via hugs, massage, sex.  I hope to learn about setting boundaries
    and opening boundaries.  (In Japan, I see a lack of physical
    connection which may not be healthy.)  I have cried here from lack
    of hugs, and thrived here who do hug.  I want to learn how to
    create a safe space for people to explore contact.  I want to
    learn how to lead the 3 Minute Game.

Is there anything that would make this training more accessible to
you?

    Possibly a lower price, but hopefully I can pay full price soon.
    (details below)

    On May 11th 2022, I discovered I fit the profile of an compulsive
    underearner, as described in Underearners Anonymous symptoms:
    https://www.underearnersanonymous.org/about-ua/symptoms-of-underearning/
    I've been attending 12 step meetings daily since 11 May 2022, so I
    am in recovery.

    I've been working 1/8th full time for a few years while trying(?)
    to start my personal practice (confidential listener, personal
    growth coach, leading barefoot walking events).

    After joining UA, doing lots of prayer and meditation, I have
    started applying for more traditional jobs, just to start earning
    a higher salary to take pressure off my personal practice to
    perform well.

Anything else you want to add?

    I walk everywhere barefoot.  It's a personal choice for my own
    joy.  I have led barefoot walking events and barefoot walking
    hikes to encourage people to give their foot bones and musculature
    a chance to grow strong and healthy (invaluable for balance in
    later life!)