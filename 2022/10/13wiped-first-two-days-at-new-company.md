---
title: 'wiped first two days at new company'
author: 'Rob Nugen'
date: '2022-10-13T07:44:25+09:00'
---

"Stick it in Your Bitlocker"

Received a new Windows 11 laptop on Tuesday.

Activated Bitlocker to encrypt the drive at rest.

Got local dev environment set up by end of day Wednesday (yesterday).

Last night I started the process of installing Ubuntu alongside Windows 11.

Ubuntu installer detected I had encrypted the Windows drive, so it cannot be installed without decrypting the drive.

Hmm.  Nothing written in my copious notes about Bitlocker key so I tried to boot Windows so I could find the key.

Windows detected Ubuntu had been poking around, so before letting me unlock the drive, Windows wanted the very same key I was trying retrieve from Windows.

I tried to recover the key via Microsoft website but it said it didn't have a key uploaded to it for safe keeping.

So I wiped my drive (and 2 days of work) by installing Ubuntu on top of everything on the drive, thereby wiping Windows and any trace of the encrypted drive data.

I'm now in the process of redoing 2 days of installation work (hopefully faster this time)
