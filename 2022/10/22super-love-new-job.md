---
title: "Super love new job"
tags: [ "job", "yay", "love" ]
author: Rob Nugen
date: 2022-10-22T11:06:58+09:00
draft: false
---

I've really enjoyed my new workmates and working conditions in general
at my new company.

I don't have to wear shoes.

We don't have to wear masks.

We don't work overtime.  Really!

We do something called "code reviews" and "tests" on the code!

Other teammates are helpful and thoughtful.

Yayyyyy!

