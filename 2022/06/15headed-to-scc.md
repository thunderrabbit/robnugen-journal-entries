---
title: 'headed to SCC'
author: 'Rob Nugen'
date: '2022-06-15T12:15:47+09:00'
---

Headed to SCC.  Barefoot, of course.

Probably will have lessons with Erika, Kotone, and maybe Naoko.  Haven't seen Naoko in a while. Like over a month?

The lessons start at 1pm, so I like to get on the 12:15 p.m. from Noborito Station, where I will definitely get a seat because a local train and "everyone" prefers to take the express train and leaves at 12:13 from the opposite side of the same platform.

The train will arrive by 12:45 p.m. which gives me leisurely stroll to SCC upon arrival in Kawasaki.

Once I get to the building entrance I'll put on my socks and shoes and walk into the building.
