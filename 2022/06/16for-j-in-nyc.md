---
title: 'for J in NYC'
author: 'Rob Nugen'
location: 'geo:35.66944,139.68236;u=699.9990234375'
date: '2022-06-16T21:26:23+09:00'
---

[![2022 jun 16 train leaving yoyogi uehara station ](//b.robnugen.com/journal/2022/thumbs/2022_jun_16_train_leaving_yoyogi_uehara_station_.jpeg)](//b.robnugen.com/journal/2022/2022_jun_16_train_leaving_yoyogi_uehara_station_.jpeg)          

[![2022 jun 16 noborito station ](//b.robnugen.com/journal/2022/thumbs/2022_jun_16_noborito_station_.jpeg)](//b.robnugen.com/journal/2022/2022_jun_16_noborito_station_.jpeg)          

[![2022 jun 16 on train ](//b.robnugen.com/journal/2022/thumbs/2022_jun_16_on_train_.jpeg)](//b.robnugen.com/journal/2022/2022_jun_16_on_train_.jpeg)          

[![2022 jun 16 getting off the train in shinyurigaoka ](//b.robnugen.com/journal/2022/thumbs/2022_jun_16_getting_off_the_train_in_shinyurigaoka_.jpeg)](//b.robnugen.com/journal/2022/2022_jun_16_getting_off_the_train_in_shinyurigaoka_.jpeg)          
