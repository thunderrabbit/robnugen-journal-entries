---
title: 'State of My Life Address'
author: 'Rob Nugen'
location: 'geo:35.58843,139.62784;u=1000'
date: '2022-06-15T12:36:16+09:00'
---

1. I found God.  Not via Jesus or anyone specific, but my personal God with whom I enjoy giving high fives and high tens.
2. More specifically, I've been attending meetings of a 12-step program called UA. (Underearners Anonymous), and sharing lots of high tens with God.
3. I didn't even know Underearning was a "thing" ... Much less one from which I can recover in a systematic way.
4. I've been underearning and underbeing for a long time, and the UA steps have helped me find prosperity in my life.
5. ((If any of this speaks to you, I invite you to check out the symptoms of under earning to see if it may be something that can support you.
https://www.underearnersanonymous.org/about-ua/symptoms-of-underearning/))
6. As a result of attending UA recovery groups and doing steps, I've been expressing gratitude as often as possible, asking God for insight on how to act in any given moment.
7. For example, last night, before heading to my full moon barefoot meditation event, I prayed and expressed desire to feel joy and happiness during and after the event.
8.  Prayers answered!  One of the attendees brought hot tea and mooncakes to share.  This was an unexpected gift and the first time for me to even hear about (Chinese) moon cakes. 9. We enjoyed a great time in meditation under the full moon yesterday.  🌝  The sky was overcast and the rain very light, keeping us cool and connected to the natural cycles of earth 🌎, sun 🌞, moon 🌝 and life!
10. On Zoom just yesterday, I met Tina, a student of Cham, who co-founded Authentic Education. I look forward to discovering my niche and sharing it in a way that attracts my optimal clients.
11. So I'm going to get back to the video that she sent me to work through and keep working through it.
