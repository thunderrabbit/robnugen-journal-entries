---
title: 'synchronicity city'
author: 'Rob Nugen'
location: 'geo:35.69747,139.76217;u=1399.9990234375'
date: '2022-06-16T17:58:12+09:00'
---

Today has been loaded with synchronicities as I have been showering the universe with gratitudes and the universe has been showering me with insights and beautiful synchronicities.

* Had a great connection this morning with the UA group, chatting with M and K afterward
* Lovely time this morning putting the wrapping paper on my Mystery Box, which I will use at my speaking event on July 9th.
* Took a nice 62 minute nap and woke up naturally one minute before my alarm
* Seven people joined the barefoot walking event.
* Clare helped me write Japanese to my Japanese language barefoot community.
* Three of the members discovered they all live to the same neighborhood.
* They live near a nursing home and I may be able to approach them soon
* We met Hug Yoga Mari on the way to the cafe.
* We shared beautiful deep conversation, beautiful silence, and arrived at the station exactly on time for my train.
* Before leaving the station I remembered I need to go pee and there's a restroom right here and I was able to go poo as well in a quiet loo to write this entry.

Next I'll be heading to this event:

Checkout this Meetup with 東京ドネーション式レッスン：イギリスハーフの英語レッスン。Tokyo Donation Lessons: https://meetu.ps/e/L8PZV/TWnP/i
