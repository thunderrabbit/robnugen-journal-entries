---
title: "Colorized photo of me catching frisbee at UH"
tags: [ "colorized", "disc", "jump", "uh" ]
author: Rob Nugen
date: 2022-07-12T09:20:32+09:00
draft: false
---

(written 09:20 Tuesday 12 July 2022 JST)

<img
src="https://b.robnugen.com/journal/2022/rob_catching_disc_colorized.jpg"
alt="rob catching disc colorized"
class="title" />

Here is the original

[![030 catching
frisbee](//b.robnugen.com/rob/presentations/pechakucha_2016/thumbs/030_catching_frisbee.png)](//b.robnugen.com/rob/presentations/pechakucha_2016/030_catching_frisbee.png)

Super thanks to Ade for great work!

https://www.instagram.com/ade_23005/

https://imgur.com/user/Ade23005
