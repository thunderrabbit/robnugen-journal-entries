---
title: 'my answer to "are you enjoying Reddit?"'
author: 'Rob Nugen'
date: '2022-07-03T08:48:41+09:00'
---

You asked me if I'm enjoying Reddit and I must say no it's just an addiction that gives me quick shots of adrenaline and doesn't really do much good in my life realistically; however there are some times that I do rely on Reddit to give a quick response that's up to date written by a human so thank you for keeping the humanity in Reddit as much as possible and for parsing this run-on sentence.
