---
title: 'Gratitude after Mt Takao barefoot challenge'
author: 'Rob Nugen'
date: '2022-07-10T18:42:49+09:00'
---

Gratitude journal

[![2022 jul 10 thank you god ](//b.robnugen.com/journal/2022/thumbs/2022_jul_10_thank_you_god_.jpeg)](//b.robnugen.com/journal/2022/2022_jul_10_thank_you_god_.jpeg)         

Dearest God in All Things --

Thank You so much for such a blessed amazing experience - so much gratitude and joy 😊

I feel like a millionaire!  Thank You as beautiful community coming together to joyfully support each other and experience beautiful barefoot community and time and nature, all You!

I happily say I'm overflowing with joy and cash and abundance and happy gratitude.  Thank You God.  So Much!

Thank You for blessing me safe and loving while I share Your infinite Abundance!

Please help guide me in Your Light, helping me know Your Will.
 Thank You God for perfecting my nature.  May I shine Your Light and Love and Grace to the worlds around me.  

Please continue to guide me moment by moment with Your Grand Design.  May I rest forever in Your Service.

Barefoot Rob!



