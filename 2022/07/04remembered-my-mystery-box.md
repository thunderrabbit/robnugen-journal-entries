---
title: 'remembered my Mystery Box'
author: 'Rob Nugen'
location: 'geo:35.68383,139.70247;u=699.9990234375'
date: '2022-07-04T12:36:03+09:00'
---

Wowwwww I'm so so so grateful for all the things that worked well for me to recover my Mystery Box from an Odakyu Line train.

1. Put Mystery Box up on the overhead rack in ShinYurigaoka
2. Go to Shinjuku, reading Hacker News articles
3. Arrive in Shinjuku with lots of time available
4. Wait for escalator crowd to clear out.
5. Casually walk to Yamanote Line
6. Line up on platform for Yamanote Line
7. Think about plans for today
8. Oh shit the Mystery Box!!!
9. Dart up the stairs to ticket gate level
10. *Run* through the ticket gates to Odakyu Line
11. Zip down the stairs to train level.
12. Check the couple of cars on about to leave train (no box)
13. Check a couple of cars on the next to leave train
14. Find the Mystery Box!  Yayyyy
18. Stroll calmly and happily up the steps.
15. The guy at ticket gate reset my Pasmo so I could *walk* through the gates
16. Put the Mystery Box next to my bag while I write this on Yamanote Line so I will remember it.
17. Thank you Future Rob for continuing to remember the Mystery Box!

