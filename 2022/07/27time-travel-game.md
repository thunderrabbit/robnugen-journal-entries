---
title: "Time Travel Game"
tags: [ "game", "time travel", "screenshot" ]
author: Rob Nugen
date: 2022-07-27T11:44:56+09:00
draft: false
---

<img
src="https://b.robnugen.com/journal/2022/2022_jul_27_time_travel_level_1003.png"
alt="2022 jul 27 time travel level 1003"
class="title" />

Based on level 1000, I made this level of a time travel game.

https://time-travel-game.lamdera.app/level/1003

The game was created by AT on https://ascii-collab.app/?x=993&y=362
