---
title: "Overpaying on Dreamhost"
tags: [ "dh", "huh", "money", "overpay" ]
author: Rob Nugen
date: 2022-07-03T05:56:07+09:00
draft: false
---

(written 05:56 Sunday 03 July 2022 JST)

Hmm!  Interesting that I have just discovered I've been paying
Dreamhost for a private MySQL server since January 2020.

$58 / month.   Hmmm.

January 2020 - December 2020 = 12 months
January 2021 - December 2021 = 12 months
January 2022 - July 2022 = 7 months

So, hopefully I can get (12 + 12 + 7) * 58 = ($1798) credit, or even a
check, but for simplicity, I prefer the credit.

##### 09:17 Tuesday 11 July 2022 JST

They gave me $864 credit.