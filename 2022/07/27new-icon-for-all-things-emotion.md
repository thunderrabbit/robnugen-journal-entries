---
title: "New icon for All Things Emotion"
tags: [ "meetup", "icon", "all things emotion" ]
author: Rob Nugen
date: 2022-07-27T13:27:46+09:00
draft: false
---

About a month ago, Miki created All Things Emotion.  She did one event
in Yoyogi, which I attended and enjoyed.  She chose not to do any more
events, so abandoned the group.  I figured I would take it over.

Working with Shane, I am putting some thought into the branding.

Working with Jimmy, I have a couple of icons.

<img
src="https://b.robnugen.com/journal/2022/hug_2022_july_27_jimmy.png"
alt="hug 2022 july 27 jimmy"
class="title" />

