---
title: "Apply to Colorkrew"
tags: [ "colorkrew", "work" ]
author: Rob Nugen
date: 2022-08-24T16:35:33+09:00
draft: false
---

(written 16:35 Wednesday 24 August 2022 JST)

Today I found a company called Colorkrew which shares a lot of my
values.  I'm writing to them via their application/internship page

I'm fluent in computer languages and human emotions.  All my adult life I have been conversant with both.  My dad introduced me to BASIC programming on a VIC 20 in 1981 and I started learning counseling skills from my mom and my church youth group around the same time.

Since graduation from University of Houston in 1995 (Computer Science), I focused professionally in programming, including C++, MySQL, PHP, Java, Objective-C, SAS, and other languages at various points in my career.

Since 1997 I have also been involved with peer counseling groups, where people can share honestly about personal topics in a confidential setting.

I'm also an expert barefoot walker, having walked barefoot everywhere for 21 years and counting.

Fluent in emotions, programming, and barefoot walking, I moved to Japan in 2003 in order to see a different part of the world.

After marrying my Japanese girlfriend in 2010, I realized marriage is quite challenging, especially a mixed culture marriage.  I wanted a place to talk about the challenges of marriage so I expanded the peer counseling group, which continues to this day https://www.mkpjapan.org/

Seeking more counseling skills, I also was trained as a lifeline volunteer for suicide hotline.

I was 裸足のYOU on YOUは何し日本へ in 2016.  https://www.youtube.com/watch?v=Vvs3sKYYh5k

I spoke at TEDxOgikubo last year https://www.youtube.com/watch?v=LJzb5n9ldP8 

Though my Japanese language skills are not enough to do counseling in Japanese, I want to facilitate more opportunities for using technology to increase connection and reduce loneliness in society, especially with the pandemic still affecting people around the world.

I know well the isolation one can feel as a gaijin in Japan (Tokyo), and I know there are many opportunities to connect deeply.  I lead barefoot walking events and hiking events which encourage people to connect with each other and the present moment.  I also regularly support discussions in other Meetup groups.

The Japanese concept of いきがい cannot be fully captured in English, but it's perfect for what I mean: having a vision or passion for creativity and life; using our passion to bring about a better world for ourselves and others.

My vision is to reduce the epidemic of loneliness in Japan.  Barefoot walking events have been a great intercultural, intergenerational activity to bring people together with a simple positive experience of earthing (walking barefoot).

Reading your website, I'm excited about the Colorkrew vision and what we can create together.  I love that you encourage us to learn and expand our skills and vision.

I'd like to propose a new type of currency or unit of value in the internal office Mamoru-Biz app: Quality of Life points, which can be created as we improve relationships with one another.  These could be based on Gary Chapman's Five Love Languages, a simple way to understand how to support one another in a preferential way.

In any case, I can of course work with your existing projects.  I'm well-versed in PHP, MySQL, HTML, and various Javascript frameworks.  I use git, BASH, Perl, Ansible, Vagrant, Emacs, Atom, and sometimes vi.  

Most recently, I have been maintaining a website (PHP + MySQL) I created with my employer in 2012.  I have been working part time at upgrading the server on AWS (EC2) from Ubuntu 14 to Ubuntu 22, all while keeping the site running including up to date versions of PHP. 

Let's see what we can do together.  (^_^)V

   blessings
   - Rob


