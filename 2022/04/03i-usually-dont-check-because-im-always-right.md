---
title: '"I usually don''t check because I''m always right."'
location: 'geo:35.70515,139.73056;u=300'
date: '2022-04-03T19:57:27+09:00'
aliases: [
    "/journal/2022/04/03/i-usually-dont-check-because-im-always-right/",
    "/journal/2022/04/03/i-usually-dont-check-because-im-always-right./"
]
---

　　ー Hiro, age 13, regarding scrambling Rubick's Cube according to notation



