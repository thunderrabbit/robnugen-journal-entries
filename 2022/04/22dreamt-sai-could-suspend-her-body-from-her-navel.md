---
title: "Dreamt Sai could suspend her body from her navel"
tags: [ "dream", "sai", "navel" ]
author: Rob Nugen
date: 2022-04-22T16:20:02+09:00
---

##### 16:20 Friday 22 April 2022 JST

<p class="dream">
I told Sai I heard she would go on a trip.  She showed me part of the
training she had to do: sit upright with a stick holding her from her
navel.  Basically she had abs as hard as steel to support her body
weight that way.
</p>

<p class="dream">
Her doctor came by to show he could hold himself up with his eyelid.
</p>
