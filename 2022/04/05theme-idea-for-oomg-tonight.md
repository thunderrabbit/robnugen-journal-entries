---
title: "Theme idea for OOMG tonight"
tags: [ "mkp", "japan", "oomg" ]
author: Rob Nugen
date: 2022-04-05T18:26:29+09:00
draft: false
---

(written 18:26 Tuesday 05 April 2022 JST)

Theme idea: passions

1. What did you generally like to do as a child?
2. What was one big passion you had as a child?
3. What passions do you have now?
4. How do your passions affect those around you?
5. What have you learned about your passions today?
6. How do you want to move forward regarding your passions?
