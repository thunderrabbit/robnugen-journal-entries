---
title: 'super glue on laptop'
author: 'Rob Nugen'
date: '2022-04-07T15:08:13+09:00'
---


Oh man I just sprayed super glue by squeezing the bottle way too hard.

I was trying to fix the scissors ✂️ handle I broke.

[![2022 apr 07 super glue on laptop ](//b.robnugen.com/journal/2022/thumbs/2022_apr_07_super_glue_on_laptop_.jpeg)](//b.robnugen.com/journal/2022/2022_apr_07_super_glue_on_laptop_.jpeg)
[![2022 apr 07 dsc 3227](//b.robnugen.com/journal/2022/thumbs/2022_apr_07_dsc_3227.jpeg)](//b.robnugen.com/journal/2022/2022_apr_07_dsc_3227.jpeg)          
