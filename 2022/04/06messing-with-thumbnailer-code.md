---
title: "Messing with thumbnailer code"
tags: [ "code", "perl", "thumbs", "images" ]
author: Rob Nugen
date: 2022-04-06T09:42:44+09:00
draft: false
---

When I uploaded a vertically oriented jpg via my laptop, it showed up
sideways on my site.

And just now while testing with my upjournal.sh it showed up
vertically aligned.

[![2022 apr 05 gchoppy curving tssfig](//b.robnugen.com/journal/2022/thumbs/2022_apr_05_gchoppy_curving_tssfig.jpg)](//b.robnugen.com/journal/2022/2022_apr_05_gchoppy_curving_tssfig.jpg)

Dang; I need to do from scratch with new photo.

[![some code](//b.robnugen.com/journal/2022/thumbs/some_code.png)](//b.robnugen.com/journal/2022/some_code.png)