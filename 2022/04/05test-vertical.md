---
title: 'test vertical'
author: 'Rob Nugen'
date: '2022-04-05T17:13:49+09:00'
---

[![2022 apr 05 doctor sugar holding zog rails support ](//b.robnugen.com/journal/2022/thumbs/2022_apr_05_doctor_sugar_holding_zog_rails_support_.jpeg)](//b.robnugen.com/journal/2022/2022_apr_05_doctor_sugar_holding_zog_rails_support_.jpeg)

I'm testing it here on my phone because I thought it was already set
up to do this, but I can't find the changes in the get commit messages
so maybe I didn't do it. I'm kind of confused here..

So hopefully the image above shows upright.  The one I uploaded via
`~/mt3.com/upparts.sh` definitely did *not*.  Here it is for posterity.

[![2022 apr 05 doctor sugar holding zog rails support](//b.robnugen.com/art/marble_track_3/track/parts/2022/thumbs/2022_apr_05_doctor_sugar_holding_zog_rails_support.jpg)](//b.robnugen.com/art/marble_track_3/track/parts/2022/2022_apr_05_doctor_sugar_holding_zog_rails_support.jpg)

Page 62 of my journal BOOK TWENTY-ONE is where I wrote about it to
track my progress.

##### 17:24

Okay, testing locally, I see the thumbnail is rotated properly, so we
are quite close.  Click the second thumb above now and it shows up
sideways.
