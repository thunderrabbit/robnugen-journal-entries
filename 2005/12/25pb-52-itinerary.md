---
title: "PB 52 itinerary"
tags: [ "pb", "itinerary" ]
author: Rob Nugen
date: 2005-12-25T17:26:45+09:00
draft: false
---

(compiled 17:26 Tuesday 17 September 2024 JST)

<div class="note">Thank you, https://web.archive.org/</div>

1. 2005/12/26: Yokohama, Japan
2. 2005/12/27: Kobe, Japan
3. 2006/01/02: Danang, Vietnam
4. 2006/01/06: Singapore
5. 2006/01/15: Port Victoria, Seychelles
6. 2006/01/18: Mombasa, Kenya
7. 2006/01/26: Cape Town, South Africa
8. 2006/01/29: Walvis Bay, Namibia
9. 2006/02/08: Rio de Janeiro, Brazil
10. 2006/02/12: Buenos Aires, Argentina
11. 2006/02/18: Ushuaia, Argentina
12. 2006/02/24: Valparaíso, Chile
13. 2006/03/2: Rapa Nui, Chile
14. 2006/03/10: Papeete, Tahiti
15. 2006/03/18: Lautoka, Fiji
16. 2006/03/23: Rabaul, Papua New Guinea
17. 2006/03/30: Yokohama, Japan
18. 2006/03/31: Kobe, Japan
