---
title: "freewriting poetry in Vietnam"
tags: [ "poetry", "transcribed", "viet nam" ]
author: Rob Nugen
date: 2005-05-30T14:22:57+09:00
draft: false
---

(transcribed 14:22 Friday 23 February 2024 JST)

どぞ my freewriting Vietnam poetry workshop おつかれオドリーさまちまは
Sugoi Suばらしい！ If I go anywhere it will be Vietnam The people are
as friendly as friends and money is easy as pie and all smiles all
around. My sandals are as big as my feet cuase they are ジャストセイズ [sic]
and made just for my footses which are also just my size.  And in this
workshop with roof flapping in the lovely breeze seawind wafting all
around including seaward and forward そのかんじはピンポン awesome
without borders to constrain air molecules from exerciesing their free
will to sing freely to the music at the speakers' thumping command
powerful enough to move my hair during an outdoor rock concert put on
by SPB at UH for the sneezing people without persmission nor cause
except just to do it and again with Vietnam in the background out of
sight but never out of mind, my friend Tam's face smiling in my mind
as she gets me the best prices in town in Hoi An and tells me that
she'll remember me for 1,3,5 years if and when I return to climb
marble mountain and I hope I write her a postcard; I already plan to
send this to janette whose personality shines brightly in her American
village in Dallas, a mere suburb of the world with people driving fast
cars and fasting not on fast food and watching stuffing cramming movies
& trivia into their trivialized minds all because they don't know what
going on in the rest of the world nor Vietnam but we onboard are
fortunate to have this travel opportunity to stop for a moment and
write nothing - just as it flows from my pen onto the page and my ねこ
to be an 女大 and not a cat on a brat but to have fun onboard though I
think she was mad at me when I saw her this AM after VN - was it cause
I was walking next to Kim?  Well if so I just want you to know that I
will always be one who loves to love.
