---
title: "PB 49 itinerary"
tags: [ "pb", "list" ]
author: Rob Nugen
date: 2005-05-09T16:18:37+09:00
draft: false
---

(compiled 16:18 Tuesday 17 September 2024 JST)

2005:

1. May 21: Yokohama
2. May 22: Kobe
3. May 26: Hong Kong
4. May 28-29: Danang (Vietnam)
5. June 1: Singapore
6. June 6: Colombo (Sri Lanka)
7. June 15-16: Aqaba (Jordan)
8. June 18: Suez Canal
9. June 19: Port Said (Egypt)
10. June 21: Piraeus (Greece)
11. June 23: Catania (Italy)
12. June 26: Barcelona (Spain)
13. July 1-2: Le Havre (France)
14. July 5: Bergen (Norway)
15. July 9-10: Dublin (Ireland)
16. July 18-20: New York (USA)
17. July 25: Montego Bay (Jamaica)
18. July 27-28: Cristobal/Balboa (Panama)
19. July 30: Puntarenas (Costa Rica)
20. August 3-4: Acapulco (Mexico)
21. August 12-13: Vancouver (Canada)
22. August 19: Seward (Alaska)
23. August 30: Yokohama (finally back home, wait what??)
