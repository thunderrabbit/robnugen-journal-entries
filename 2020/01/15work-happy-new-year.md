---
title: "Work Happy new year"
tags: [ "work" ]
author: Rob Nugen
date: 2020-01-15T16:13:01+09:00
draft: false
---

(transcribed 16:13 Tuesday 10 August 2021 JST)

Tatsuya went to Chiba for 4 days.  Kyoko studied English because she will
take TOEIC this weekend on the 18th.

Chinatsu got flu for Christmas and had to work on the 30th.  Went
skiing in Niigata on 4 Jan.

I went to Naoshima for New Years then to Japanese Folk House.

Kyoko will practice piano daily (at night maybe). 4 or 5 nights per
week 30 mins.

Chinatsu will practice piano every night (7 nights/week) 30 minutes.

Tatsuya will watch movies 3 or 4 days per week 1 title / night.

Rob will post, host, write, or invite re: workshop every day.

5 minute rant about daily practice.
