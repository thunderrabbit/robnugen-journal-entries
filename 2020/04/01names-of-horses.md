---
title: "Names of horses"
tags: [ "horses", "chinatsu" ]
author: Rob Nugen
date: 2020-04-01T06:24:09+09:00
draft: false
---

Chinatsu has been riding horses.  In order of decreasingly easy to
ride: Shiny, Minnie, Cafe Ole, Charlie, Maggie.

Lessons will be moved online due to COVID.
