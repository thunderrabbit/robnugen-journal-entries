---
title: "State of My Life Address"
tags: [ "soml", "adaptation", "notes", "one"  ]
author: Rob Nugen
date: 2020-04-01T09:10:42+09:00
draft: false
---

(09:11 Monday 18 September 2023 JST)

Adapted from mini book ONE:

01. Working on AB
02. Using SB-hosted Redmine to track hours 
03. Working on ConSwi
04. Lessons with Yuna (every other Saturday?)
05. Taking to Celine about Jason Winter tea EOS (MLM)
06. doing Your Art Matters (YAM)
07. Lin will buy a car soon
08. COVID is a thing?
09. JB lessons at Open Tone, Toshiba, Shukoh
10. Studying Japanese via KUMON
11. Doing MT3
12. Talking to Mohamed
13. 8 peeps at recent barefoot walking meditation
14. I've been talking to Robert Golden 
15. I started creating a soak-through art piece using 100 pages of A4 newsprint paper
16. Lin and I doing well
17. Trying to do Bold Life Brotherhood
18. I got to play with a big magnet at Toshiba because no visitors
