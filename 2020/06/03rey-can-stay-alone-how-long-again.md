---
title: "Rey can stay alone how long again"
tags: [ "rey", "stay", "data" ]
author: Rob Nugen
date: 2020-06-03T06:31:56+09:00
draft: false
---

Rey is a dog Chinatsu got from a shelter.  He panics when left alone.
They are planning to train him to stay alone calmly.  Now he can stay
alone 30 seconds.

* 2020 Jun 03: 30 seconds
* 2020 Jun 09: 2 minutes
* 2020 Jun 10: 6 minutes
* 2020 Jun 17: 10 minutes
* 2020 Jun 24: 30 minutes
* 2020 Jul 15: 90 minutes
* 2020 Jul 22: 90 minutes
* 2020 Jul 29: 2 hours
* 2020 Aug 19: 2 hours
* 2020 Aug 26: 2 hours
* 2020 Oct 14: 3+ hours
* 2020 Nov 11: 4 hours
* 2021 Jan 13: 5 hours
* 2021 Feb 10: 6+ hours
* 2021 Mar 02: 7 hours
* 2021 Apr 07: 9 hours

