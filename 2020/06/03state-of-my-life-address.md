---
title: "State of My Life Address"
tags: [ "soml", "tired", "icantbreathe", "georgefloyd" ]
author: Rob Nugen
date: 2020-06-03T23:56:14+09:00
draft: false
---

(started 23:56 Wednesday 03 June 2020 JST)

1. Very curious to me that I have been writing *less* in my journal
   while ostensibly having more time to do whatever I want during this
   COVID stuff.
2. After we did lessons online for 5 of the past 8 weeks, I just went
   back to JB School today for 4 lessons today.  I had the option of
   sitting behind a clear sheet hanging from the ceiling, but I felt
   claustrophobic behind it and just opted to wear a mask.
3. I seem to have gotten a scratch on my eye from fiber from my
   mask. Very annoying.
4. I am planning to walk to Niigata.  Now that I have found a way
   across the mountains and found **Dragondola**, I am getting super
   excited about the walk.
5. I wrote an entry for George Floyd

##### 17:56 Monday 08 June 2020 JST

6. After writing the entry, I saw some interesting evidence that the
   murder may have been faked, and lots of rioting has been amplified
   by paid shills and free bricks delivered to peaceful demonstration
   locations.
7. I have been visiting "my" tree semi regularly as a place to chill.
   Today I even did some planning for ConSwi scoring system.  Last
   time Tariq, Dipendra, and I met, I came up with a new way to
   calculate the scores.
8. In Bold Life Brotherhood, we are now in the month of June, focusing
   on RIGOR.
9. For AB, I have gotten rid of at least 3 singletons, and am down to
   2 remaining in the codebase.  I have not yet figured out how to
   convert `SiteHandler`, but `ErrorHandler` will be converted to
   non-singleton soon.  I actually did not notice `ErrorHandler` was a
   singleton because I had been searching for `extends
   \Pattern\Singleton` but `ErrorHandler` came from a different source
   and has its singleton aspects right in the class definition.
10. Mai has been hanging out with us occassionally since COVID; it has
    been great to have a friend living close by.  Richard moved first
    back to his home country and now they are waiting for Mai's visa
    to be approved.
11. While walking in the mornings, I found a neat house at the top of
    a hill which uses the shape of the hill to give a window to an
    otherwise underground room.

##### 20:21 Tuesday 16 June 2020 JST

12. I have been walking in the mornings recently, ostensibly preparing
    for my planned walk to Niigata in April 2021.  I can walk for an
    hour before I really notice any tiredness.  I think I could walk
    an hour four or five times in a day (with breaks) and maintain my
    planned schedule.
13. Marble Track 3 is going well.  I have been spending 2 hours per
	week livestreaming the animation of its construction.  Today I
	actually did 1.4 hours, so I want to make up the rest by the end
	of the week.
14. I just recently started the idea of making <del>a</del> marble
    track<ins>s</ins> out of wire, after watching a video explaining
    how to do it with simple tools.  I was like "wow!" and "I can do
    that!" while hoping I can actually do it.  I bought some stainless
    steel wire from a hardware shop on the way to JB.  Today I bought
    some piano wire from a shop I found by following inspiration at
    each turn.

##### 18:54pm Saturday 14 October 2023

2023 above is not a typo.  These are notes I put together based on some old notes from BOOK ONE (small notebook, not a journal)

SOML 2020 June

* Lin and I live nearish Shin Yurigaoka with Jennie
* Duo Lingo and Kumon で日本語を勉強します
* design your day with Shraddhan
* BLB Bold Life Brotherhood
* Godot ConSwi
* Freya re coaching
* Kitsiri story plan
* Yuna
* walk with Lin and Mai
* EOS
* TY
* climbing trees
* Preparing for my walk across Honshu
* MT3
* Akihabara
* Mo
* LCLA Life Changing Life Adventures
* YAM Your Art Matters
* Yuri
* Erik re website
* Sloyer: what does 1000 times bigger look like?
* me:
* * walking barefoot from City to City meeting presenting leading sharing
* * Wikipedia pages for me and my creations
* * Inspiring others into bold action, shedding shackles and bringing peace via heart connection
* * my works translated into languages I don't know
