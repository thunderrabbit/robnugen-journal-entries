---
title: "Second day at Kirra beach"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-16T09:28:00+09:00
draft: false
---

Welcome to Kirra beach! 🏖️

[![2025 jan 16 beach glow 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_jan_16_beach_glow.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_jan_16_beach_glow_1000.jpeg)

Jess meditating

[![2025 jan 16 beach perspective 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_jan_16_beach_perspective.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_jan_16_beach_perspective_1000.jpeg)

Happily buried myself deeply enough it was hard to get out 😂

[![2025 jan 16 half man half monkey 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_jan_16_half_man_half_monkey.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_jan_16_half_man_half_monkey_1000.jpeg)

Thank you Kirra for your lovely sandy beach!

