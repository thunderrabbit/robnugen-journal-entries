---
title: "Kirra beach little sandy balls"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-16T07:24:00+09:00
draft: false
---

We saw loads of little balls of sand on Kirra Beach near Gold Coast Airport 

[![2025 jan 16 little sandy balls 1000](//b.robnugen.com/journal/2025/thumbs/2025_jan_16_little_sandy_balls.jpeg)](//b.robnugen.com/journal/2025/2025_jan_16_little_sandy_balls_1000.jpeg)
[![2025 jan 16 loads of little sandy balls 1000](//b.robnugen.com/journal/2025/thumbs/2025_jan_16_loads_of_little_sandy_balls.jpeg)](//b.robnugen.com/journal/2025/2025_jan_16_loads_of_little_sandy_balls_1000.jpeg)