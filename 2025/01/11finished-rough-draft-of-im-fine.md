---
title: "Finished rough draft of I'M FINE"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-11T14:29:00+09:00
draft: false
---

I'm *finally* done with a draft of *I'M FINE*!

Standing outside to enjoy sun now as a reward 😄 

[![2025 jan 11 standing in the sun 1000](//b.robnugen.com/journal/2025/thumbs/2025_jan_11_standing_in_the_sun.jpeg)](//b.robnugen.com/journal/2025/2025_jan_11_standing_in_the_sun_1000.jpeg)