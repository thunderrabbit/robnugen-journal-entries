---
title: "Highlights of The Root ceremony (cleaned up by ChatGPT)"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-20T06:10:00+09:00
draft: false
---

Here's the cleaned-up version with minimal edits, thanks to https://chatgpt.com/g/g-6781a6dec0a48191aaec9b8b243930c6-transcript-tuner/ :  

---

The experience at The Root ceremony was wild and varied, with highs and lows!  

Jess and several others had great results, including visions of dark past lives that they karmically cleared—basically by purging and performing ho'oponopono toward those they had wronged.  

I thought it would be a good idea to take a double dose, but I just ended up with a tummy ache and was still incoherent during the first night's closing ceremony (held around 7 a.m. the next morning).  

Due to my incoherence, when my next-door mattress neighbor gave me a bell to ring during the closing ceremony, I couldn't figure out how to ring it. For everyone else, it rang, but not for me! So confused! I looked inside and realized there wasn’t even a clapper inside it.  

My other next-door mattress neighbor pointed toward my candle and said, "It's to put out the candle."  

Ohhhh! So I extinguished the candle with the bell-shaped snuffer, getting molten wax on it in the process.  

For some reason, I shook the snuffer again, dripping wax on the floor.  

Due to my generally playful behavior, Jess (from across the room) thought I was mucking around and breaking the flow of the ceremony. She said, "Rob, stoppit!"  

This hit me like an emotional spear, and I burst into gasping, sobbing tears.  

The male facilitator said, "Jess, now, he's incoherent," and his wife, the co-facilitator, immediately came to my side to hold space for me.  

Alaya comforted me, saying, "Just cry, you're okay, let it out. Very good, yes, you're okay, just cry it out," and she held space beautifully.  

Christof came over as well to hold space for me.  

I said, "It will take me some time; you can continue," but they were like, "No, we are all here for you. Take your time."  

And I cried even more, thinking, *Omg, do I deserve such support?!*  

Alaya went over to Jess and explained what had happened from an energetic and emotional standpoint. Jess came over and apologized, saying she had no idea and was so sorry.  

That was after the first ceremony, entitled "Death."  

The next night (the third night of the retreat), the ceremony was called "Rebirth."  

I consumed far less of the plant medicine—by my calculation, 1/48th of my previous night's dosage.  

I didn’t have any "visions" on either night, but I was relatively coherent on the rebirth morning—enough to do lots of happy, wild dancing...  

During which I stubbed my left middle finger quite badly. It still hurts in the middle knuckle, but at least the bruising has subsided.  

I lay down on my mattress until my dizziness wore off, carefully checked that it wasn’t broken, and then got back to dancing again—amidst cheers from the group (none of whom knew I had stubbed my finger).  

Christof invited us to stop dancing and stand shoulder-to-shoulder in a circle. I was between Babak, the muscular doctor, and one of the co-facilitators (whose name I don’t recall at the moment, except that it was not Clayton).  

Due to suddenly stopping dancing—after bruising the lights out of my finger—I felt myself passing out. I didn’t say anything, just started to sit down. Babak apparently caught me, but I was already unconscious.  

I woke up maybe 10 seconds later with Christof crouched down. I might as well have been in deeeeeep sleep all night because I woke up so confused, saying, "What happened??" And then realizing, "Oh yeah, I passed out."  

They had me lie down on my mattress for safety.  

I slept until breakfast, and many people checked in on me to see how I was doing. I basically felt loved by the group and humbled by my mind and body.  

And those are the highlights of the retreat! Jess will definitely attend again at the next chance she gets. I’m 50/50. I definitely won’t try taking a double dose on the first night—or ever again!  

