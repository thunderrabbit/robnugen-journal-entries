---
title: "great final day of my ultimate winter season "
tags: [ "2025", "Mike", "sogo", "ultimate", "chilly" ]
author: Rob Nugen
date: 2025-01-12T16:38:00+09:00
draft: false
---

[![2025 jan 12 yoyogi ultimate group 1000](//b.robnugen.com/journal/2025/thumbs/2025_jan_12_yoyogi_ultimate_group.jpeg)](//b.robnugen.com/journal/2025/2025_jan_12_yoyogi_ultimate_group_1000.jpeg)

Today we had a great time in Yoyogi Park playing ultimate 🥏, where we had six on six players for most of the time. 

Orange team basically dominated Banana team, but it was still good game and not a total wipeout.

I had a few perfect throws into the end zone for points with Beau, Mike from New York, and Mei Shen.  Great way to finish out my season!