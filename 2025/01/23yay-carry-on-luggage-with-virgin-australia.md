---
title: "Yay carry-on luggage with Virgin Australia"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-23T14:00:00+09:00
draft: false
---

Jetstar "starter" fare doesn't include an extra carry-on, so I had to pay extra to fly from Cairns to Gold Coast last week.

Today I went to Jetstar kiosk to check in but couldn't find my flight.

Turns out I'm flying with Virgin.  And oh wow they allow my extra bag to fly free!
