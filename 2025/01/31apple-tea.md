---
title: "Apple tea"
tags: [ "2025", "tff" ]
author: Rob Nugen
date: 2025-01-31T08:58:00+09:00
draft: false
---

"Um, excuse me; I just wanted to double check if this is apple tea, because it tastes like green tea."

"Oh, it's not apple tea, it's Apple Tea brand green tea."

🫨