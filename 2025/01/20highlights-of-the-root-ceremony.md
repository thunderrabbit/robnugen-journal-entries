---
title: "Highlights of The Root ceremony"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-20T06:03:00+09:00
draft: false
---

Based on text messages sent to Soness 8 February 2025

The experience at The Root ceremony was vary wild.  

Oh, I mean, wild and varied with highs and lows!

Jess and several others got great results, including visions of dark past lives that they karmically cleared (basically by purging and performing ho'oponopono toward those they had wronged)

I thought it would be a good idea to take a double dose, but I just had a tummy ache and was still incoherent during the first night's closing ceremony (held around 7am the next morning).

Due to my incoherence, when my next door mattress person gave me a bell to ring during the closing ceremony, I couldn't figure out how to ring it.  

For everyone else, it rang, but not for me!!  So confused!  I looked inside the bell, and found there wasn't even a bell clapper inside it!

My other next door mattress person pointed toward my candle in front of me, "it's to put out the candle"

Ohhhh!  So i extinguished the candle with bell shaped candle snuffer, getting molten wax on it in the process.

Not sure why, but I shook the snuffer again which dripped wax on the floor.

Due to my generally playful behavior, Jess (from across the room) thought I was mucking around (and breaking the flow of the ceremony).  

She goes "Rob, stoppit!" 

This hit me like an emotional spear and burst me into gasping sobbing tears.

Christof, the male facilitator said "Jess, now, he's incoherent" and his wife + co-facilitator Alaya immediately came to my side to hold space for me.

Alaya comforted me like "just cry, you're okay, let it out, very good yes you're okay, just cry it out" and held space beautifully for me.

Christof came over as well to hold space for me.

I said, "it will take me some time; you can continue," but they were like, "no; we are all here for you, take your time"

And I cried even more, thinking *omg do I deserve such support??!*

Alaya went over to Jess and explained what had happened from an energetic and emotional standpoint.   Jess came over and apologized to me, saying she had no idea and was so sorry etc.

In that moment, was still barely coherent so could hardly even look at her, much less "accept" her apology.

That was after the first ceremony, entitled "death"

The next night (the third night of the retreat), the ceremony was entitled "rebirth"

I consumed far less of the plant medicine (by my calculation, 1/48th of my previous night dosage).  

I didn't have any "visions" on either night, but I was relatively coherent on the rebirth morning.   Enough to do lots of happy wild dancing....   
during which I stubbed my left middle finger quite badly (enough that it still hurts in the middle knuckle, but at least the bruising has subsided.)

I lay down on my mattress until my finger-stubbing dizziness wore off, carefully checked that it wasn't broken, and then got back to dancing again amidst cheers from the group (none of whom knew I stubbed my finger).

Christof invited us to stop dancing and stand shoulder-to-shoulder in a circle.   I was between Babak the muscular doctor and one of the co-facilitators (whose name I don't recall at the moment except it was not Clayton).

Due to suddenly stopping dancing (after bruising the lights out of my finger) I could feel myself passing out; I didn't say anything but just started to sit down.  Babak apparently caught me, but I was already unconscious.

I woke up maybe 10 seconds later with Christof crouched down beside me.  I might as well have been in deeeeeep sleep all night because I woke up so confused saying "what happened??"  And then realizing "oh yeah I passed out"

They had me lay down on my mattress for safety.

I slept until breakfast and many people checked in on me to see how I was doing.  I basically felt loved by the group and humbled by my mind+body. 

And those are highlights of the retreat!   

Jess will definitely attend again at the next chance she can with them.  I'm like 50/50.  I definitely won't try to take a double dose on the first night (or ever again)!
