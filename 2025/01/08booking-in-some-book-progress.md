---
title: "booking in some book progress"
tags: [ "2025", "recently" ]
author: Rob Nugen
date: 2025-01-08T17:30:00+09:00
draft: false
---

Not yet including today, I have had pretty good progress recently writing my book.  This morning I had a call with my book writing accountability buddy. Karen who's writing the book titled something like *Hey Lady, You Are Gorgeous*

Today I got to hang out with Nathan for a little while and had perfect timing getting him back to the station so he could catch his correct train.

Tomorrow I'm going to hang out with Yuki for a while after she said she wanted to say bye to me before I head to OZ for a few months.

I just took a little nap and then got woken up by an alert and couldn't go back to sleep. So I'm writing this entry and then I'll get back to work on my book 📖.

Friday I have a dental appointment, then annual checkup, then nap or book, then visit David in Roppongi at 7pm.