---
title: "Arrived in Cairns"
tags: [ "2025", "oz", "cairns", "summer", "happy" ]
author: Rob Nugen
date: 2025-01-14T06:08:00+09:00
draft: false
---

I've arrived safely in Cairns; next flight is to Gold Coast in 3 hours.

The weather is definitely not winter ❄️  yayyyy!

Summer weather is such a welcome relief 😺 

[![2025 jan 14 cairns weather forecast 1000](//b.robnugen.com/journal/2025/thumbs/2025_jan_14_cairns_weather_forecast.png)](//b.robnugen.com/journal/2025/2025_jan_14_cairns_weather_forecast_1000.png)

Feeling very grateful for this chance to travel!