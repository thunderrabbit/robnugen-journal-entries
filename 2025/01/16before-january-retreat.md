---
title: "Before January retreat "
tags: [ "2025", "intentions", "2025", "root" ]
author: Rob Nugen
date: 2025-01-16T09:30:00+09:00
draft: false
---

Delicious fruit buffet breakfast today then went to Kirra Beach for some sandy fun (see previous entry!)

Jess and I are chilling in Rydges Gold Coast Hotel foyer after packing our stuff and checking out.

We have about an hour and 15 minutes before our ride is scheduled to pick us up along with another two attendees from out of town.

Today, each retreat attendee will have a private consultation with the facilitators to prepare for the ceremonies tomorrow and the next day (Friday and Saturday).

My current intentions for the retreat are something like "know who I am at a soul level" and "increase my connection with my positive spirit guides."

I've tuned into some other intentions like "finish my first book by April 2025" and "finish Marble Track 3 once I get back to Japan."