---
title: "things to do today"
tags: [ "2025", "todo", "today" ]
author: Rob Nugen
date: 2025-01-07T10:31:00+09:00
draft: false
---

6. ✅ Wash and hang clothes
2. ✅ Parse contents of blue envelope to prepare for annual health exam
4. ✅ Write to John Gaughan
3. ✅ update YNAB
1. ✅ Add to SOML I switched to povo2.0 yesterday to greatly reduce my cellphone costs
5. ✅ Cancel water pause (note on front door)
10. ✅ Ask Aika about water pause
11. ❌ Get black shoe polish
12. ✅ Use inkpad ink on leather shoes
13. ✅ See a rainbow and Lin
14. ✅ Buy two meals of food from Bio c' Bon for 30 yen!
7. ✅ Create Feb event for Yoko
9. ✅ Create walking event for May
16. ✅ Begin creating holes in bicycle cover
8. Create hiking event for May --> 11 Jan 2025
15. ✅ MKP Japan meeting
