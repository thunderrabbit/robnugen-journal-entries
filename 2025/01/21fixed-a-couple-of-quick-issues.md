---
title: "Fixed a couple of Quick issues"
tags: [ "2025", "quick", "fix" ]
author: Rob Nugen
date: 2025-01-21T17:39:00+09:00
draft: false
---

Earlier today I fixed the 500 error https://github.com/thunderrabbit/Quick/issues/7

Jess and I had a nice walk and chill at the beach before her 6pm call with a home owner for whom she'll be house sitting in March.

I just now added a link to my journal, currently using Hugo and visible at https://www.robnugen.com/journal/
