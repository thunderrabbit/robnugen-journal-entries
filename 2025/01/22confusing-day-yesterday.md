---
title: "Confusing Day Yesterday"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-22T05:30:00+09:00
draft: false
---

"I'm hungry," I commented, not knowing I was thereby triggering Jess.

She had spent some time guiding me through a healing with each of my parents, ending with my performing a Ho'oponopono process for each of them.

Jess encouraged and supported me, saying the process helped to release my neediness of being a good boy.

"Yeah, I can be a bad boy," I joked.

"You don't have to be a good boy or a bad boy; you can just be Rob, the man."

I didn't explain it was just joking, knowing there's a kernel of truth behind each joke.  I can just be Rob.

(Goodness, the inner world has lots of nuance.)

She did her own processing after I went down to the dining area for the breakfast buffet.  The maître d' bouncer guy was the same we had seen last week, who asked multiple times to confirm we don't wear shoes.  He finally softened after a while and just said to watch out for glass.
Today he didn't comment on my bare feet.  For this I was thankful.  I told him something like, "I'm checking in one of two people for our room," to ensure Jess would be able to enter easily when she came down.

I enjoyed food, including fruits, coconut yogurt, hash browns, and baked beans and drinks (orange juice I mixed with pineapple juice).   
I also enjoyed the music, including Total Eclipse of the Heart, which reminds me of singing with Sita in Thailand all those years ago. Near the end, I heard a woman behind me singing along as well.  When I looked back, she smiled shyly and then explained, "it's a classic."   I agreed.

Jess arrived after a while and shared what came up for her in her processing.  I actively listened with curiosity and encouraging words.

We were having such a lovely time.  At one point Jess suggested we could go to the beach for a dip and the come back to the hotel and practice full body orgasms.  I jokingly mimicked throwing my bowl onto the table, fake excited "yay let's go!"

She looked almost surprised and said, "I'm not done eating yet."

I wondered how she could have taken me seriously.

We kept talking and eating happily, commenting on the food and activity around us.

At some point Jess was happily dancing and lip syncing to the song "Get Down On It."  Her dance moves included pointing down to her crotch for each "get down on it" in the lyrics.  (There are many)

I was glad to get the message I've been seeking for a long time.  I've been optimistic for sensual connection since New Year's when she was feeling stressed by house sitting Dash and Dex.  Through long distance life and her nomadic lifestyle it seems I've been trying to 'get down on her' for basically our entire relationship.  Sex happens when it happens and I'm trying to be super patient.

Frustrated by the weeks, I said, "I've been trying to get down on it... " Without much thought I joked, "ya lying bitch 😉"

## NEVER CRITICIZE A VIRGO

   On the surface, they may find your critiques unnecessary.
   Remember, they are their own worst critics. They have already
   overanalyzed the outcome and concluded what needs to be done to
   correct it. Sometimes, however, this may not always be the
   case. Deeper introspection into their psyche reveals that they
   could also be feeling insecure. According to Cafe Astrology, “They
   get nervous when others look at their unfinished work, and they are
   quite protective of their methods of doing things.”  Unless
   asked, do not give them any form of criticism. And if you do,
   prepare to see their not-so-friendly side.