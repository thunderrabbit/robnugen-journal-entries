---
title: "Back on track with Quick"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-09T09:19:00+09:00
draft: false
---

Activating the side effect in the index.php file allows the classes to just have that taken care of for them.

Next I want to clean up a bunch of debug output, only showing it when it's requested, and possibly showing it in a nice way.