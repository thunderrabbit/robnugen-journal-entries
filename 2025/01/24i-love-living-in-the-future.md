---
title: "I love living in the future"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-24T17:31:00+09:00
draft: false
---

This is such a simple thing but pleases me so much:

Concrete tracks for buses!

[![2025 jan 24 adelaide bus tracks 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_jan_24_adelaide_bus_tracks.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_jan_24_adelaide_bus_tracks_1000.jpeg)

I love seeing the natural areas as we flow through the trees and hills.
