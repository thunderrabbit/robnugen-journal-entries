---
title: "extra yummy breakfast today"
tags: [ "2025", "breakfast " ]
author: Rob Nugen
date: 2025-01-23T08:35:00+09:00
draft: false
---

Breakfast was extra delicious at Rydges today because according to the system, it was not included in the hotel booking for today even though the bookings were all done at the same time.

The woman knew I've been here for the past 2 days so she just said it's okay to come in.