---
title: "taped my bicycle to a fence"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-13T07:52:00+09:00
draft: false
---

Just using tape so a human can remove it if necessary, but hopefully wind won't blow it down.

[![2025 jan 13 bicycle secured to fence 1000](//b.robnugen.com/journal/2025/thumbs/2025_jan_13_bicycle_secured_to_fence.jpeg)](//b.robnugen.com/journal/2025/2025_jan_13_bicycle_secured_to_fence_1000.jpeg)