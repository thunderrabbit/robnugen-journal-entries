---
title: "I got a B"
tags: [ "future", "past", "self", "a", "guillebeau" ]
author: Rob Nugen
date: 2025-01-05T12:34:18+09:00
draft: false
---

(written 12:34 Sunday 31 March 2024 AEDT)

<p class="note">via
https://yearofmentalhealth.substack.com/p/grading-your-future-self</p>

Dear Rob

Great job!

You get a B for the past year!  I'm really proud of your efforts in
moving forward in your personal practice.  You had been trying for a
long time to do so, and this year was a standout success!

You did this by:

* Hosting Connection Circle events with Jess twice!
* Writing more of your book I'M FINE!
* Attending Soulmate mini-level 2 with Jess in Bali

Alongside these is a goal of receiving an upfront payment of at least
$3000 (*) for a private container with a client.  As of this writing,
$3000 is greater than what you've been receiving in part time job AB.

(*) Equivalent for this exercise:

* 300,000 yen
* AUD $3,000
* USD $3,000
