---
title: "possible itinerary for New Zealand"
tags: [ "2025", "ChatGPT", "NZ" ]
author: Rob Nugen
date: 2025-01-30T17:12:00+09:00
draft: false
---

I created a [ChatGPT to help with planning trips in New Zealand](https://chatgpt.com/g/g-679b05d3c96481919d725d5a2e55399b-travel-new-zealand-2025)

[![2025 jan 30 chatgpt itinerary 1000](//b.robnugen.com/journal/2025/NewZealand/thumbs/2025_jan_30_chatgpt_itinerary.png)](//b.robnugen.com/journal/2025/NewZealand/2025_jan_30_chatgpt_itinerary_1000.png)


Here is a link to the ChatGPT if you want to play with it:

https://chatgpt.com/g/g-679b05d3c96481919d725d5a2e55399b-travel-new-zealand-2025