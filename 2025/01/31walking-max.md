---
title: "Walking Max"
tags: [ "2025", "max", "dog", "walk" ]
author: Rob Nugen
date: 2025-01-31T11:06:00+09:00
draft: false
---

[![2025 jan 31 walking chilling max 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_jan_31_walking_chilling_max.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_jan_31_walking_chilling_max_1000.jpeg)

Max is Sophie and Nick's black toy poodle(?) who Jess and I are sitting for two nights. We went for a quite short walk only about 1 house away from home; he didn't seem interested in going further.