---
title: "before getting my annual physical done"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-10T14:58:00+09:00
draft: false
---

One of the great things about Japan is receiving an annual physical checkup subsidized by government.

I found the nearest clinic that seems to do all the tests mentioned in the list, and made an appointment with them a few weeks ago by going to the front desk of the clinic (which happens to be within a hospital) and letting them see the papers which say I can get the physical.

Japan loves paperwork.  It's at least half the reason I haven't had to really learn the language. 😔

Via postal mail, they sent me two poop collection vials and a urine collection vial and several papers to fill out.

I purposefully did not fill out the temperature  chart which hoped to get my temperature recorded for the past ten days.

It's one way of pushing back against the plandemic induced planic, plus no one wears masks anywhere except Japan apparently.

Plus, I ain't got a thermometer.

I arrived at the clinic within the hospital and turned in my papers, poop vials, and insurance card.

After having a seat for a bit, one of them called my name.

She returned some of the excess papers I had given her and then said in Japanese 「マスク外していは失礼から」(It is rude to not wear your mask.)

I said in Japanese, I don't have a mask.

She said well downstairs on the first floor, you can buy a mask from the shop.

I said in English "You know this whole mask thing doesn't help, right?  I'm not going to go downstairs to buy a mask. I wasn't told anything about this when I came last time."

She briefly communicated with the person on her right and then went and got a mask from their back office.

I was feeling angry but held space and didn't say anything more. I almost got what I wanted, and hopefully this makes them think a bit more before asking everyone to wear masks.