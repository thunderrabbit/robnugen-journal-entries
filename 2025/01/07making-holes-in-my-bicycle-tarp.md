---
title: "making holes in my bicycle tarp"
tags: [ "2025", "bicycle", "tarp", "holes" ]
author: Rob Nugen
date: 2025-01-07T17:55:00+09:00
draft: false
---

I wrapped two chapstick tubes with paper and tape, stabbed them through my bicycle tarp and then sealed them with waterproof ゴム.

Once that has dried, I'll do the other side and then pull the tubes out of the paper and have two perfectly round, sealed holes in the tarp.

This will allow me to lock my bicycle to a fence while it's wearing its tarp.

[![2025 jan 07 making hole in bicycle tarp 1000](//b.robnugen.com/journal/2025/bicycle/thumbs/2025_jan_07_making_hole_in_bicycle_tarp.jpeg)](//b.robnugen.com/journal/2025/bicycle/2025_jan_07_making_hole_in_bicycle_tarp_1000.jpeg)