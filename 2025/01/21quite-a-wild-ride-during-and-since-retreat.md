---
title: "Quite a wild ride during and since retreat"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-21T14:35:00+09:00
draft: false
---

Wow

1. I was inspired to take a double-dose (23 spooonfuls) after hearing we cannot have too much.
2. During the ceremony I felt plenty sick but didn't seem to experience more healing than from a regular men's group facilitation would have done.
3. I was sleeping while people in the circle were ringing a bell; I was woken up when it was my time to ring, but the bell was silent.  I couldn't understand why nor really ask.  I finally realized it was to snuff out our candles, so I did that, then shook the bell again which caused wax to get on the floor.  Jess yelled across the room "Robstoppit" or at least "stoppit!" and it hit me like a ton of arrows and I just collapsed in my agonizing grief of not being understood.
4. Christof said something like "now Jess, he's not coherent" before he and Alaya helped hold space for me directly while the group held space for us.
5. After the next night, when I had approximately 1/80th the previous dose, I injured my finger while dancing, nearly passed out (so I lay down instead), then got up for more dancing and then properly passed out after we were invited to stand shoulder to shoulder in a circle.
6. I knew I was passing out, and started to sit down, and was fully unconscious by the time Babak caught me on my way down.
7. When I awoke 30 seconds later to see Christof I was so confused like "? what happened? oh yeah I passed out."
