---
title: "Gwiddy Beach"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-01-23T07:26:00+09:00
draft: false
---

I think Gwiddy Beach is where Fred and I climbed up a few meters on a rock and found a fish that had been trapped in its own little pool of water.  We saved it (hopefully!) and proudly let Ma and Dad know our good deed.

https://maps.app.goo.gl/9njBh8jPEGo69Xwg6

The beach was loaded with sponges when we were there in June 1985.