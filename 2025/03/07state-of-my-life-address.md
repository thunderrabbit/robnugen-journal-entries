---
title: "State Of My Life Address"
tags: [ "soml" ]
author: Rob Nugen
date: 2025-03-07T04:28:32+09:00
draft: false
---

(written 04:28 Friday 07 March 2025 ACDT)

1. I was just in Noarlunga for a couple of days.  So fun to float
up-river (around the bend) as the tide flowed in!  A bit spooky when
the mouth of the river was *almost* too deep for me to get back
across.
2. Today I'm heading to Kuala Lumpur to top up my Australian visa
3. Jess is in London for a month (3 more weeks to go?)
4. I house-sat Guvvie in Seacliff for a week and will do another week
from 18th March
5. MKP Parklands men gave great feedback on my facilitation
6. Nearing completion of my first draft of I'M FINE!  Joe and Jacob
described it as gold and diamond in the rough.
7. Still working on AB
8. Cuddle Party was fun in Feb and hopefully fun in April
9. I'm currently planning to start closing down my apartment in Japan
when I get back in May.
