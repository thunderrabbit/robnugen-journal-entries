---
title: "To Dooz"
tags: [ "todo", "oz", "kl" ]
author: Rob Nugen
date: 2025-03-07T04:27:08+09:00
draft: false
---

(written 04:27 Friday 07 March 2025 ACDT)

Today is March 7th, 2025.  I have been working diligently on I'M FINE.  I'm nearing completion of the first draft. 

Regarding I'M FINE, I may need some extra encouragement to finish the draft, so be on the look out for me ignoring it.  ;-)

In other news, I have been studying DeFi, and working on AndBeyond a bit.  I need to complete 25 more hours of AndBeyond in March.   Those will come bit by bit and maybe in a burst near the end of the month.

I followed up with Tomomi on frontend search issue but she hasn't replied.  I should follow up again this week.

I posted the April 27th Cuddle Party event pages.   I should promote it on FB today or tomorrow March 8th.

I finished meeting with Susan regarding NZ trip.  We decided to postpone the trip indefinitely.  I'm going to Kuala Lumpur today until March 12th when I'll come back to Adelaide.

I finished the call with Jen re Landmark.  We both feel complete.

I paid $30 to Parklands MKP on March 4th.

OTher items on your above list are still outstanding.

Here are new items.  (Most of the dates below are the dates of flights, so the due dates for the items are necessarily before the flights themselves (I suggest at least one week ahead).)

* Fly to Kuala Lumpur 11 AM today
* Pay MTC for their Premium package this afternoon after arrival in KL.
* attend MTC bootcamp this weekend (tomorrow and next day)
* renew my Australian visa while in Kuala Lumpur
* Make a note of fees received in Kamino.finance for WILDNOUT - SOL  (received 0.11 SOL on March 7th (please look up the equivalent in USD for that day))
* book a flight from Adelaide to Perth for March 25th
* tell Denise the time of the March 25th flight to Perth so she can take me to the airport
* book flights from Perth to Adelaide (flights will be approximately April 13th)
* confirm dates for flights from Adelaide to Melbourne (flights will be around the end of April)
* book flights from Adelaide to Melbourne (for around the end of April)
