---
title: "Upside down by Glenelg beach"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-02-07T09:55:00+09:00
draft: false
---

Jess and I met Ribbia on Friday.  I'm sad we forgot to get a pic of all three of us, but Ribbia and I got to catch up during a swim.

[![2025 feb 08 rob upside down in glenelg beach 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_08_rob_upside_down_in_glenelg_beach.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_08_rob_upside_down_in_glenelg_beach_1000.jpeg)