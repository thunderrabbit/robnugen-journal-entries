---
title: "Sent out ruff draft of my book to four men"
tags: [ "2025", "im-fine", "ruff" ]
author: Rob Nugen
date: 2025-02-02T06:48:00+09:00
draft: false
---

I sent it to JH, JP, MK, and ミ

Thank you to MK for initial feedback:

    Just got started reading ‘I’m Fine’—the table of contents is impressive.  Wow!  Looks like it’s going to be an amazing book.  I know you want big stuff, Rob—and I’ll get to that in time—but I couldn’t help but notice you’re editing quite frequently, so please consider my sometimes nit picky stuff.



Thank you to JP for initial disclaimer:

    Hello Rob,

    I started reading your book and I decided I needed to let you know of some things about me that will impact my review.

    First, I used to work in journalism and was an editor for a while. This instilled in me an approach of being overly critical.

    Second, all my comments are intended to help you. I love what I have read so far and I think your ideas are right on the mark. I can absolutely relate to what you are saying. You are definitely on the right track; what I am seeing so far is some clean-up and reorganization may be needed, but that's all.

    I wanted you to know this because I didn't want you to misinterpret my comments. Obviously, please feel free to ignore anything I will say - it is your book and I am just providing my thoughts on it, which may be totally misguided.
