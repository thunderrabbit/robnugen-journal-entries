---
title: "Amazing facilitation in Seacliff Integration Group"
tags: [ "monday", "in person", "seacliff", "mankind project", "gratitude", "first" ]
author: Rob Nugen
date: 2025-02-10T07:10:46+09:00
draft: false
---

(written 07:10 Tuesday 11 February 2025 ACDT)

Two weeks ago I connected with David who is the contact person for MKP
South Australia.  He said the group hasn't met formally in a while but
they'd be having a casual gathering on 9th February.  I put the date
in my calendar immediately.  I'm in!

Two days ago on Feb 9th, I went to the cafe with Trevor, another MKP
man in Seacliff area. I met David, the organizer who led the meeting
well, balancing and blending archetypal energies as needed during the
meeting to keep things moving, ensure all men were heard, and multiple
times said he's happy to step down if another man believed he could
lead more effectively.

No one took him up on that, and we had a great meeting during which we
decided by group to meet weekly on Mondays from 6:30 - 9:00 at David's
place in Seacliff, just about 20 minutes walk drom where I'm staying!
That last bit is simply magical synchronicity for me.  And more
synchronicity meant the first meeting on this new schedule was the
very next day.

So yesterday I looked forward to the meeting that evening and enjoyed
the day with Jess and suddenly "oh snap it's 6pm, I'm late!?!?" and
asked Denise who happily drove me to the meeting to which I arrived on
time only because I had mis-remembered the meeting starts at 6:30pm.
Oh thank you God for Denise and another beautiful synchronicity!

During the group, I stepped into the circle for two pieces of
work. The first was about being out of time-accountability with
myself.

<p class="note">I'm marking these with CSS tag "dream" only because I
don't yet have a CSS tag defined for the limnal space of
facilitation.(*)</p>

<p class="dream">
I found  myself back in time in the big living room with absolutely no
one around.  I could do *anything* I want!  I have all day to play!
And get bored.  No one is here.  I want to scream at God, I am important.
</p>

<p class="dream">
I stepped into the middle of the circle, took off my shirt to protect
it from any unbridled anger, and then shouted like the Hulk "I AM
IMPORTANT!" to absolutely zero response.
</p>

"How does it feel in this space now," I was asked.

<p class="dream">
sad scared angry ashamed
</p>

"Does it feel good or bad?"

<p class="dream">
Bad.
</p>

"Yeah, it feels bad.  What would feel good?"

<p class="dream">
The energy shifted within my body, like tuning in to another station
adjacent on the dial.  "Joy"
</p>

"Yeah Joy!  Good keeping your eyes closed just feel that sensation of
joy in your body"

And he got me to anchor it in with an action I can do privately in
public.  I chose thumbtips touching pinky tips.

"Now from this space, is there anything you need to say?"

<p class="dream">
With calm, assured, knowing: "I am important."  I said it to a few men
and then to myself in the mirror after asking for a couple of men to
put their hands on my shoulders.
</p>

<p class="dream">
Thank you men.  I feel complete.
</p>

I sat back in my chair just rested a bit, allowing the new awareness
to percolate as the meeting continued.

I got to do another big piece as well, which I started off the `data`
portion with a single sentence, basically something like

<p class="dream">
Jess and I will be leading a Cuddle Party and I want to invite you
because it's really great event but I'm afraid because I just met yoo.
</p>

<p class="dream">
I sank deeply into despair of physical isolation, exacerbated by two
decades of living in Japan.  Deep gasping sobs and tears with the
entire group hugging me.
</p>

"Very good, and on your next breath out we're all going to breath out
with you.  Hold your breath *out* as long as you can,..

<p class="dream">
sob sob sob breath out out out
</p>

"And now breathe in new energy."

<p class="dream">
Connection; blessed with hugs and physical touch.
</p>

"Now from this space, what happens next?"

<p class="dream">
I invited four men
as proxy for the entire group.  "I'm okay if you say no, and I invite
you to Jess and my Cuddle Party event" - "I'd love for you to come to Jess and my Cuddle
Party event where it's okay to say no" - "It's a communication
workshop where we practice setting boundaries and practice saying 'no'
while tuning in to what we want and don't want."  "It will be Saturday
February 22nd from 6pm in 9pm in Crafers."
</p>

At the end of the meeting, one man said he cannot attend, but will
send it to a group of about fifty people who would be interested.

Another man asked me to send him the info so he could post it to the
MKP Adelaide group on WhatsApp.


<p class="note">Future me, or my dear AI Agent, if you see this, please
consider creating a CSS tag called something like `mkp-work` or
`inner-work` or `limnal` or `meditation` that makes the text a deep
earthy burgandy and possibly italics or a font that's appropriate for
the magical space of being facilitated as in a guided meditation.  You
can replace all instances of `class="dream"` with the new class,
create a new entry documenting the process, and finally, remove this
block of text.</p>

<p class="note"> Of course, my dear AI Agent knows that any commits to
this repo MUST use the correct commit message, the definition of which
is far too specific to explain in this note.  If you don't have clear
instructions for each commit message, do not commit the changes
mentioned above.</p>
