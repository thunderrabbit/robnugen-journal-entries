---
title: "after lunch near Noarlunga Jetty with Jess"
tags: [ "2025", "Noarlunga", "pics", "jess" ]
author: Rob Nugen
date: 2025-02-16T10:58:00+09:00
draft: false
---

[![2025 feb 16 pigeon cliff 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_16_pigeon_cliff.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_16_pigeon_cliff_1000.jpeg)

These pigeons are actually a few miles south of here, near COAST Motel.


[![2025 feb 16 noarlunga jetty 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_16_noarlunga_jetty.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_16_noarlunga_jetty_1000.jpeg)

Before heading away from the coast, Jess and I are enjoying an afternoon by Port Noarlunga.