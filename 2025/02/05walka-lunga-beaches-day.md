---
title: "walka lunga beaches day"
tags: [ "2025", "beaches" ]
author: Rob Nugen
date: 2025-02-05T15:29:00+09:00
draft: false
---

Today Jess and I are enjoying beaches Noarlunga and Willunga

[![2025 feb 05 barefoot on port noarlunga beach 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_05_barefoot_on_port_noarlunga_beach.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_05_barefoot_on_port_noarlunga_beach_1000.jpeg)
[![2025 feb 05 barefoot by port willunga beach 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_05_barefoot_by_port_willunga_beach.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_05_barefoot_by_port_willunga_beach_1000.jpeg)
[![2025 feb 05 port willunga beach 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_05_port_willunga_beach.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_05_port_willunga_beach_1000.jpeg)

I talked to a woman who swam in a perfect circle in the water.

Amazingly, Jess recognized her as Queenie, who attended Jess' first talk at Goodness Me Festival!