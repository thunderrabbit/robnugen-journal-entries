---
title: "Granges beach"
tags: [ "2025", "beach", "oz", "Jess", "Ruri " ]
author: Rob Nugen
date: 2025-02-01T10:45:00+09:00
draft: false
---

Special thanks to Ruri for driving us to Granges Beach today!

Special thanks to the beautiful weather and the gentle waves.

Thank you God for beautiful day and experience.

[![2025 feb 01 bare feet in water 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_01_bare_feet_in_water.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_01_bare_feet_in_water_1000.jpeg)

