---
title: "great time playing UNO with Jess"
tags: [ "2025", "" ]
author: Rob Nugen
date: 2025-02-15T18:25:00+09:00
draft: false
---

It was really nice to play! We plan to do so more often.

Unrelated photo?

[![2025 feb 15 crepuscular 1000](//b.robnugen.com/journal/2025/Australia/thumbs/2025_feb_15_crepuscular.jpeg)](//b.robnugen.com/journal/2025/Australia/2025_feb_15_crepuscular_1000.jpeg)

